﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MonoBehaviour
{
    public float speed = 500; // cycles per minute

    // Update is called once per frame
    void Update()
    {

        transform.Rotate(0, 0, Time.deltaTime * -1 * speed, Space.World);
    }

}
