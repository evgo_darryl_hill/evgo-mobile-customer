﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(OnlineMapsTileSetControl)]

public class OnlineMapHelper : MonoBehaviour
{
    public OnlineMapsTileSetControl onlineMapsTileSetControl;

    // Use this for initialization
    void Start()
    {
        OnlineMapsTileSetControl.instance.Resize(1024, 1024);
    }

    public void OnButtonClick()
    {

        OnlineMapsTileSetControl.instance.Resize(512, 512);
        OnlineMapsTileSetControl.instance.UpdateControl();


    }
}

