//
// Created by Taras Leskiv on 4/5/18.
//

#import "GoogleMapsViewClusterItem.h"


@implementation GoogleMapsViewClusterItem {

}

- (instancetype)initWithPosition:(CLLocationCoordinate2D)position name:(NSString *)name snippet:(NSString *)snippet {
    self = [super init];
    if (self) {
        _position = position;
        _name = name;
        _snippet=snippet;
    }

    return self;
}

+ (instancetype)itemWithPosition:(CLLocationCoordinate2D)position name:(NSString *)name snippet:(NSString *)snippet {
    return [[self alloc] initWithPosition:position name:name snippet:snippet];
}


@end