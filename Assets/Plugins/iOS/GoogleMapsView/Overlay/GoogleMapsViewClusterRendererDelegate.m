
#import <GoogleMaps/GoogleMaps.h>
#import "GoogleMapsViewClusterRendererDelegate.h"
#import "GoogleMapsViewClusterItem.h"


@implementation GoogleMapsViewClusterRendererDelegate {

}

- (void)renderer:(id <GMUClusterRenderer>)renderer willRenderMarker:(GMSMarker *)marker {
    NSString *className = GoogleMapsViewClusterItem.class.description;
    if ([[[marker.userData class] description] isEqualToString:className]) {
        GoogleMapsViewClusterItem *item = marker.userData;
        marker.title = item.name;
        marker.snippet = item.snippet;
    }
}

@end
