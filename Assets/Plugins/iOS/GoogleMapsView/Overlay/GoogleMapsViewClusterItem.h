#import <Foundation/Foundation.h>

#import <GoogleMaps/GoogleMaps.h>
#import "GMUClusterItem.h"

@interface GoogleMapsViewClusterItem : NSObject <GMUClusterItem>

@property(nonatomic, readonly) CLLocationCoordinate2D position;
@property(nonatomic, readonly) NSString *name;
@property(nonatomic, readonly) NSString *snippet;

- (instancetype)initWithPosition:(CLLocationCoordinate2D)position name:(NSString *)name snippet:(NSString *)snippet;

+ (instancetype)itemWithPosition:(CLLocationCoordinate2D)position name:(NSString *)name snippet:(NSString *)snippet;


@end
