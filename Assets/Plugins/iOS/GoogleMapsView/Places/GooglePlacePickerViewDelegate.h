#import <Foundation/Foundation.h>
#import <GooglePlacePicker/GMSPlacePickerViewController.h>

@interface GooglePlacePickerViewDelegate : NSObject <GMSPlacePickerViewControllerDelegate>

+ (GooglePlacePickerViewDelegate *)instance;

@end
