
#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"

#import <GooglePlacePicker/GMSPlacePicker.h>
#import <GooglePlacePicker/GMSPlacePickerViewController.h>
#import <GoogleMaps/GoogleMaps.h>
#import "GooglePlacePickerViewDelegate.h"
#import "GooglePlaceAutocompleteViewDelegate.h"
#import "GoogleMapsViewUtils.h"
#import "GooglePlacesJsonUtils.h"

extern "C" {

void _googlePlacesInit(char *apiKey) {
    NSString *apiKeyStr = [GoogleMapsViewUtils createNSStringFrom:apiKey];
    [GMSServices provideAPIKey:apiKeyStr];
    [GMSPlacesClient provideAPIKey:apiKeyStr];
}

void _googleMapsShowPlacePicker() {
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
    GMSPlacePickerViewController *placePicker = [[GMSPlacePickerViewController alloc] initWithConfig:config];
    placePicker.delegate = GooglePlacePickerViewDelegate.instance;

    [UnityGetGLViewController() presentViewController:placePicker animated:YES completion:nil];
}

void _googleMapsShowPlaceAutocomplete(int mode, int filter, char* countryCode, char *bounds) {
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = GooglePlaceAutocompleteViewDelegate.instance;

    GMSAutocompleteFilter *acFilter = [[GMSAutocompleteFilter alloc] init];
    acFilter.type = (GMSPlacesAutocompleteTypeFilter)filter;
    
    if (countryCode != nil) {
        NSString *countryStr = [GoogleMapsViewUtils createNSStringFrom:countryCode];
        acFilter.country = countryStr;
    }
    
    acController.autocompleteFilter = acFilter;
    
    if (bounds != nil) {
        NSString *boundsStr = [GoogleMapsViewUtils createNSStringFrom:bounds];
        NSDictionary *boundsDic = [GooglePlacesJsonUtils deserializeDictionary:boundsStr];
        GMSCoordinateBounds *acBounds = [GooglePlacesJsonUtils deserializeBounds:boundsDic];
        acController.autocompleteBounds = acBounds;
    }

    [UnityGetGLViewController() presentViewController:acController animated:YES completion:nil];
}

}

#pragma clang diagnostic pop

