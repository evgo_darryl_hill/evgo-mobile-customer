#import "GooglePlaceAutocompleteViewDelegate.h"
#import "GooglePlacesJsonUtils.h"

@implementation GooglePlaceAutocompleteViewDelegate {
    
}
+ (GooglePlaceAutocompleteViewDelegate *)instance {
    static GooglePlaceAutocompleteViewDelegate *_instance = nil;
    
    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }
    
    return _instance;
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place {
    [viewController dismissViewControllerAnimated:YES completion:nil];
    NSString* json = [GooglePlacesJsonUtils serializePlace:place];
    UnitySendMessage("GooglePlacesSceneHelper", "OnPlaceAutocompleteSuccess", [json UTF8String]);
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error {
    [viewController dismissViewControllerAnimated:YES completion:nil];
    UnitySendMessage("GooglePlacesSceneHelper", "OnPlaceAutocompleteFailed", "TODO");
}

- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [viewController dismissViewControllerAnimated:YES completion:nil];
    UnitySendMessage("GooglePlacesSceneHelper", "OnPlaceAutocompleteFailed", "USER_CANCELLED");
}

@end
