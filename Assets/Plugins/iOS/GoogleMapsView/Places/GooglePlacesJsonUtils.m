#import <GooglePlaces/GooglePlaces.h>
#import "GooglePlacesJsonUtils.h"

@implementation GooglePlacesJsonUtils

+ (NSString *)serializeDictionary:(NSDictionary *)dictionary {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:nil
                                                         error:&error];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

+ (NSDictionary *)deserializeDictionary:(NSString *)jsonDic {
    NSError *e = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[jsonDic dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&e];
    if (dictionary != nil) {
        NSMutableDictionary *prunedDict = [NSMutableDictionary dictionary];
        [dictionary enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL *stop) {
            if (![obj isKindOfClass:[NSNull class]]) {
                prunedDict[key] = obj;
            }
        }];
        return prunedDict;
    }
    return dictionary;
}

+ (GMSCoordinateBounds *)deserializeBounds:(NSDictionary *)boundsDic {
    if (boundsDic == nil) {
        return nil;
    }

    CLLocationDegrees southWestLat = [boundsDic[@"latLngBoundsSouthWestLat"] doubleValue];
    CLLocationDegrees southWestLng = [boundsDic[@"latLngBoundsSouthWestLng"] doubleValue];
    CLLocationDegrees northEastLat = [boundsDic[@"latLngBoundsNorthEastLat"] doubleValue];
    CLLocationDegrees northEastLng = [boundsDic[@"latLngBoundsNorthEastLng"] doubleValue];
    CLLocationCoordinate2D NE = CLLocationCoordinate2DMake(northEastLat, northEastLng);
    CLLocationCoordinate2D SW = CLLocationCoordinate2DMake(southWestLat, southWestLng);
    return [[GMSCoordinateBounds alloc] initWithCoordinate:NE coordinate:SW];
}

+ (NSString *)serializePlace:(GMSPlace *)place {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    dictionary[@"id"] = place.placeID;
    dictionary[@"placeTypes"] = place.types;
    dictionary[@"address"] = place.formattedAddress;
    dictionary[@"attributions"] = place.attributions;
    dictionary[@"name"] = place.name;
    dictionary[@"phoneNumber"] = place.phoneNumber;
    dictionary[@"priceLevel"] = @(place.priceLevel);
    dictionary[@"rating"] = @(place.rating);

    //Location
    dictionary[@"lat"] = @(place.coordinate.latitude);
    dictionary[@"lng"] = @(place.coordinate.longitude);

    //Bounds
    dictionary[@"boundsNorthEastLat"] = @(place.viewport.northEast.latitude);
    dictionary[@"boundsNorthEastLng"] = @(place.viewport.northEast.longitude);
    dictionary[@"boundsSouthWestLat"] = @(place.viewport.southWest.latitude);
    dictionary[@"boundsSouthWestLng"] = @(place.viewport.southWest.longitude);

    dictionary[@"uri"] = place.website.absoluteString;

    return [self serializeDictionary:dictionary];
}

@end
