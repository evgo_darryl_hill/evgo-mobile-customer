#import <Foundation/Foundation.h>
#import <GooglePlaces/GMSAutocompleteViewController.h>

@interface GooglePlaceAutocompleteViewDelegate : NSObject<GMSAutocompleteViewControllerDelegate>

+ (GooglePlaceAutocompleteViewDelegate *)instance;

@end
