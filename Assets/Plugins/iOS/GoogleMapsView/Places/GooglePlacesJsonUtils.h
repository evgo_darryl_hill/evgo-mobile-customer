#import <Foundation/Foundation.h>

@class GMSPlace;
@class GMSCoordinateBounds;

@interface GooglePlacesJsonUtils : NSObject

+ (NSString *)serializeDictionary:(NSDictionary *)dictionary;

+ (NSDictionary *)deserializeDictionary:(NSString *)jsonDic;

+ (GMSCoordinateBounds *)deserializeBounds:(NSDictionary *)boundsDic;

+ (NSString *)serializePlace:(GMSPlace *)place;

@end
