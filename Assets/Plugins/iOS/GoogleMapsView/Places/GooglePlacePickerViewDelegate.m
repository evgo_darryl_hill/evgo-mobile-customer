
#import "GooglePlacePickerViewDelegate.h"
#import "GooglePlacesJsonUtils.h"

@implementation GooglePlacePickerViewDelegate {

}
+ (GooglePlacePickerViewDelegate *)instance {
    static GooglePlacePickerViewDelegate *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}

- (void)placePicker:(GMSPlacePickerViewController *)viewController didPickPlace:(GMSPlace *)place {
    [viewController dismissViewControllerAnimated:YES completion:nil];
    NSString* json = [GooglePlacesJsonUtils serializePlace:place];
    UnitySendMessage("GooglePlacesSceneHelper", "OnPlacePickSuccess", [json UTF8String]);
}

- (void)placePicker:(GMSPlacePickerViewController *)viewController didFailWithError:(NSError *)error {
    [viewController dismissViewControllerAnimated:YES completion:nil];
    UnitySendMessage("GooglePlacesSceneHelper", "OnPlacePickFailed", [error localizedDescription].UTF8String);
}

- (void)placePickerDidCancel:(GMSPlacePickerViewController *)viewController {
    [viewController dismissViewControllerAnimated:YES completion:nil];
    UnitySendMessage("GooglePlacesSceneHelper", "OnPlacePickFailed", "USER_CANCELLED");
}

@end
