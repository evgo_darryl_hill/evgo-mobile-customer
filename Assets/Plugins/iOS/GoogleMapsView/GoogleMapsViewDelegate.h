//
// Created by Taras Leskiv on 29/12/2017.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@class CustomGMSMarker;
@class CustomGMSCircle;


@interface GoogleMapsViewDelegate : NSObject<GMSMapViewDelegate>

+ (GoogleMapsViewDelegate *)instance;

@property(nonatomic, copy) void (^mapTapped)(CLLocationCoordinate2D location);

@property(nonatomic, copy) void (^mapLongTapped)(CLLocationCoordinate2D location);

@property(nonatomic, copy) void (^markerTapped)(CustomGMSMarker* marker);

@property(nonatomic, copy) void (^markerInfoWindowTapped)(CustomGMSMarker *marker);

@property(nonatomic, copy) void (^circleTapped)(CustomGMSCircle* circle);

@end