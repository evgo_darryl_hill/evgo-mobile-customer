﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace EVgo
{
    [RequireComponent(typeof(Image))]
    public class ShowIconForType : MonoBehaviour
    {
        public Text typeText;
        public List<Sprite> sprites;
        private Image image;

        public void Start()
        {
            image = this.GetComponent<Image>();
            eConnectorType myType = (eConnectorType)Enum.Parse(typeof(eConnectorType), typeText.text, true);

            switch (myType)
            {
                case eConnectorType.Chademo:
                    image.sprite = sprites[(int)myType];
                    break;

                case eConnectorType.Combo:
                    image.sprite = sprites[(int)myType];
                    break;
            }
        }

        //void ForceIconToIndex(int index)
        //{
        //    image.sprite = sprites[index];
        //}
    }

}

