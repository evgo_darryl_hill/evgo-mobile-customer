﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EVgo
{
    public class GoogleApi : MonoBehaviour
    {
        public RawImage rawImage;

        string url;
        //GeoPoint geoPoint;

        //LocationInfo li;
        public float lat;
        public float lon;

        LocationInfo li;
        public int zoom = 14;
        public int mapWidth = 640;
        public int mapHeight = 640;
        public int scale;

        public enum eMapType
        {
            roadmap, satellite, hybrid, terrain
        }
        public eMapType mapType;


        IEnumerator Map()
        {
            url = "";
            WWW www = new WWW(url);
            yield return www;

            rawImage.texture = www.texture;
            rawImage.SetNativeSize();
        }

        // Use this for initialization
        void Start()
        {
            rawImage = gameObject.GetComponent<RawImage>();
            StartCoroutine(Map());
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
