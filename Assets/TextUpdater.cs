﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DoozyUI;
using UnityEngine.UI;
using TMPro;

namespace EVgo
{
    [RequireComponent(typeof(TMP_Text))]

    public class TextUpdater : MonoBehaviour
    {
        TMP_Text myText;

        void Start()
        {
            myText = this.GetComponent<TMP_Text>();

            UpdateMyValue();
        }

        public void UpdateMyValue()
        {
            myText.text = AppStateController.Instance.appStateData.CurrentState;
        }
    }
}