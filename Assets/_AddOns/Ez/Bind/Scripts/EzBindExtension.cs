// Copyright (c) 2016 - 2018 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using System.Collections.Generic;
using Ez.Binding.Vars;
using UnityEngine;
using UnityEngine.Events;

namespace Ez.Binding.Internal
{
    public class EzBindExtension : MonoBehaviour
    {
        public List<BindData> bindsData;

        private List<Bind> bindsCreatedBySelf;
        private List<KeyValuePair<Bind, ReferencedVariable>> refVarsCreatedBySelf;
        private List<KeyValuePair<Bind, UnityAction>> listenersAddedBySelf;
        private bool shouldCleanup = false;

        // Use this for initialization
        void Start()
        {
            bindsCreatedBySelf = new List<Bind>();
            refVarsCreatedBySelf = new List<KeyValuePair<Bind, ReferencedVariable>>();
            listenersAddedBySelf = new List<KeyValuePair<Bind, UnityAction>>();

            AddOwnBinds();

            bindsData = null;
            shouldCleanup = true;
        }

        private void OnDisable() { if(shouldCleanup) { RemoveOwnBinds(); } }
        private void OnApplicationQuit() { shouldCleanup = false; }

        void AddOwnBinds()
        {
            if(bindsData == null || bindsData.Count == 0) { return; }

            Bind bind;
            ReferencedVariable refVar = null;
            object holderRef;
            bool refVarCreated;
#if EZ_PLAYMAKER_SUPPORT
            HutongGames.PlayMaker.NamedVariable fsmVar;
#endif
#if EZ_BOLT_SUPPORT
            Bolt.VariableDeclaration boltVar;
#endif
            for(int i = 0; i < bindsData.Count; i++)
            {
                if(string.IsNullOrEmpty(bindsData[i].bindName) || bindsData[i].bindName.Equals("UnnamedBind")) { continue; }

                bind = EzBind.FindBindByName(bindsData[i].bindName);
                refVarCreated = false;

                if(bind == null)
                {
                    // The bind does not exist, it must be created first
                    bind = EzBind.AddBind(bindsData[i].bindName, bindsData[i].OnValueChanged);
                    if(bind == null) { continue; } // should NEVER happen here, but check just in case

                    bindsCreatedBySelf.Add(bind);

                    if(!bindsData[i].source.useScriptableObject &&
                        bindsData[i].source.gameObject != null &&
                        bindsData[i].source.component != null &&
                        !string.IsNullOrEmpty(bindsData[i].source.variableName) &&
                        !bindsData[i].source.variableName.Equals("None"))
                    {
                        if(EzBind.GetHolderReferenceIfExists(bindsData[i].source, bindsData[i].source.component, out holderRef))
                        {
                            // Add the source with GO & holder; check if PlayMaker / Bolt 
#if EZ_PLAYMAKER_SUPPORT
                            if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                            {
                                // Make refVar a reference to the FSM NamedVariable
                                fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(bindsData[i].source.variableName.Split(' ')[1]);
                                refVar = new ReferencedVariable(bind.name, bindsData[i].source.gameObject, fsmVar, fsmVar.Name, "RawValue");
                                refVarCreated = true;
                            }
#endif
#if EZ_BOLT_SUPPORT
                            if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                            {
                                // Make refVar a reference to the Bolt Variable
                                boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(bindsData[i].source.variableName.Split(' ')[1]);
                                refVar = new ReferencedVariable(bind.name, bindsData[i].source.gameObject, boltVar, boltVar.name, "value");
                                refVarCreated = true;
                            }
#endif
                            // Add Source with holder
                            refVar = refVarCreated ? refVar : new ReferencedVariable(bind.name, bindsData[i].source.gameObject, holderRef, bindsData[i].source.holderName.Split(' ')[1], bindsData[i].source.variableName.Split(' ')[1]);
                            EzBind.AddSourceToBind(bind, refVar);
                        }
                        else
                        {
                            // Add Source w/o holder
                            refVar = EzBind.AddSourceToBind(bind, bindsData[i].source.component, bindsData[i].source.variableName.Split(' ')[1]); // Add Source from GO (new Bind)
                        }
                    }
                    else if(bindsData[i].source.useScriptableObject &&
                            bindsData[i].source.scriptableObject != null &&
                            !string.IsNullOrEmpty(bindsData[i].source.variableName) &&
                            !bindsData[i].source.variableName.Equals("None"))
                    {
                        if(EzBind.GetHolderReferenceIfExists(bindsData[i].source, bindsData[i].source.scriptableObject, out holderRef))
                        {
                            // Add the source with SO & holder; check if PlayMaker / Bolt
#if EZ_PLAYMAKER_SUPPORT
                            if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                            {
                                // Make refVar a reference to the FSM NamedVariable
                                fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(bindsData[i].source.variableName.Split(' ')[1]);
                                refVar = new ReferencedVariable(bind.name, bindsData[i].source.scriptableObject, fsmVar, fsmVar.Name, "RawValue");
                                refVarCreated = true;
                            }
#endif
#if EZ_BOLT_SUPPORT
                            if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                            {
                                // Make refVar a reference to the Bolt Variable
                                boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(bindsData[i].source.variableName.Split(' ')[1]);
                                refVar = new ReferencedVariable(bind.name, bindsData[i].source.scriptableObject, boltVar, boltVar.name, "value");
                                refVarCreated = true;
                            }
#endif
                            // Add source with holder
                            refVar = refVarCreated ? refVar : new ReferencedVariable(bind.name, bindsData[i].source.scriptableObject, holderRef, bindsData[i].source.holderName.Split(' ')[1], bindsData[i].source.variableName.Split(' ')[1]);
                            EzBind.AddSourceToBind(bind, refVar);
                        }
                        else
                        {
                            // Add source w/o holder
                            refVar = EzBind.AddSourceToBind(bind, bindsData[i].source.scriptableObject, bindsData[i].source.variableName.Split(' ')[1]); // Add Source from ScriptableObject (new Bind)
                        }
                    }
                    if(bindsData[i].observers != null && bindsData[i].observers.Count > 0)
                    {
                        refVarCreated = false;

                        for(int j = 0; j < bindsData[i].observers.Count; j++)
                        {
                            if(!bindsData[i].observers[j].useScriptableObject &&
                               bindsData[i].observers[j].gameObject != null &&
                               bindsData[i].observers[j].component != null &&
                               !string.IsNullOrEmpty(bindsData[i].observers[j].variableName) &&
                               !bindsData[i].observers[j].variableName.Equals("None"))
                            {
                                if(EzBind.GetHolderReferenceIfExists(bindsData[i].observers[j], bindsData[i].observers[j].component, out holderRef))
                                {
                                    // Add observer[j] with GO & holder; check if PlayMaker or Bolt
#if EZ_PLAYMAKER_SUPPORT
                                    if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                                    {
                                        // Make refVar a reference to the FSM NamedVariable
                                        fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(bindsData[i].observers[j].variableName.Split(' ')[1]);
                                        refVar = new ReferencedVariable(bind.name, bindsData[i].observers[j].gameObject, fsmVar, fsmVar.Name, "RawValue");
                                        refVarCreated = true;
                                    }
#endif
#if EZ_BOLT_SUPPORT
                                    if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                                    {
                                        // Make refVar a reference to the Bolt Variable
                                        boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(bindsData[i].observers[j].variableName.Split(' ')[1]);
                                        refVar = new ReferencedVariable(bind.name, bindsData[i].observers[j].gameObject, boltVar, boltVar.name, "value");
                                        refVarCreated = true;
                                    }
#endif
                                    // Add observer with holder
                                    refVar = refVarCreated ? refVar : new ReferencedVariable(bind.name, bindsData[i].observers[j].gameObject, holderRef, bindsData[i].observers[j].holderName.Split(' ')[1], bindsData[i].observers[j].variableName.Split(' ')[1]);
                                    EzBind.AddObserverToBind(bind, refVar);
                                }
                                else
                                {
                                    // Add observer w/o holder
                                    refVar = EzBind.AddObserverToBind(bind, bindsData[i].observers[j].component, bindsData[i].observers[j].variableName.Split(' ')[1]); // Add Observer from GO (new Bind)
                                }
                            }
                            else if(bindsData[i].observers[j].useScriptableObject &&
                                    bindsData[i].observers[j].scriptableObject != null &&
                                    !string.IsNullOrEmpty(bindsData[i].observers[j].variableName) &&
                                    !bindsData[i].observers[j].variableName.Equals("None"))
                            {
                                if(EzBind.GetHolderReferenceIfExists(bindsData[i].observers[j], bindsData[i].observers[j].scriptableObject, out holderRef))
                                {
                                    // Add observer[j] with SO & holder; check if PlayMaker or Bolt
#if EZ_PLAYMAKER_SUPPORT
                                    if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                                    {
                                        // Make refVar a reference to the FSM NamedVariable
                                        fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(bindsData[i].observers[j].variableName.Split(' ')[1]);
                                        refVar = new ReferencedVariable(bind.name, bindsData[i].observers[j].scriptableObject, fsmVar, fsmVar.Name, "RawValue");
                                        refVarCreated = true;
                                    }
#endif
#if EZ_BOLT_SUPPORT
                                    if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                                    {
                                        // Make refVar a reference to the Bolt Variable
                                        boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(bindsData[i].observers[j].variableName.Split(' ')[1]);
                                        refVar = new ReferencedVariable(bind.name, bindsData[i].observers[j].scriptableObject, boltVar, boltVar.name, "value");
                                        refVarCreated = true;
                                    }
#endif
                                    // Add observer with holder
                                    refVar = refVarCreated ? refVar : new ReferencedVariable(bind.name, bindsData[i].observers[j].scriptableObject, holderRef, bindsData[i].observers[j].holderName.Split(' ')[1], bindsData[i].observers[j].variableName.Split(' ')[1]);
                                    EzBind.AddObserverToBind(bind, refVar);
                                }
                                else
                                {
                                    // Add observer w/o holder
                                    refVar = EzBind.AddObserverToBind(bind, bindsData[i].observers[j].scriptableObject, bindsData[i].observers[j].variableName.Split(' ')[1]); // Add Observer from ScriptableObject (new Bind)
                                }
                            }
                        }
                    }
                }
                else
                {
                    // bind !=null, add to it
                    if(!bindsData[i].source.useScriptableObject &&
                        bindsData[i].source.gameObject != null &&
                        bindsData[i].source.component != null &&
                        !string.IsNullOrEmpty(bindsData[i].source.variableName) &&
                        !bindsData[i].source.variableName.Equals("None"))
                    {
                        if(EzBind.GetHolderReferenceIfExists(bindsData[i].source, bindsData[i].source.component, out holderRef))
                        {
                            // Add the source with GO & holder; check if PlayMaker / Bolt 
#if EZ_PLAYMAKER_SUPPORT
                            if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                            {
                                // Make refVar a reference to the FSM NamedVariable
                                fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(bindsData[i].source.variableName.Split(' ')[1]);
                                refVar = new ReferencedVariable(bind.name, bindsData[i].source.gameObject, fsmVar, fsmVar.Name, "RawValue");
                                refVarCreated = true;
                            }
#endif
#if EZ_BOLT_SUPPORT
                            if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                            {
                                // Make refVar a reference to the Bolt Variable
                                boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(bindsData[i].source.variableName.Split(' ')[1]);
                                refVar = new ReferencedVariable(bind.name, bindsData[i].source.gameObject, boltVar, boltVar.name, "value");
                                refVarCreated = true;
                            }
#endif
                            // Add Source with holder
                            refVar = refVarCreated ? refVar : new ReferencedVariable(bind.name, bindsData[i].source.gameObject, holderRef, bindsData[i].source.holderName.Split(' ')[1], bindsData[i].source.variableName.Split(' ')[1]);
                            EzBind.AddSourceToBind(bind, refVar);
                        }
                        else
                        {
                            // Add Source w/o holder
                            refVar = EzBind.AddSourceToBind(bind, bindsData[i].source.component, bindsData[i].source.variableName.Split(' ')[1]); // Add Source from GO (existing Bind)
                        }
                    }
                    else if(bindsData[i].source.useScriptableObject &&
                            bindsData[i].source.scriptableObject != null &&
                            !string.IsNullOrEmpty(bindsData[i].source.variableName) &&
                            !bindsData[i].source.variableName.Equals("None"))
                    {
                        if(EzBind.GetHolderReferenceIfExists(bindsData[i].source, bindsData[i].source.scriptableObject, out holderRef))
                        {
                            // Add the source with SO & holder; check if PlayMaker / Bolt
#if EZ_PLAYMAKER_SUPPORT
                            if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                            {
                                // Make refVar a reference to the FSM NamedVariable
                                fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(bindsData[i].source.variableName.Split(' ')[1]);
                                refVar = new ReferencedVariable(bind.name, bindsData[i].source.scriptableObject, fsmVar, fsmVar.Name, "RawValue");
                                refVarCreated = true;
                            }
#endif
#if EZ_BOLT_SUPPORT
                            if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                            {
                                // Make refVar a reference to the Bolt Variable
                                boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(bindsData[i].source.variableName.Split(' ')[1]);
                                refVar = new ReferencedVariable(bind.name, bindsData[i].source.scriptableObject, boltVar, boltVar.name, "value");
                                refVarCreated = true;
                            }
#endif
                            // Add source with holder
                            refVar = refVarCreated ? refVar : new ReferencedVariable(bind.name, bindsData[i].source.scriptableObject, holderRef, bindsData[i].source.holderName.Split(' ')[1], bindsData[i].source.variableName.Split(' ')[1]);
                            EzBind.AddSourceToBind(bind, refVar);
                        }
                        else
                        {
                            // Add source w/o holder
                            refVar = EzBind.AddSourceToBind(bind, bindsData[i].source.scriptableObject, bindsData[i].source.variableName.Split(' ')[1]); // Add Source from ScriptableObject (existing Bind)
                        }
                    }

                    if(bindsData[i].observers != null && bindsData[i].observers.Count > 0)
                    {
                        for(int j = 0; j < bindsData[i].observers.Count; j++)
                        {
                            if(!bindsData[i].observers[j].useScriptableObject &&
                               bindsData[i].observers[j].gameObject != null &&
                               bindsData[i].observers[j].component != null &&
                               !string.IsNullOrEmpty(bindsData[i].observers[j].variableName) &&
                               !bindsData[i].observers[j].variableName.Equals("None"))
                            {
                                if(EzBind.GetHolderReferenceIfExists(bindsData[i].observers[j], bindsData[i].observers[j].component, out holderRef))
                                {
                                    // Add observer[j] with GO & holder; check if PlayMaker or Bolt
#if EZ_PLAYMAKER_SUPPORT
                                    if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                                    {
                                        // Make refVar a reference to the FSM NamedVariable
                                        fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(bindsData[i].observers[j].variableName.Split(' ')[1]);
                                        refVar = new ReferencedVariable(bind.name, bindsData[i].observers[j].gameObject, fsmVar, fsmVar.Name, "RawValue");
                                        refVarCreated = true;
                                    }
#endif
#if EZ_BOLT_SUPPORT
                                    if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                                    {
                                        // Make refVar a reference to the Bolt Variable
                                        boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(bindsData[i].observers[j].variableName.Split(' ')[1]);
                                        refVar = new ReferencedVariable(bind.name, bindsData[i].observers[j].gameObject, boltVar, boltVar.name, "value");
                                        refVarCreated = true;
                                    }
#endif
                                    // Add observer with holder
                                    refVar = refVarCreated ? refVar : new ReferencedVariable(bind.name, bindsData[i].observers[j].gameObject, holderRef, bindsData[i].observers[j].holderName.Split(' ')[1], bindsData[i].observers[j].variableName.Split(' ')[1]);
                                    EzBind.AddObserverToBind(bind, refVar);
                                }
                                else
                                {
                                    // Add observer w/o holder
                                    refVar = EzBind.AddObserverToBind(bind, bindsData[i].observers[j].component, bindsData[i].observers[j].variableName.Split(' ')[1]); // Add Observer from GO (existing Bind)
                                }
                            }
                            else if(bindsData[i].observers[j].useScriptableObject &&
                                    bindsData[i].observers[j].scriptableObject != null &&
                                    !string.IsNullOrEmpty(bindsData[i].observers[j].variableName) &&
                                    !bindsData[i].observers[j].variableName.Equals("None"))
                            {
                                if(EzBind.GetHolderReferenceIfExists(bindsData[i].observers[j], bindsData[i].observers[j].scriptableObject, out holderRef))
                                {
                                    // Add observer[j] with SO & holder; check if PlayMaker or Bolt
#if EZ_PLAYMAKER_SUPPORT
                                    if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                                    {
                                        // Make refVar a reference to the FSM NamedVariable
                                        fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(bindsData[i].observers[j].variableName.Split(' ')[1]);
                                        refVar = new ReferencedVariable(bind.name, bindsData[i].observers[j].scriptableObject, fsmVar, fsmVar.Name, "RawValue");
                                        refVarCreated = true;
                                    }
#endif
#if EZ_BOLT_SUPPORT
                                    if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                                    {
                                        // Make refVar a reference to the Bolt Variable
                                        boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(bindsData[i].observers[j].variableName.Split(' ')[1]);
                                        refVar = new ReferencedVariable(bind.name, bindsData[i].observers[j].scriptableObject, boltVar, boltVar.name, "value");
                                        refVarCreated = true;
                                    }
#endif
                                    // Add observer with holder
                                    refVar = refVarCreated ? refVar : new ReferencedVariable(bind.name, bindsData[i].observers[j].scriptableObject, holderRef, bindsData[i].observers[j].holderName.Split(' ')[1], bindsData[i].observers[j].variableName.Split(' ')[1]);
                                    EzBind.AddObserverToBind(bind, refVar);
                                }
                                else
                                {
                                    // Add observer w/o holder
                                    refVar = EzBind.AddObserverToBind(bind, bindsData[i].observers[j].scriptableObject, bindsData[i].observers[j].variableName.Split(' ')[1]); // Add Observer from ScriptableObject (existing Bind)
                                }
                            }
                            refVarsCreatedBySelf.Add(new KeyValuePair<Bind, ReferencedVariable>(bind, refVar));
                        }
                    }

                    bind.AddListener(bindsData[i].InvokeOwnEvent);
                    listenersAddedBySelf.Add(new KeyValuePair<Bind, UnityAction>(bind, bindsData[i].InvokeOwnEvent));
                }
            }
        }

        void RemoveOwnBinds()
        {
            int i;
            for(i = 0; i < bindsCreatedBySelf.Count; i++)
            {
                EzBind.RemoveBind(bindsCreatedBySelf[i]);
            }
            bindsCreatedBySelf = null;

            for(i = 0; i < refVarsCreatedBySelf.Count; i++)
            {
                EzBind.RemoveObserverFromBind(refVarsCreatedBySelf[i].Key, refVarsCreatedBySelf[i].Value);
            }
            refVarsCreatedBySelf = null;

            for(i = 0; i < listenersAddedBySelf.Count; i++)
            {
                EzBind.RemoveListenerFromBind(listenersAddedBySelf[i].Key, listenersAddedBySelf[i].Value);
            }
            listenersAddedBySelf = null;

            shouldCleanup = false;
        }
    }
}