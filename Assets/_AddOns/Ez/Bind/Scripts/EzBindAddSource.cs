// Copyright (c) 2016 - 2018 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using System.Collections;
using Ez.Binding.Vars;
using UnityEngine;

namespace Ez.Binding.Internal
{
    public class EzBindAddSource : MonoBehaviour
    {
        /// Inspector Data.
        public string bindName;
        public BoundItem source;

        private Bind bind = null;
        private ReferencedVariable sourceVar = null;

        private void Start()
        {
            if(string.IsNullOrEmpty(bindName))
            {
                // Bind does not have a name
                this.enabled = false;
                throw new UnityException(@"[Ez][Bind] Add Source on the """ + gameObject.name + @""" GameObject must have a bind name assigned! The Component is disabling itself.");
            }

            if(source.gameObject != gameObject || source.component == null || string.IsNullOrEmpty(source.variableName) || source.variableName.Equals("None"))
            {
                // Bind was not configured properly
                this.enabled = false;
                throw new UnityException(@"[Ez][Bind] Add Source on the """ + gameObject.name + @""" GameObject was not configured correctly in the inspector. The Component is disabling itself.");
            }
            StartCoroutine(WaitOneFrameAndAddSource());
        }

        private IEnumerator WaitOneFrameAndAddSource()
        {
            yield return null;
            AddSelfSource();
        }

        private void AddSelfSource()
        {
            bind = EzBind.FindBindByName(bindName);
            if(bind == null)
            {
                bind = EzBind.AddBind(bindName);
            }

            object holderRef;
            bool sourceVarCreated = false;
#if EZ_PLAYMAKER_SUPPORT
            HutongGames.PlayMaker.NamedVariable fsmVar;
#endif
#if EZ_BOLT_SUPPORT
            Bolt.VariableDeclaration boltVar;
#endif

            if(EzBind.GetHolderReferenceIfExists(source, source.component, out holderRef))
            {
#if EZ_PLAYMAKER_SUPPORT
                if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                {
                    // Make sourceVar a reference to the FSM NamedVariable
                    fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(source.variableName.Split(' ')[1]);
                    sourceVar = new ReferencedVariable(bindName, source.gameObject, fsmVar, fsmVar.Name, "RawValue");
                    sourceVarCreated = true;
                }
#endif
#if EZ_BOLT_SUPPORT
                if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                {
                    // Make refVar a reference to the Bolt Variable
                    boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(source.variableName.Split(' ')[1]);
                    sourceVar = new ReferencedVariable(bindName, source.gameObject, boltVar, boltVar.name, "value");
                    sourceVarCreated = true;
                }
#endif
                // Add Source with holder
                sourceVar = sourceVarCreated ? sourceVar : new ReferencedVariable(bindName, source.gameObject, holderRef, source.holderName.Split(' ')[1], source.variableName.Split(' ')[1]);
                if(!EzBind.AddSourceToBind(bind, sourceVar))
                {
                    this.enabled = false;
                    throw new UnityException(@"[Ez][Bind] Add Source on the """ + gameObject.name + @""" GameObject could not attach a new source to bind named """ + bindName + @"""");
                }
            }
            else
            {
                // Add source w/o holder
                sourceVar = EzBind.AddSourceToBind(bind, source.component, source.variableName.Split(' ')[1]);
                if(sourceVar == null)
                {
                    this.enabled = false;
                    throw new UnityException(@"[Ez][Bind] Add Source on the """ + gameObject.name + @""" GameObject could not attach a new source to bind named """ + bindName + @"""");
                }
            }
        }

    }
}