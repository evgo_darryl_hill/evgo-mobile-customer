// Copyright (c) 2016 - 2018 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using System.Collections;
using Ez.Binding.Vars;
using UnityEngine;

namespace Ez.Binding.Internal
{
    public class EzBindAddObserver : MonoBehaviour
    {
        /// Inspector Data.
        public string bindName;
        public BoundItem observer;

        // Should the added observer update itself if this GameObject is not enabled?
        public bool updateWhenDisabled = true;

        private Bind bind = null;
        private ReferencedVariable obsVar = null;
        private bool isInitialized = false;

        private void Start()
        {
            if(string.IsNullOrEmpty(bindName))
            {
                this.enabled = false;
                throw new UnityException(@"[Ez][Bind] Add Observer on the """ + gameObject.name + @""" GameObject must have a bind name assigned! The Component is disabling itself.");
            }

            if(observer.gameObject != gameObject || observer.component == null || string.IsNullOrEmpty(observer.variableName) || observer.variableName.Equals("None"))
            {
                // Bind was not configured properly
                this.enabled = false;
                throw new UnityException(@"[Ez][Bind] Add Observer on the """ + gameObject.name + @""" GameObject was not configured correctly in the inspector. The Component is disabling itself.");
            }
            StartCoroutine(WaitOneFrameAndAddObserver());
        }

        private void OnEnable()
        {
            if(!isInitialized) { return; } // Start() didn't run yet or there was an error
            if(updateWhenDisabled) { return; } // Only add once, from Start()

            EzBind.AddObserverToBind(bind, obsVar);
        }

        private void OnDisable()
        {
            if(!isInitialized) { return; } // Start() didn't run yet or there was an error
            if(updateWhenDisabled) { return; } // Always receive updates, do nothing OnDisable() 

            EzBind.RemoveObserverFromBind(bind, obsVar);
        }

        private IEnumerator WaitOneFrameAndAddObserver()
        {
            yield return null;
            AddOwnObserver();
        }

        private void AddOwnObserver()
        {
            bind = EzBind.FindBindByName(bindName);
            if(bind == null)
            {
                bind = EzBind.AddBind(bindName);
            }

            object holderRef;
            bool obsVarCreated = false;
#if EZ_PLAYMAKER_SUPPORT
            HutongGames.PlayMaker.NamedVariable fsmVar;
#endif
#if EZ_BOLT_SUPPORT
            Bolt.VariableDeclaration boltVar;
#endif
            if(EzBind.GetHolderReferenceIfExists(observer, observer.component, out holderRef))
            {
                // Add observer[j] with GO & holder; check if PlayMaker or Bolt
#if EZ_PLAYMAKER_SUPPORT
                if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                {
                    // Make refVar a reference to the FSM NamedVariable
                    fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(observer.variableName.Split(' ')[1]);
                    obsVar = new ReferencedVariable(bindName, observer.gameObject, fsmVar, fsmVar.Name, "RawValue");
                    obsVarCreated = true;
                }
#endif
#if EZ_BOLT_SUPPORT
                if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                {
                    // Make refVar a reference to the Bolt Variable
                    boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(observer.variableName.Split(' ')[1]);
                    obsVar = new ReferencedVariable(bindName, observer.gameObject, boltVar, boltVar.name, "value");
                    obsVarCreated = true;
                }
#endif
                // Add observer with holder
                obsVar = obsVarCreated ? obsVar : new ReferencedVariable(bindName, observer.gameObject, holderRef, observer.holderName.Split(' ')[1], observer.variableName.Split(' ')[1]);
                if(!EzBind.AddObserverToBind(bind, obsVar))
                {
                    this.enabled = false;
                    throw new UnityException(@"[Ez][Bind] Add Observer on the """ + gameObject.name + @""" GameObject could not attach a new observer to bind named """ + bindName + @"""");
                }
            }
            else
            {
                // Add observer w/o holder
                obsVar = EzBind.AddObserverToBind(bind, observer.component, observer.variableName.Split(' ')[1]);
                if(obsVar == null)
                {
                    this.enabled = false;
                    throw new UnityException(@"[Ez][Bind] Add Observer on the """ + gameObject.name + @""" GameObject could not attach a new observer to bind named """ + bindName + @"""");
                }
            }
            isInitialized = true;
        }
    }
}