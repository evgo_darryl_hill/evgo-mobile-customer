// Copyright (c) 2016 - 2018 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ez.Binding.Vars;
using UnityEngine;
using UnityEngine.Events;
using QuickEngine.Extensions;

namespace Ez.Binding
{
    public class EzBind : MonoBehaviour
    {
        /// <summary>
        /// Event invoked on (late) update to trigger all listening binds to check if the source has changed.
        /// </summary>
        protected UnityEvent onUpdate = new UnityEvent();
        /// <summary>
        /// Dinctionary holding all the binds, using their names as key.
        /// </summary>
        protected Dictionary<string, Bind> bindsHolder = new Dictionary<string, Bind>();
        public Dictionary<string, Bind> BindsHolder { get { return bindsHolder; } }

        #region STATIC UTILS
        /// <summary>
        /// Creates a new bind with the specified name and returns it.
        /// <para>If the operation fails, returns null.</para>
        /// </summary>
        /// <param name="newBindName">The bind's name. It must be unique among all binds.</param>
        /// <returns></returns>
        public static Bind AddBind(string newBindName)
        {
            var bind = new Bind(newBindName);
            return AddBind(bind) ? bind : null;
        }
        /// <summary>
        /// Creates a new bind with the specified name and having the specified initial value, then returns it.
        /// <para>If the operation fails, returns null.</para>
        /// </summary>
        /// <param name="newBindName">The bind's name. It must be unique among all binds.</param>
        /// <param name="newBindValue">The bind's initial value.</param>
        /// <returns></returns>
        public static Bind AddBind(string newBindName, object newBindValue)
        {
            var bind = new Bind(newBindName, newBindValue);
            return AddBind(bind) ? bind : null;
        }
        /// <summary>
        /// Creates a new bind with the specified name and returns it.
        /// <para>If the operation fails, returns null.</para>
        /// </summary>
        /// <param name="newBindName">The bind's name. It must be unique among all binds.</param>
        /// <param name="onValueChanged">Event invoked when the bind's value changes.</param>
        /// <returns></returns>
        public static Bind AddBind(string newBindName, UnityEvent onValueChanged)
        {
            var bind = new Bind(newBindName, onValueChanged);
            return AddBind(bind) ? bind : null;
        }
        /// <summary>
        /// Creates a new bind with the specified name and having the specified initial value, then returns it.
        /// <para>If the operation fails, returns null.</para>
        /// </summary>
        /// <param name="newBindName">The bind's name. It must be unique among all binds.</param>
        /// <param name="newBindValue">The bind's initial value.</param>
        /// <param name="onValueChanged">Event invoked when the bind's value changes.</param>
        /// <returns></returns>
        public static Bind AddBind(string newBindName, object newBindValue, UnityEvent onValueChanged)
        {
            var bind = new Bind(newBindName, newBindValue, onValueChanged);
            return AddBind(bind) ? bind : null;
        }
        /// <summary>
        /// Private method that adds the bind to the dictionary.
        /// <para>Returns true if the operation is successful, false otherwise.</para>
        /// </summary>
        /// <param name="newBind">Bind to be added</param>
        /// <returns>Returns <c>true</c> if the operation is successful, <c>false</c> otherwise</returns>
        private static bool AddBind(Bind newBind)
        {
            try
            {
                EzBind.Instance.bindsHolder.Add(newBind.name, newBind);
            }
            catch(Exception e)
            {
                Debug.LogError(@"[Ez][Bind] - Error when adding a new bind named """ + newBind.name + @""" : " + e.Message);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Removes the bind specified by name and destroys it.
        /// </summary>
        /// <param name="bindName">Bind's name.</param>
        public static void RemoveBind(string bindName)
        {
            Bind var;
            if(EzBind.Instance.bindsHolder.TryGetValue(bindName, out var))
            {
                EzBind.Instance.bindsHolder.Remove(bindName);
                var.Dispose();
            }
        }
        /// <summary>
        /// Removes the bind specified by reference and destroys it.
        /// </summary>
        /// <param name="bind">Bind's reference.</param>
        public static void RemoveBind(Bind bind)
        {
            Bind var;
            if(EzBind.Instance.bindsHolder.TryGetValue(bind.name, out var))
            {
                EzBind.Instance.bindsHolder.Remove(bind.name);
                var.Dispose();
            }
        }
        /// <summary>
        /// Adds a new observer to the referenced bind and returns a reference to the new observer.
        /// <para>If unsuccessful, returns null.</para>
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="parent">Observer's Component.</param>
        /// <param name="observerName">Observer's name (field or property name)</param>
        /// <returns></returns>
        public static ReferencedVariable AddObserverToBind(Bind bind, UnityEngine.Object parent, string observerName)
        {
            if(bind == null)
            {
                return null;
            }
            var refVar = new ReferencedVariable(bind.name, parent, observerName);
            return bind.AddObserver(refVar) ? refVar : null;
        }
        /// <summary>
        ///  Adds a new observer to the specified bind and returns a reference to the new observer.
        ///  <para>If unsuccessful, returns null.</para>
        /// </summary>
        /// <param name="bindName">Bind's name.</param>
        /// <param name="parent">Observer's Component.</param>
        /// <param name="observerName">Observer's name (field or property name)</param>
        /// <returns></returns>
        public static ReferencedVariable AddObserverToBind(string bindName, UnityEngine.Object parent, string observerName)
        {
            var bind = FindBindByName(bindName);
            return bind != null ? AddObserverToBind(bind, parent, observerName) : null;
        }
        /// <summary>
        /// Add a new observer to the referenced bind. The observer is a reference to a ReferencedVariable previously created.
        /// <para>Returns true if the operation was successful, false otherwise.</para>
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="observerVar">Observer reference.</param>
        /// <returns></returns>
        public static bool AddObserverToBind(Bind bind, ReferencedVariable observerVar)
        {
            if(bind == null)
            {
                return false;
            }
            return bind.AddObserver(observerVar);
        }
        /// <summary>
        /// Add a new observer to the specified bind. The observer is a reference to a ReferencedVariable previously created.
        /// <para>Returns true if the operation was successful, false otherwise.</para>
        /// </summary>
        /// <param name="bindName">Bind's name.</param>
        /// <param name="observerVar">Observer reference.</param>
        /// <returns></returns>
        public static bool AddObserverToBind(string bindName, ReferencedVariable observerVar)
        {
            var bind = FindBindByName(bindName);
            return bind != null ? AddObserverToBind(bind, observerVar) : false;
        }
        /// <summary>
        /// Adds a new source to the referenced bind and returns a reference to the new source. 
        /// <para>If another source was defined, it is replaced. If unsuccessful, returns null.</para>
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="parent">Source's Component.</param>
        /// <param name="sourceName">Source's name (field or property name)</param>
        /// <returns></returns>
        public static ReferencedVariable AddSourceToBind(Bind bind, UnityEngine.Object parent, string sourceName)
        {
            if(bind == null)
            {
                return null;
            }
            var refVar = new ReferencedVariable(bind.name, parent, sourceName);
            return bind.AddSourceVariable(refVar) ? refVar : null;
        }
        /// <summary>
        /// Adds a new source to the specified bind and returns a reference to the new source. 
        /// <para>If another source was defined, it is replaced. If unsuccessful, returns null.</para>
        /// </summary>
        /// <param name="bindName">Bind's name.</param>
        /// <param name="parent">Source's Component.</param>
        /// <param name="observerName">Source's name (field or property name)</param>
        /// <returns></returns>
        public static ReferencedVariable AddSourceToBind(string bindName, UnityEngine.Object parent, string observerName)
        {
            var bind = FindBindByName(bindName);
            return bind != null ? AddSourceToBind(bind, parent, observerName) : null;
        }
        /// <summary>
        /// Adds a new source to the referenced bind. The source is a reference to a ReferenceVariable previously created. 
        /// <para>If another source was defined, it is replaced. Returns true if successful, false otherwise.</para>
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="refVar">Source reference.</param>
        /// <returns></returns>
        public static bool AddSourceToBind(Bind bind, ReferencedVariable sourceVar)
        {
            if(bind == null)
            {
                return false;
            }
            return bind.AddSourceVariable(sourceVar);
        }
        /// <summary>
        /// Adds a new source to the specified bind. The source is a reference to a ReferenceVariable previously created.
        /// <para>If another source was defined, it is replaced. Returns true if successful, false otherwise.</para>
        /// </summary>
        /// <param name="bindName">Bind's name.</param>
        /// <param name="sourceVar">Source reference.</param>
        /// <returns></returns>
        public static bool AddSourceToBind(string bindName, ReferencedVariable sourceVar)
        {
            var bind = FindBindByName(bindName);
            return bind != null ? AddSourceToBind(bind, sourceVar) : false;
        }
        /// <summary>
        /// Finds and returns an existing bind by it's name. 
        /// <para>If the bind doesn't exist, returns null.</para>
        /// </summary>
        /// <param name="bindName">The bind's name.</param>
        /// <returns>The bind, if found, <c>null</c> otherwise.</returns>
        public static Bind FindBindByName(string bindName)
        {
            Bind var;
            return EzBind.Instance.bindsHolder.TryGetValue(bindName, out var) ? var : null;
        }
        /// <summary>
        /// Removes an observer from the bind.
        /// <para>Returns true if successful, false otherwise.</para>
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="refVar">Observer reference.</param>
        /// <returns>Returns <c>true</c> if the operation was successful, <c>false</c> otherwise.</returns>
        public static bool RemoveObserverFromBind(Bind bind, ReferencedVariable refVar)
        {
            return (bind != null && refVar != null) ? bind.RemoveObserver(refVar) : false;
        }
        /// <summary>
        /// Removes an observer from the bind.
        /// <para>Returns true if successful, false otherwise.</para>
        /// </summary>
        /// <param name="bindName">The bind's name.</param>
        /// <param name="refVar">Observer reference.</param>
        /// <returns>Returns <c>true</c> if the operation was successful, <c>false</c> otherwise.</returns>
        public static bool RemoveObserverFromBind(string bindName, ReferencedVariable refVar)
        {
            return RemoveObserverFromBind(FindBindByName(bindName), refVar);
        }
        /// <summary>
        /// Adds a listener method to a bind. 
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="listener">Listener method.</param>
        public static void AddListenerToBind(Bind bind, UnityAction listener)
        {
            bind.AddListener(listener);
        }
        /// <summary>
        /// Adds a listener method to a bind.
        /// <para> Returns true if successful, false otherwise.</para>
        /// </summary>
        /// <param name="bindName">The bind's name.</param>
        /// <param name="listener">Listener method.</param>
        /// <returns>Returns <c>true</c> if successful, <c>false</c> otherwise.</returns>
        public static bool AddListenerToBind(string bindName, UnityAction listener)
        {
            var bind = FindBindByName(bindName);
            if(bind != null)
            {
                AddListenerToBind(bind, listener);
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Removes the listener from the bind.
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="listener">Listener method.</param>
        public static void RemoveListenerFromBind(Bind bind, UnityAction listener)
        {
            bind.RemoveListener(listener);
        }
        /// <summary>
        /// Removes the listener method from the bind.
        /// <para> Returns true if successful, false otherwise.</para>
        /// </summary>
        /// <param name="bindName">The bind's name.</param>
        /// <param name="listener">Listener method.</param>
        /// <returns></returns>
        public static bool RemoveListenerFromBind(string bindName, UnityAction listener)
        {
            var bind = FindBindByName(bindName);
            if(bind != null)
            {
                RemoveListenerFromBind(bind, listener);
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Helper method to find a variable's name at runtime. The variable MUST be passed as an annonymous type!
        /// <para>Usage example: var myVariable; EzBind.GetObserverName(new { myVariable });</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string GetReferenceName<T>(T item)
        {
            if(item == null)
            {
                Debug.LogError("[Ez][Bind] You are trying to get a reference name for a null reference! Empty string is passed instead.");
                return string.Empty;
            }

            if(!IsAnonymousType(typeof(T)))
            {
                Debug.LogError("[Ez][Bind] You are trying to get a reference name for a non-anonymous type! " +
                    " To find the name, you muse pass an anonymous type as a parameter like this: GetTargetName(new { <target> });");
                return string.Empty;
            }
            return typeof(T).GetProperties()[0].Name;
        }
        /// <summary>
        /// Checks if the given type is Anonymous or not.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static bool IsAnonymousType(Type type)
        {
            return type == null ?
                false :
                Attribute.IsDefined(type, typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), false)
                && type.IsGenericType
                && type.Name.Contains("Anon")
                && (type.Name.StartsWith("<>", StringComparison.OrdinalIgnoreCase) || type.Name.StartsWith("VB$", StringComparison.OrdinalIgnoreCase))
                && (type.Attributes & System.Reflection.TypeAttributes.NotPublic) == System.Reflection.TypeAttributes.NotPublic;
        }
        #endregion //STATIC UTILS

        #region Singleton
        protected EzBind() { }
        private static EzBind _instance;
        public static EzBind Instance
        {
            get
            {
                if(_instance == null)
                {
                    if(applicationIsQuitting) { return null; }
                    GameObject singleton = new GameObject("(singleton) " + typeof(EzBind).ToString());
                    _instance = singleton.AddComponent<EzBind>();
                    DontDestroyOnLoad(singleton);
                }
                return _instance;
            }
        }

        private static bool applicationIsQuitting = false;
        private void OnApplicationQuit()
        {
            applicationIsQuitting = true;
        }
        #endregion

        private void Awake()
        {
            if(_instance != null)
            {
                Debug.Log("[Ez][Bind] There cannot be two EzBinds active at the same time. Destryoing this one!");
                Destroy(gameObject);
                return;
            }
            _instance = this;
            DontDestroyOnLoad(gameObject);

            CreateBinds();

            bindsData = null;
            auxHolderValuesList.Clear();
        }

        /// <summary>
        /// Helper method allowing binds to subscribe to the onUpdate event.
        /// </summary>
        /// <param name="listener">Listener method.</param>
        public void SubscribeToUpdates(UnityAction listener)
        {
            onUpdate.AddListener(listener);
        }
        /// <summary>
        /// Helper method allowing binds to subscribe to the onUpdate event.
        /// </summary>
        /// <param name="listener">Listener method.</param>
        public void UnsubscribeFromUpdates(UnityAction listener)
        {
            onUpdate.RemoveListener(listener);
        }

        private void LateUpdate()
        {
            onUpdate.Invoke();
        }

        public List<BindData> bindsData;
        public const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static;
        private static List<object> auxHolderValuesList = new List<object>();

        public static bool GetHolderReferenceIfExists(BoundItem item, UnityEngine.Object parent, out object holder)
        {
            holder = null;
            if(!item.useHolder) { return false; }
            auxHolderValuesList.Clear();

#if EZ_PLAYMAKER_SUPPORT
            if(!item.useScriptableObject) { if(parent.GetType() == typeof(PlayMakerFSM) && item.holderName == "FsmVariables Variables") { holder = ((PlayMakerFSM) parent).FsmVariables; return true; } }
            else if(parent.GetType() == typeof(PlayMakerGlobals)) { holder = PlayMakerGlobals.Instance.Variables; return true; }
#endif
#if EZ_BOLT_SUPPORT
            if(item.useScriptableObject && parent.GetType() == typeof(Bolt.VariablesAsset))
            {
                if(item.scriptableObject.name == "ApplicationVariables") { holder = Bolt.Variables.Application; return true; }
                else if(item.scriptableObject.name == "SavedVariables") { holder = Bolt.Variables.Saved; return true; }
            }
#endif

            string holderName = item.holderName.Split(' ')[1];
            auxHolderValuesList.AddRange
                (
                   parent.GetType().GetFields(flags)
                   .Where(x => x.Name.Equals(holderName))
                   .Select(x => x.GetValue(parent))
                );
            if(auxHolderValuesList.Count == 1) { holder = auxHolderValuesList[0]; return true; }

            auxHolderValuesList.AddRange
                   (
                      parent.GetType().GetProperties(flags)
                      .Where(x => x.Name.Equals(holderName))
                      .Select(x => x.GetValue(parent, null))
                   );

            if(auxHolderValuesList.Count == 1) { holder = auxHolderValuesList[0]; return true; }

            return false;
        }

        private void CreateBinds()
        {
            if(bindsData == null || bindsData.Count == 0) { return; }

            Bind tempBind;
            ReferencedVariable refVar = null;
            object holderRef;
            bool refVarCreated;
#if EZ_PLAYMAKER_SUPPORT
            HutongGames.PlayMaker.NamedVariable fsmVar;
#endif
#if EZ_BOLT_SUPPORT
            Bolt.VariableDeclaration boltVar;
#endif

            for(int i = 0; i < bindsData.Count; i++)
            {
                if(string.IsNullOrEmpty(bindsData[i].bindName) || bindsData[i].bindName.Equals("UnnamedBind")) { continue; }
                tempBind = AddBind(bindsData[i].bindName, bindsData[i].OnValueChanged); //Create Bind

                if(tempBind == null) { continue; }

                refVarCreated = false;

                if(!bindsData[i].source.useScriptableObject &&
                    bindsData[i].source.gameObject != null &&
                    bindsData[i].source.component != null &&
                    !string.IsNullOrEmpty(bindsData[i].source.variableName) &&
                    !bindsData[i].source.variableName.Equals("None"))
                {
                    if(GetHolderReferenceIfExists(bindsData[i].source, bindsData[i].source.component, out holderRef))
                    {
                        // Add the source with GO & holder; check if PlayMaker / Bolt 
#if EZ_PLAYMAKER_SUPPORT
                        if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                        {
                            // Make refVar a reference to the FSM NamedVariable
                            fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(bindsData[i].source.variableName.Split(' ')[1]);
                            refVar = new ReferencedVariable(tempBind.name, bindsData[i].source.gameObject, fsmVar, fsmVar.Name, "RawValue");
                            refVarCreated = true;
                        }
#endif
#if EZ_BOLT_SUPPORT
                        if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                        {
                            // Make refVar a reference to the Bolt Variable
                            boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(bindsData[i].source.variableName.Split(' ')[1]);
                            refVar = new ReferencedVariable(tempBind.name, bindsData[i].source.gameObject, boltVar, boltVar.name, "value");
                            refVarCreated = true;
                        }
#endif
                        // Add Source with holder
                        refVar = refVarCreated ? refVar : new ReferencedVariable(tempBind.name, bindsData[i].source.gameObject, holderRef, bindsData[i].source.holderName.Split(' ')[1], bindsData[i].source.variableName.Split(' ')[1]);
                        AddSourceToBind(tempBind, refVar);
                    }
                    else
                    {
                        // Add Source w/o holder
                        AddSourceToBind(tempBind, bindsData[i].source.component, bindsData[i].source.variableName.Split(' ')[1]); // Add Source from GO
                    }
                }
                else if(bindsData[i].source.useScriptableObject &&
                        bindsData[i].source.scriptableObject != null &&
                        !string.IsNullOrEmpty(bindsData[i].source.variableName) &&
                        !bindsData[i].source.variableName.Equals("None"))
                {
                    if(GetHolderReferenceIfExists(bindsData[i].source, bindsData[i].source.scriptableObject, out holderRef))
                    {
                        // Add the source with SO & holder; check if PlayMaker / Bolt
#if EZ_PLAYMAKER_SUPPORT
                        if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                        {
                            // Make refVar a reference to the FSM NamedVariable
                            fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(bindsData[i].source.variableName.Split(' ')[1]);
                            refVar = new ReferencedVariable(tempBind.name, bindsData[i].source.scriptableObject, fsmVar, fsmVar.Name, "RawValue");
                            refVarCreated = true;
                        }
#endif
#if EZ_BOLT_SUPPORT
                        if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                        {
                            // Make refVar a reference to the Bolt Variable
                            boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(bindsData[i].source.variableName.Split(' ')[1]);
                            refVar = new ReferencedVariable(tempBind.name, bindsData[i].source.scriptableObject, boltVar, boltVar.name, "value");
                            refVarCreated = true;
                        }
#endif
                        // Add source with holder
                        refVar = refVarCreated ? refVar : new ReferencedVariable(tempBind.name, bindsData[i].source.scriptableObject, holderRef, bindsData[i].source.holderName.Split(' ')[1], bindsData[i].source.variableName.Split(' ')[1]);
                        AddSourceToBind(tempBind, refVar);
                    }
                    else
                    {
                        // Add source w/o holder
                        AddSourceToBind(tempBind, bindsData[i].source.scriptableObject, bindsData[i].source.variableName.Split(' ')[1]); // Add Source from ScriptableObject
                    }
                }
                if(bindsData[i].observers != null && bindsData[i].observers.Count > 0)
                {
                    for(int j = 0; j < bindsData[i].observers.Count; j++)
                    {
                        refVarCreated = false;

                        if(!bindsData[i].observers[j].useScriptableObject &&
                           bindsData[i].observers[j].gameObject != null &&
                           bindsData[i].observers[j].component != null &&
                           !string.IsNullOrEmpty(bindsData[i].observers[j].variableName) &&
                           !bindsData[i].observers[j].variableName.Equals("None"))
                        {
                            if(GetHolderReferenceIfExists(bindsData[i].observers[j], bindsData[i].observers[j].component, out holderRef))
                            {
                                // Add observer[j] with GO & holder; check if PlayMaker or Bolt
#if EZ_PLAYMAKER_SUPPORT
                                if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                                {
                                    // Make refVar a reference to the FSM NamedVariable
                                    fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(bindsData[i].observers[j].variableName.Split(' ')[1]);
                                    refVar = new ReferencedVariable(tempBind.name, bindsData[i].observers[j].gameObject, fsmVar, fsmVar.Name, "RawValue");
                                    refVarCreated = true;
                                }
#endif
#if EZ_BOLT_SUPPORT
                                if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                                {
                                    // Make refVar a reference to the Bolt Variable
                                    boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(bindsData[i].observers[j].variableName.Split(' ')[1]);
                                    refVar = new ReferencedVariable(tempBind.name, bindsData[i].observers[j].gameObject, boltVar, boltVar.name, "value");
                                    refVarCreated = true;
                                }
#endif
                                // Add observer with holder
                                refVar = refVarCreated ? refVar : new ReferencedVariable(tempBind.name, bindsData[i].observers[j].gameObject, holderRef, bindsData[i].observers[j].holderName.Split(' ')[1], bindsData[i].observers[j].variableName.Split(' ')[1]);
                                AddObserverToBind(tempBind, refVar);
                            }
                            else
                            {
                                // Add observer w/o holder
                                AddObserverToBind(tempBind, bindsData[i].observers[j].component, bindsData[i].observers[j].variableName.Split(' ')[1]); // Add Observer from GO
                            }
                        }
                        else if(bindsData[i].observers[j].useScriptableObject &&
                                bindsData[i].observers[j].scriptableObject != null &&
                                !string.IsNullOrEmpty(bindsData[i].observers[j].variableName) &&
                                !bindsData[i].observers[j].variableName.Equals("None"))
                        {
                            if(GetHolderReferenceIfExists(bindsData[i].observers[j], bindsData[i].observers[j].scriptableObject, out holderRef))
                            {
                                // Add observer[j] with SO & holder; check if PlayMaker or Bolt
#if EZ_PLAYMAKER_SUPPORT
                                if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                                {
                                    // Make refVar a reference to the FSM NamedVariable
                                    fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(bindsData[i].observers[j].variableName.Split(' ')[1]);
                                    refVar = new ReferencedVariable(tempBind.name, bindsData[i].observers[j].scriptableObject, fsmVar, fsmVar.Name, "RawValue");
                                    refVarCreated = true;
                                }
#endif
#if EZ_BOLT_SUPPORT
                                if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                                {
                                    // Make refVar a reference to the Bolt Variable
                                    boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(bindsData[i].observers[j].variableName.Split(' ')[1]);
                                    refVar = new ReferencedVariable(tempBind.name, bindsData[i].observers[j].scriptableObject, boltVar, boltVar.name, "value");
                                    refVarCreated = true;
                                }
#endif
                                // Add observer with holder
                                refVar = refVarCreated ? refVar : new ReferencedVariable(tempBind.name, bindsData[i].observers[j].scriptableObject, holderRef, bindsData[i].observers[j].holderName.Split(' ')[1], bindsData[i].observers[j].variableName.Split(' ')[1]);
                                AddObserverToBind(tempBind, refVar);
                            }
                            else
                            {
                                // Add observer w/o holder
                                AddObserverToBind(tempBind, bindsData[i].observers[j].scriptableObject, bindsData[i].observers[j].variableName.Split(' ')[1]); // Add Observer from ScriptableObject
                            }
                        }
                    }
                }

            }
        }
    }

    [Serializable]
    public class BindData
    {
        public string bindName = string.Empty;
        public BoundItem source = new BoundItem();
        public List<BoundItem> observers = new List<BoundItem>();
        public UnityEvent OnValueChanged = new UnityEvent();
        public void InvokeOwnEvent()
        {
            OnValueChanged.Invoke();
        }
    }

    [Serializable]
    public class BoundItem
    {
        public GameObject gameObject;
        public Component component;
        public bool useScriptableObject;
        public ScriptableObject scriptableObject;
        public bool useHolder;
        public string holderName;
        public string variableName;

        public BoundItem()
        {
            Reset();
        }

        public BoundItem(GameObject gameObject, Component component, string variableName)
        {
            this.gameObject = gameObject;
            this.component = component;
            this.useScriptableObject = false;
            this.scriptableObject = null;
            this.useHolder = false;
            this.holderName = string.Empty;
            this.variableName = variableName;
        }

        public BoundItem(GameObject gameObject, Component component, object holder, string holderName, string variableName)
        {
            this.gameObject = gameObject;
            this.component = component;
            this.useScriptableObject = false;
            this.scriptableObject = null;
            this.useHolder = true;
            this.holderName = holderName;
            this.variableName = variableName;
        }

        public BoundItem(ScriptableObject scriptableObject, string variableName)
        {
            this.gameObject = null;
            this.component = null;
            this.useScriptableObject = true;
            this.scriptableObject = scriptableObject;
            this.useHolder = false;
            this.holderName = string.Empty;
            this.variableName = variableName;
        }

        public BoundItem(ScriptableObject scriptableObject, object holder, string holderName, string variableName)
        {
            this.gameObject = null;
            this.component = null;
            this.useScriptableObject = true;
            this.scriptableObject = scriptableObject;
            this.useHolder = true;
            this.holderName = holderName;
            this.variableName = variableName;
        }

        public void Reset()
        {
            gameObject = null;
            component = null;
            useScriptableObject = false;
            scriptableObject = null;
            useHolder = false;
            holderName = string.Empty;
            variableName = string.Empty;
        }
    }
}
