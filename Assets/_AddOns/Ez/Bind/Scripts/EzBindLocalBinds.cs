﻿using System;
using System.Collections.Generic;
using Ez.Binding.Vars;
using UnityEngine;
using UnityEngine.Events;

namespace Ez.Binding
{
    public class EzBindLocalBinds : MonoBehaviour
    {
        /// <summary>
        /// Event invoked on (late) update to trigger all listening binds to check if the source has changed.
        /// </summary>
        protected UnityEvent onUpdate = new UnityEvent();
        /// <summary>
        /// Dinctionary holding all the local binds, using their names as key.
        /// </summary>
        protected Dictionary<string, Bind> localBinds = new Dictionary<string, Bind>();
        public Dictionary<string, Bind> LocalBinds { get { return localBinds; } }

        public List<BindData> localBindsData;

        #region PUBLIC UTILS
        /// <summary>
        /// Creates a new local bind with the specified name and returns it.
        /// <para>If the operation fails, returns null.</para>
        /// </summary>
        /// <param name="newBindName">The bind's name. It must be unique among other local binds of this component.</param>
        /// <returns></returns>
        public Bind AddLocalBind(string newBindName)
        {
            var bind = new Bind(newBindName);
            return AddLocalBind(bind) ? bind : null;
        }
        /// <summary>
        /// Creates a new local bind with the specified name and having the specified initial value, then returns it.
        /// <para>If the operation fails, returns null.</para>
        /// </summary>
        /// <param name="newBindName">The bind's name. It must be unique among other local binds of this component.</param>
        /// <param name="newBindValue">The bind's initial value.</param>
        /// <returns></returns>
        public Bind AddLocalBind(string newBindName, object newBindValue)
        {
            var bind = new Bind(newBindName, newBindValue);
            return AddLocalBind(bind) ? bind : null;
        }
        /// <summary>
        /// Creates a new local bind with the specified name and returns it.
        /// <para>If the operation fails, returns null.</para>
        /// </summary>
        /// <param name="newBindName">The bind's name. It must be unique among other local binds of this component.</param>
        /// <param name="onValueChanged">Event invoked when the bind's value changes.</param>
        /// <returns></returns>
        public Bind AddLocalBind(string newBindName, UnityEvent onValueChanged)
        {
            var bind = new Bind(newBindName, onValueChanged);
            return AddLocalBind(bind) ? bind : null;
        }
        /// <summary>
        /// Creates a new local bind with the specified name and having the specified initial value, then returns it.
        /// <para>If the operation fails, returns null.</para>
        /// </summary>
        /// <param name="newBindName">The bind's name. It must be unique among other local binds of this component.</param>
        /// <param name="newBindValue">The bind's initial value.</param>
        /// <param name="onValueChanged">Event invoked when the bind's value changes.</param>
        /// <returns></returns>
        public Bind AddLocalBind(string newBindName, object newBindValue, UnityEvent onValueChanged)
        {
            var bind = new Bind(newBindName, newBindValue, onValueChanged);
            return AddLocalBind(bind) ? bind : null;
        }
        /// <summary>
        /// Private method that adds the local bind to the dictionary.
        /// <para>Returns true if the operation is successful, false otherwise.</para>
        /// </summary>
        /// <param name="newBind">Bind to be added</param>
        /// <returns>Returns <c>true</c> if the operation is successful, <c>false</c> otherwise</returns>
        private bool AddLocalBind(Bind newBind)
        {
            try
            {
                localBinds.Add(newBind.name, newBind);
            }
            catch(Exception e)
            {
                Debug.LogError(@"[Ez][LocalBind] on " + name + @" - Error when adding a new bind named """ + newBind.name + @""" : " + e.Message);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Removes the local bind specified by name and destroys it.
        /// </summary>
        /// <param name="bindName">Bind's name.</param>
        public void RemoveLocalBind(string bindName)
        {
            Bind var;
            if(localBinds.TryGetValue(bindName, out var))
            {
                localBinds.Remove(bindName);
                var.Dispose();
            }
        }
        /// <summary>
        /// Removes the local bind specified by reference and destroys it.
        /// </summary>
        /// <param name="bind">Bind's reference.</param>
        public void RemoveLocalBind(Bind bind)
        {
            Bind var;
            if(localBinds.TryGetValue(bind.name, out var))
            {
                localBinds.Remove(bind.name);
                var.Dispose();
            }
        }
        /// <summary>
        /// Adds a new observer to the referenced local bind and returns a reference to the new observer.
        /// <para>If unsuccessful, returns null.</para>
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="parent">Observer's Component.</param>
        /// <param name="observerName">Observer's name (field or property name)</param>
        /// <returns></returns>
        public ReferencedVariable AddObserverToLocalBind(Bind bind, UnityEngine.Object parent, string observerName)
        {
            if(bind == null)
            {
                return null;
            }
            var refVar = new ReferencedVariable(bind.name, parent, observerName);
            return bind.AddObserver(refVar) ? refVar : null;
        }
        /// <summary>
        ///  Adds a new observer to the specified local bind and returns a reference to the new observer.
        ///  <para>If unsuccessful, returns null.</para>
        /// </summary>
        /// <param name="bindName">Bind's name.</param>
        /// <param name="parent">Observer's Component.</param>
        /// <param name="observerName">Observer's name (field or property name)</param>
        /// <returns></returns>
        public ReferencedVariable AddObserverToLocalBind(string bindName, UnityEngine.Object parent, string observerName)
        {
            var bind = FindLocalBindByName(bindName);
            return bind != null ? AddObserverToLocalBind(bind, parent, observerName) : null;
        }
        /// <summary>
        /// Add a new observer to the referenced local bind. The observer is a reference to a ReferencedVariable previously created.
        /// <para>Returns true if the operation was successful, false otherwise.</para>
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="observerVar">Observer reference.</param>
        /// <returns></returns>
        public bool AddObserverToLocalBind(Bind bind, ReferencedVariable observerVar)
        {
            if(bind == null)
            {
                return false;
            }
            return bind.AddObserver(observerVar);
        }
        /// <summary>
        /// Add a new observer to the specified local bind. The observer is a reference to a ReferencedVariable previously created.
        /// <para>Returns true if the operation was successful, false otherwise.</para>
        /// </summary>
        /// <param name="bindName">Bind's name.</param>
        /// <param name="observerVar">Observer reference.</param>
        /// <returns></returns>
        public bool AddObserverToLocalBind(string bindName, ReferencedVariable observerVar)
        {
            var bind = FindLocalBindByName(bindName);
            return bind != null ? AddObserverToLocalBind(bind, observerVar) : false;
        }
        /// <summary>
        /// Adds a new source to the referenced local bind and returns a reference to the new source. 
        /// <para>If another source was defined, it is replaced. If unsuccessful, returns null.</para>
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="parent">Source's Component.</param>
        /// <param name="sourceName">Source's name (field or property name)</param>
        /// <returns></returns>
        public ReferencedVariable AddSourceToLocalBind(Bind bind, UnityEngine.Object parent, string sourceName)
        {
            if(bind == null)
            {
                return null;
            }
            var refVar = new ReferencedVariable(bind.name, parent, sourceName);
            return bind.AddSourceVariable(refVar) ? refVar : null;
        }
        /// <summary>
        /// Adds a new source to the specified local bind and returns a reference to the new source. 
        /// <para>If another source was defined, it is replaced. If unsuccessful, returns null.</para>
        /// </summary>
        /// <param name="bindName">Bind's name.</param>
        /// <param name="parent">Source's Component.</param>
        /// <param name="observerName">Source's name (field or property name)</param>
        /// <returns></returns>
        public ReferencedVariable AddSourceToLocalBind(string bindName, UnityEngine.Object parent, string observerName)
        {
            var bind = FindLocalBindByName(bindName);
            return bind != null ? AddSourceToLocalBind(bind, parent, observerName) : null;
        }
        /// <summary>
        /// Adds a new source to the referenced local bind. The source is a reference to a ReferenceVariable previously created. 
        /// <para>If another source was defined, it is replaced. Returns true if successful, false otherwise.</para>
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="refVar">Source reference.</param>
        /// <returns></returns>
        public bool AddSourceToLocalBind(Bind bind, ReferencedVariable sourceVar)
        {
            if(bind == null)
            {
                return false;
            }
            return bind.AddSourceVariable(sourceVar);
        }
        /// <summary>
        /// Adds a new source to the specified local bind. The source is a reference to a ReferenceVariable previously created.
        /// <para>If another source was defined, it is replaced. Returns true if successful, false otherwise.</para>
        /// </summary>
        /// <param name="bindName">Bind's name.</param>
        /// <param name="sourceVar">Source reference.</param>
        /// <returns></returns>
        public bool AddSourceToLocalBind(string bindName, ReferencedVariable sourceVar)
        {
            var bind = FindLocalBindByName(bindName);
            return bind != null ? AddSourceToLocalBind(bind, sourceVar) : false;
        }
        /// <summary>
        /// Finds and returns an existing local bind by it's name. 
        /// <para>If the bind doesn't exist, returns null.</para>
        /// </summary>
        /// <param name="bindName">The bind's name.</param>
        /// <returns>The bind, if found, <c>null</c> otherwise.</returns>
        public Bind FindLocalBindByName(string bindName)
        {
            Bind var;
            return localBinds.TryGetValue(bindName, out var) ? var : null;
        }
        /// <summary>
        /// Removes an observer from the local bind.
        /// <para>Returns true if successful, false otherwise.</para>
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="refVar">Observer reference.</param>
        /// <returns>Returns <c>true</c> if the operation was successful, <c>false</c> otherwise.</returns>
        public bool RemoveObserverFromLocalBind(Bind bind, ReferencedVariable refVar)
        {
            return (bind != null && refVar != null) ? bind.RemoveObserver(refVar) : false;
        }
        /// <summary>
        /// Removes an observer from the local bind.
        /// <para>Returns true if successful, false otherwise.</para>
        /// </summary>
        /// <param name="bindName">The bind's name.</param>
        /// <param name="refVar">Observer reference.</param>
        /// <returns>Returns <c>true</c> if the operation was successful, <c>false</c> otherwise.</returns>
        public bool RemoveObserverFromLocalBind(string bindName, ReferencedVariable refVar)
        {
            return RemoveObserverFromLocalBind(FindLocalBindByName(bindName), refVar);
        }
        /// <summary>
        /// Adds a listener method to a local bind. 
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="listener">Listener method.</param>
        public void AddListenerToLocalBind(Bind bind, UnityAction listener)
        {
            bind.AddListener(listener);
        }
        /// <summary>
        /// Adds a listener method to a local bind.
        /// <para> Returns true if successful, false otherwise.</para>
        /// </summary>
        /// <param name="bindName">The bind's name.</param>
        /// <param name="listener">Listener method.</param>
        /// <returns>Returns <c>true</c> if successful, <c>false</c> otherwise.</returns>
        public bool AddListenerToLocalBind(string bindName, UnityAction listener)
        {
            var bind = FindLocalBindByName(bindName);
            if(bind != null)
            {
                AddListenerToLocalBind(bind, listener);
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Removes the listener from the local bind.
        /// </summary>
        /// <param name="bind">Bind reference.</param>
        /// <param name="listener">Listener method.</param>
        public void RemoveListenerFromLocalBind(Bind bind, UnityAction listener)
        {
            bind.RemoveListener(listener);
        }
        /// <summary>
        /// Removes the listener method from the local bind.
        /// <para> Returns true if successful, false otherwise.</para>
        /// </summary>
        /// <param name="bindName">The bind's name.</param>
        /// <param name="listener">Listener method.</param>
        /// <returns></returns>
        public bool RemoveListenerFromLocalBind(string bindName, UnityAction listener)
        {
            var bind = FindLocalBindByName(bindName);
            if(bind != null)
            {
                RemoveListenerFromLocalBind(bind, listener);
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion // PUBLIC UTILS

        private void Awake()
        {
            CreateBinds();

            localBindsData = null;
        }

        private void LateUpdate()
        {
            onUpdate.Invoke();
        }

        private void CreateBinds()
        {
            if(localBindsData == null || localBindsData.Count == 0) { return; }

            Bind tempBind;
            ReferencedVariable refVar = null;
            object holderRef;
            bool refVarCreated;
#if EZ_PLAYMAKER_SUPPORT
            HutongGames.PlayMaker.NamedVariable fsmVar;
#endif
#if EZ_BOLT_SUPPORT
            Bolt.VariableDeclaration boltVar;
#endif

            for(int i = 0; i < localBindsData.Count; i++)
            {
                if(string.IsNullOrEmpty(localBindsData[i].bindName) || localBindsData[i].bindName.Equals("UnnamedLocalBind")) { continue; }
                tempBind = AddLocalBind(localBindsData[i].bindName, localBindsData[i].OnValueChanged); // Create local bind

                if(tempBind == null) { continue; }

                tempBind.ChangeBindUpdateEvent(onUpdate); // link the bind's update event to this MonoBehaviour
                refVarCreated = false;

                if(!localBindsData[i].source.useScriptableObject &&
                    localBindsData[i].source.gameObject != null &&
                    localBindsData[i].source.component != null &&
                    !string.IsNullOrEmpty(localBindsData[i].source.variableName) &&
                    !localBindsData[i].source.variableName.Equals("None"))
                {
                    if(EzBind.GetHolderReferenceIfExists(localBindsData[i].source, localBindsData[i].source.component, out holderRef))
                    {
                        // Add the source with GO & holder; check if PlayMaker / Bolt 
#if EZ_PLAYMAKER_SUPPORT
                        if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                        {
                            // Make refVar a reference to the FSM NamedVariable
                            fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(localBindsData[i].source.variableName.Split(' ')[1]);
                            refVar = new ReferencedVariable(tempBind.name, localBindsData[i].source.gameObject, fsmVar, fsmVar.Name, "RawValue");
                            refVarCreated = true;
                        }
#endif
#if EZ_BOLT_SUPPORT
                        if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                        {
                            // Make refVar a reference to the Bolt Variable
                            boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(localBindsData[i].source.variableName.Split(' ')[1]);
                            refVar = new ReferencedVariable(tempBind.name, localBindsData[i].source.gameObject, boltVar, boltVar.name, "value");
                            refVarCreated = true;
                        }
#endif
                        // Add Source with holder
                        refVar = refVarCreated ? refVar : new ReferencedVariable(tempBind.name, localBindsData[i].source.gameObject, holderRef, localBindsData[i].source.holderName.Split(' ')[1], localBindsData[i].source.variableName.Split(' ')[1]);
                        AddSourceToLocalBind(tempBind, refVar);
                    }
                    else
                    {
                        // Add Source w/o holder
                        AddSourceToLocalBind(tempBind, localBindsData[i].source.component, localBindsData[i].source.variableName.Split(' ')[1]); // Add Source from GO
                    }
                }
                else if(localBindsData[i].source.useScriptableObject &&
                        localBindsData[i].source.scriptableObject != null &&
                        !string.IsNullOrEmpty(localBindsData[i].source.variableName) &&
                        !localBindsData[i].source.variableName.Equals("None"))
                {
                    if(EzBind.GetHolderReferenceIfExists(localBindsData[i].source, localBindsData[i].source.scriptableObject, out holderRef))
                    {
                        // Add the source with SO & holder; check if PlayMaker / Bolt
#if EZ_PLAYMAKER_SUPPORT
                        if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                        {
                            // Make refVar a reference to the FSM NamedVariable
                            fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(localBindsData[i].source.variableName.Split(' ')[1]);
                            refVar = new ReferencedVariable(tempBind.name, localBindsData[i].source.scriptableObject, fsmVar, fsmVar.Name, "RawValue");
                            refVarCreated = true;
                        }
#endif
#if EZ_BOLT_SUPPORT
                        if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                        {
                            // Make refVar a reference to the Bolt Variable
                            boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(localBindsData[i].source.variableName.Split(' ')[1]);
                            refVar = new ReferencedVariable(tempBind.name, localBindsData[i].source.scriptableObject, boltVar, boltVar.name, "value");
                            refVarCreated = true;
                        }
#endif
                        // Add source with holder
                        refVar = refVarCreated ? refVar : new ReferencedVariable(tempBind.name, localBindsData[i].source.scriptableObject, holderRef, localBindsData[i].source.holderName.Split(' ')[1], localBindsData[i].source.variableName.Split(' ')[1]);
                        AddSourceToLocalBind(tempBind, refVar);
                    }
                    else
                    {
                        // Add source w/o holder
                        AddSourceToLocalBind(tempBind, localBindsData[i].source.scriptableObject, localBindsData[i].source.variableName.Split(' ')[1]); // Add Source from ScriptableObject
                    }
                }
                if(localBindsData[i].observers != null && localBindsData[i].observers.Count > 0)
                {
                    for(int j = 0; j < localBindsData[i].observers.Count; j++)
                    {
                        refVarCreated = false;

                        if(!localBindsData[i].observers[j].useScriptableObject &&
                           localBindsData[i].observers[j].gameObject != null &&
                           localBindsData[i].observers[j].component != null &&
                           !string.IsNullOrEmpty(localBindsData[i].observers[j].variableName) &&
                           !localBindsData[i].observers[j].variableName.Equals("None"))
                        {
                            if(EzBind.GetHolderReferenceIfExists(localBindsData[i].observers[j], localBindsData[i].observers[j].component, out holderRef))
                            {
                                // Add observer[j] with GO & holder; check if PlayMaker or Bolt
#if EZ_PLAYMAKER_SUPPORT
                                if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                                {
                                    // Make refVar a reference to the FSM NamedVariable
                                    fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(localBindsData[i].observers[j].variableName.Split(' ')[1]);
                                    refVar = new ReferencedVariable(tempBind.name, localBindsData[i].observers[j].gameObject, fsmVar, fsmVar.Name, "RawValue");
                                    refVarCreated = true;
                                }
#endif
#if EZ_BOLT_SUPPORT
                                if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                                {
                                    // Make refVar a reference to the Bolt Variable
                                    boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(localBindsData[i].observers[j].variableName.Split(' ')[1]);
                                    refVar = new ReferencedVariable(tempBind.name, localBindsData[i].observers[j].gameObject, boltVar, boltVar.name, "value");
                                    refVarCreated = true;
                                }
#endif
                                // Add observer with holder
                                refVar = refVarCreated ? refVar : new ReferencedVariable(tempBind.name, localBindsData[i].observers[j].gameObject, holderRef, localBindsData[i].observers[j].holderName.Split(' ')[1], localBindsData[i].observers[j].variableName.Split(' ')[1]);
                                AddObserverToLocalBind(tempBind, refVar);
                            }
                            else
                            {
                                // Add observer w/o holder
                                AddObserverToLocalBind(tempBind, localBindsData[i].observers[j].component, localBindsData[i].observers[j].variableName.Split(' ')[1]); // Add Observer from GO
                            }
                        }
                        else if(localBindsData[i].observers[j].useScriptableObject &&
                                localBindsData[i].observers[j].scriptableObject != null &&
                                !string.IsNullOrEmpty(localBindsData[i].observers[j].variableName) &&
                                !localBindsData[i].observers[j].variableName.Equals("None"))
                        {
                            if(EzBind.GetHolderReferenceIfExists(localBindsData[i].observers[j], localBindsData[i].observers[j].scriptableObject, out holderRef))
                            {
                                // Add observer[j] with SO & holder; check if PlayMaker or Bolt
#if EZ_PLAYMAKER_SUPPORT
                                if(holderRef.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
                                {
                                    // Make refVar a reference to the FSM NamedVariable
                                    fsmVar = ((HutongGames.PlayMaker.FsmVariables) holderRef).GetVariable(localBindsData[i].observers[j].variableName.Split(' ')[1]);
                                    refVar = new ReferencedVariable(tempBind.name, localBindsData[i].observers[j].scriptableObject, fsmVar, fsmVar.Name, "RawValue");
                                    refVarCreated = true;
                                }
#endif
#if EZ_BOLT_SUPPORT
                                if(holderRef.GetType() == typeof(Bolt.VariableDeclarations))
                                {
                                    // Make refVar a reference to the Bolt Variable
                                    boltVar = ((Bolt.VariableDeclarations) holderRef).GetDeclaration(localBindsData[i].observers[j].variableName.Split(' ')[1]);
                                    refVar = new ReferencedVariable(tempBind.name, localBindsData[i].observers[j].scriptableObject, boltVar, boltVar.name, "value");
                                    refVarCreated = true;
                                }
#endif
                                // Add observer with holder
                                refVar = refVarCreated ? refVar : new ReferencedVariable(tempBind.name, localBindsData[i].observers[j].scriptableObject, holderRef, localBindsData[i].observers[j].holderName.Split(' ')[1], localBindsData[i].observers[j].variableName.Split(' ')[1]);
                                AddObserverToLocalBind(tempBind, refVar);
                            }
                            else
                            {
                                // Add observer w/o holder
                                AddObserverToLocalBind(tempBind, localBindsData[i].observers[j].scriptableObject, localBindsData[i].observers[j].variableName.Split(' ')[1]); // Add Observer from ScriptableObject
                            }
                        }
                    }
                }
            }
        }
    }
}