﻿// Copyright (c) 2016 - 2018 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Ez.Binding;
using Ez.Binding.Internal;
using QuickEditor;
using QuickEngine.Extensions;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace Ez
{
    public partial class ControlPanelWindow : QWindow
    {
#if EZ_BIND
        public enum BindSearchFor
        {
            GameObject,
            BindName,
            Variable
        }

        private const float BIND_LINE_HEIGHT = 24;

        private const float BIND_HORIZONTAL_SPACE = 8;

        private const float BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT = 0.2f;
        private const float BIND_BIND_NAME_WIDTH_PERCENT = 0.27f;
        private const float BIND_COMPONENT_NAME_WIDTH_PERCENT = 0.26f;
        private const float BIND_VARIABLE_NAME_WIDTH_PERCENT = 0.27f;

        private static EzBind _ezBind;
        public static EzBind EzBind
        {
            get
            {
                if(_ezBind == null)
                {
                    _ezBind = FindObjectOfType<EzBind>();
                }
                return _ezBind;
            }
        }

        private static EzBindExtension _ezBindExtension;
        public static EzBindExtension EzBindExtension
        {
            get
            {
                if(_ezBindExtension == null)
                {
                    _ezBindExtension = FindObjectOfType<EzBindExtension>();
                }
                return _ezBindExtension;
            }
        }

        EzBind[] ezBinds;
        EzBindExtension[] ezBindExtensions;
        EzBindAddSource[] ezBindAddSources;
        EzBindAddObserver[] ezBindAddObservers;

        AnimBool showEzBinds, showEzBindExtensions, showEzBindAddSources, showEzBindAddObservers;

        BindSearchFor searchFor = BindSearchFor.BindName;

        bool refreshEzBindOnInspectorUpdate = false;
        bool foundMatchEzBind = false;

        QLabel searchForLabel, namedLabel;
        GUIStyle NormalTextStyle, SmallTextStyle;

        float tempWidth;

        void InitBind()
        {
            InitAnimBools();
            GenerateBindInfoMessages();
            refreshEzBindOnInspectorUpdate = true;
            BindRefreshEzBindComponentsInfoInCurrentScene();
        }

        void InitAnimBools()
        {
            showEzBinds = new AnimBool(false, Repaint);
            showEzBindExtensions = new AnimBool(false, Repaint);
            showEzBindAddSources = new AnimBool(false, Repaint);
            showEzBindAddObservers = new AnimBool(false, Repaint);
        }
        void GenerateBindInfoMessages()
        {
            if(infoMessage == null) { infoMessage = new Dictionary<string, InfoMessage>(); }

            if(!infoMessage.ContainsKey("BindSearchNoResults")) { infoMessage.Add("BindSearchNoResults", new InfoMessage() { title = "Your search returned no results...", message = "", show = new AnimBool(true), type = InfoMessageType.Info }); }

            if(!infoMessage.ContainsKey("BindMultipleEzBindFound")) { infoMessage.Add("BindMultipleEzBindFound", new InfoMessage() { title = "Multiple EzBind components found! Only one should exist in your project.", message = "", show = new AnimBool(true), type = InfoMessageType.Error }); }

            if(!infoMessage.ContainsKey("BindNoEzBind")) { infoMessage.Add("BindNoEzBind", new InfoMessage() { title = "EzBind was not found in the current scene", message = "", show = new AnimBool(true), type = InfoMessageType.Info }); }
            if(!infoMessage.ContainsKey("BindNoEzBindExtension")) { infoMessage.Add("BindNoEzBindExtension", new InfoMessage() { title = "No EzBindExtension components were found in the current scene", message = "", show = new AnimBool(true), type = InfoMessageType.Info }); }
            if(!infoMessage.ContainsKey("BindNoEzBindAddSource")) { infoMessage.Add("BindNoEzBindAddSource", new InfoMessage() { title = "No EzBindAddSource components were found in the current scene", message = "", show = new AnimBool(true), type = InfoMessageType.Info }); }
            if(!infoMessage.ContainsKey("BindNoEzBindAddObserver")) { infoMessage.Add("BindNoEzBindAddObserver", new InfoMessage() { title = "No EzBindAddObserver components were found in the current scene", message = "", show = new AnimBool(true), type = InfoMessageType.Info }); }

            if(!infoMessage.ContainsKey("BindNoBindsHaveBeenFound")) { infoMessage.Add("BindNoBindsHaveBeenFound", new InfoMessage() { title = "No binds have been found", message = "", show = new AnimBool(true), type = InfoMessageType.Info }); }

            if(!infoMessage.ContainsKey("BindMissingBindName")) { infoMessage.Add("BindMissingBindName", new InfoMessage() { title = EzBindEditor.UNNAMED_BIND, message = "", show = new AnimBool(true), type = InfoMessageType.Warning }); }
            if(!infoMessage.ContainsKey("BindMissingComponent")) { infoMessage.Add("BindMissingComponent", new InfoMessage() { title = "Missing Component", message = "", show = new AnimBool(true), type = InfoMessageType.Warning }); }
            if(!infoMessage.ContainsKey("BindMissingVariable")) { infoMessage.Add("BindMissingVariable", new InfoMessage() { title = "Missing Variable", message = "", show = new AnimBool(true), type = InfoMessageType.Warning }); }

            if(!infoMessage.ContainsKey("BindRuntimeInspector")) { infoMessage.Add("BindRuntimeInspector", new InfoMessage() { title = "At runtime all Binds are shown in EzBind.", message = "", show = new AnimBool(true), type = InfoMessageType.Info }); }
        }
        void BindRefreshEzBindComponentsInfoInCurrentScene()
        {
            if(!refreshEzBindOnInspectorUpdate) { return; }
            ezBinds = FindObjectsOfType<EzBind>();
            Array.Sort(ezBinds, delegate (EzBind e1, EzBind e2) { return e1.gameObject.name.CompareTo(e2.gameObject.name); });
            ezBindExtensions = FindObjectsOfType<EzBindExtension>();
            Array.Sort(ezBindExtensions, delegate (EzBindExtension e1, EzBindExtension e2) { return e1.gameObject.name.CompareTo(e2.gameObject.name); });
            ezBindAddSources = FindObjectsOfType<EzBindAddSource>();
            Array.Sort(ezBindAddSources, delegate (EzBindAddSource e1, EzBindAddSource e2) { return e1.gameObject.name.CompareTo(e2.gameObject.name); });
            ezBindAddObservers = FindObjectsOfType<EzBindAddObserver>();
            Array.Sort(ezBindAddObservers, delegate (EzBindAddObserver e1, EzBindAddObserver e2) { return e1.gameObject.name.CompareTo(e2.gameObject.name); });
            refreshEzBindOnInspectorUpdate = false;
        }

        void DrawBind()
        {
            NormalTextStyle = QStyles.GetTextStyle(Style.Text.Normal);
            SmallTextStyle = QStyles.GetTextStyle(Style.Text.Small);
            NormalTextStyle.wordWrap = true;
            SmallTextStyle.wordWrap = true;

            DrawPageHeader("DATA BIND", QColors.Green, "Data Binding Solution / Live Data", QUI.IsProSkin ? QColors.UnityLight : QColors.UnityMild, EZResources.IconBind);
            QUI.Space(6);
            DrawBindAddRemoveButtons(WindowSettings.CurrentPageContentWidth + WindowSettings.pageShadowWidth);
            QUI.Space(SPACE_8);
            DrawEnablePlayMakerAndBoltSupport(WindowSettings.CurrentPageContentWidth + WindowSettings.pageShadowWidth);
            QUI.Space(SPACE_16);
            DrawBindRefreshButton(WindowSettings.CurrentPageContentWidth + WindowSettings.pageShadowWidth);
            QUI.Space(SPACE_8);
            DrawBindSearch(WindowSettings.CurrentPageContentWidth + WindowSettings.pageShadowWidth);
            QUI.Space(SPACE_16);
            if(EditorApplication.isPlaying)
            {
                DrawBindEzBindRuntime(WindowSettings.CurrentPageContentWidth + WindowSettings.pageShadowWidth - SPACE_8);
                QUI.Space(SPACE_8);
                DrawBindEzBindExtensionRuntime(WindowSettings.CurrentPageContentWidth + WindowSettings.pageShadowWidth - SPACE_8);
            }
            else
            {
                DrawBindEzBind(WindowSettings.CurrentPageContentWidth + WindowSettings.pageShadowWidth - SPACE_8);
                QUI.Space(SPACE_8);
                DrawBindEzBindExtension(WindowSettings.CurrentPageContentWidth + WindowSettings.pageShadowWidth - SPACE_8);
            }
            QUI.Space(SPACE_8);
            DrawBindEzBindAddSource(WindowSettings.CurrentPageContentWidth + WindowSettings.pageShadowWidth - SPACE_8);
            QUI.Space(SPACE_8);
            DrawBindEzBindAddObserver(WindowSettings.CurrentPageContentWidth + WindowSettings.pageShadowWidth - SPACE_8);
            QUI.Space(SPACE_8);

            NormalTextStyle.wordWrap = false;
            SmallTextStyle.wordWrap = false;
        }

        void DrawBindAddRemoveButtons(float width)
        {
            QUI.BeginHorizontal(width);
            {
                QUI.FlexibleSpace();
                DrawBindTabButtonAddEzBind((width - SPACE_8) / 2, 20);
                DrawBindTabButtonAddEzBindExtension((width - SPACE_8) / 2, 20);
                QUI.FlexibleSpace();
            }
            QUI.EndHorizontal();
        }

        void DrawEnablePlayMakerAndBoltSupport(float width)
        {
            QUI.BeginHorizontal(width);
            {
                QUI.Space(6);
                QUI.FlexibleSpace();
                QLabel.text = "3rd Party Support";
                QLabel.style = Style.Text.Help;
                QUI.Label(QLabel);
                QUI.Space(SPACE_2);
                tempWidth = (width - 12) / 4;
#if !EZ_PLAYMAKER_SUPPORT
                // Enable PMK support button
                if(QUI.GhostButton("Enable PlayMaker Support", QColors.Color.Gray, tempWidth, 20))
                {
                    if(QUI.DisplayDialog("PlayMaker Support",
                                         "Make sure you have PlayMaker correctly installed before enabling support!" +
                                            "\n\n" +
                                            "Symbol " + EZT.SYMBOL_EZ_PLAYMAKER_SUPPORT + " will be added to the active build platform." +
                                            "\n\n" +
                                            "Enable PlayMaker support?",
                                         "OK",
                                         "Cancel"))
                    {
                        QUtils.AddScriptingDefineSymbol(EZT.SYMBOL_EZ_PLAYMAKER_SUPPORT);
                    }
                }
#else
                // Disable PMK support button
                if(QUI.GhostButton("Disable PlayMaker Support", QColors.Color.Red, tempWidth, 20))
                {
                    if(QUI.DisplayDialog("PlayMaker Support",
                                         "Symbol " + EZT.SYMBOL_EZ_PLAYMAKER_SUPPORT + " will be removed from the active build platform.\n\n" +
                                            "Disable PlayMaker support?",
                                         "OK",
                                         "Cancel"))
                    {
                        QUtils.RemoveScriptingDefineSymbol(EZT.SYMBOL_EZ_PLAYMAKER_SUPPORT);
                    }
                }
#endif

#if !EZ_BOLT_SUPPORT
                // Enable Bolt support button
                if(QUI.GhostButton("Enable Bolt Support", QColors.Color.Gray, tempWidth, 20))
                {
                    if(QUI.DisplayDialog("Bolt Support",
                                         "Make sure you have Bolt correctly installed before enabling support!" +
                                            "\n\n" +
                                            "Symbol " + EZT.SYMBOL_EZ_BOLT_SUPPORT + " will be added to the active build platform." +
                                            "\n\n" +
                                            "Enable Bolt support?",
                                         "OK",
                                         "Cancel"))
                    {
                        QUtils.AddScriptingDefineSymbol(EZT.SYMBOL_EZ_BOLT_SUPPORT);
                    }
                }
#else
                // Disable Bolt support button
                if(QUI.GhostButton("Disable Bolt Support", QColors.Color.Red, tempWidth, 20))
                {
                    if(QUI.DisplayDialog("Bolt Support",
                                         "Symbol " + EZT.SYMBOL_EZ_BOLT_SUPPORT + " will be removed from the active build platform.\n\n" +
                                            "Disable Bolt support?",
                                         "OK",
                                         "Cancel"))
                    {
                        QUtils.RemoveScriptingDefineSymbol(EZT.SYMBOL_EZ_BOLT_SUPPORT);
                    }
                }
#endif

                QUI.Space(6);
            }
            QUI.EndHorizontal();
        }

        void DrawBindTabButtonAddEzBind(float buttonWidth, float buttonHeight)
        {
            if(EzBind == null)
            {
                if(QUI.SlicedButton("Add EzBind to Scene", QColors.Color.Green, buttonWidth, buttonHeight))
                {
                    Undo.RegisterCreatedObjectUndo(new GameObject("EzBind", typeof(EzBind)), "AddEzBindToScene");
                    Selection.activeObject = EzBind.gameObject;
                }
            }
            else
            {
                if(QUI.SlicedButton("Remove EzBind from Scene", QColors.Color.Red, buttonWidth, buttonHeight))
                {
                    if(QUI.DisplayDialog("Remove EzBind",
                                            "Are you sure you want to remove (delete) the EzBind gameObject from the current scene?" +
                                            "\n\n\n" +
                                            "You will lose all the references and values you set in the inspector.",
                                         "Ok",
                                         "Cancel"))
                    {
                        Undo.DestroyObjectImmediate(EzBind.gameObject);
                    }
                }
            }
        }
        void DrawBindTabButtonAddEzBindExtension(float buttonWidth, float buttonHeight)
        {
            if(EzBindExtension == null)
            {
                if(QUI.SlicedButton("Add EzBindExtension to Scene", QColors.Color.Blue, buttonWidth, buttonHeight))
                {
                    Undo.RegisterCreatedObjectUndo(new GameObject("EzBindExtension", typeof(EzBindExtension)), "AddEzBindExtensionToScene");
                    Selection.activeObject = EzBindExtension.gameObject;
                }
            }
            else
            {
                if(QUI.SlicedButton("Remove EzBindExtension from Scene", QColors.Color.Red, buttonWidth, buttonHeight))
                {
                    if(QUI.DisplayDialog("Remove EzBindExtension",
                                            "Are you sure you want to remove (delete) the EzBindExtension gameObject from the current scene?" +
                                            "\n\n\n" +
                                            "You will lose all the references and values you set in the inspector.",
                                         "Ok",
                                         "Cancel"))
                    {
                        Undo.DestroyObjectImmediate(EzBindExtension.gameObject);
                    }
                }
            }
        }

        void DrawBindRefreshButton(float width)
        {
            QUI.BeginHorizontal(width);
            {
                QUI.FlexibleSpace();
                if(QUI.GhostButton("Scan all scene objects and update EzBind info", QColors.Color.Gray, width - SPACE_8, BIND_LINE_HEIGHT))
                {
                    refreshEzBindOnInspectorUpdate = true;
                    BindRefreshEzBindComponentsInfoInCurrentScene();
                }
                QUI.FlexibleSpace();
            }
            QUI.EndHorizontal();
        }

        void DrawBindSearch(float width)
        {
            QUI.BeginHorizontal(width);
            {
                QUI.FlexibleSpace();
                if(searchForLabel == null) { searchForLabel = new QLabel("Search for a"); }
                QUI.Label(searchForLabel.text, Style.Text.Normal, searchForLabel.x, 18);
                searchFor = (BindSearchFor)EditorGUILayout.EnumPopup(searchFor, GUILayout.Width(90), GUILayout.Height(18));
                if(namedLabel == null) { namedLabel = new QLabel("named"); }
                GUI.SetNextControlName("SearchPattern");
                QUI.Label(namedLabel.text, Style.Text.Normal, namedLabel.x, 18);
                SearchPattern = QUI.TextField(SearchPattern, Color.white);
                if(SearchPatternAnimBool.faded > 0.4f)
                {
                    if(QUI.GhostButton("Clear Search", QColors.Color.Gray, 90 * SearchPatternAnimBool.faded, 20)
                       || (Event.current.isKey && Event.current.keyCode == KeyCode.Escape && Event.current.type == EventType.KeyUp && GUI.GetNameOfFocusedControl() == "SearchPattern"))
                    {
                        SearchPattern = string.Empty;
                        SearchPatternAnimBool.target = false;
                        showEzBinds.target = false;
                        showEzBindExtensions.target = false;
                        showEzBindAddSources.target = false;
                        showEzBindAddObservers.target = false;
                        GUI.FocusControl(""); //deselect
                    }
                }
                QUI.FlexibleSpace();
            }
            QUI.EndHorizontal();
        }

        void DrawBindEzBind(float width)
        {
            QUI.BeginHorizontal(width);
            {
                QUI.Space(SPACE_4);
                QUI.DrawTexture(EZResources.pageIconEzBind.texture, BIND_LINE_HEIGHT, BIND_LINE_HEIGHT);
                QUI.Space(SPACE_2);
                if(QUI.GhostBar("EzBind", QColors.Color.Green, showEzBinds, width - BIND_LINE_HEIGHT - SPACE_2, BIND_LINE_HEIGHT))
                {
                    showEzBinds.target = !showEzBinds.target;
                }

                if(SearchPatternAnimBool.target)
                {
                    if(searchFor == BindSearchFor.Variable)
                    {
                        showEzBinds.target = false;
                        showEzBinds.value = false;
                    }
                    else
                    {
                        showEzBinds.target = true;
                    }
                }
            }
            QUI.EndHorizontal();

            if(ezBinds != null && ezBinds.Length > 1)
            {
                QUI.Space(-BIND_LINE_HEIGHT);
                QUI.BeginHorizontal(width);
                {
                    QUI.Space(width * 0.3f);
                    DrawInfoMessage("BindMultipleEzBindFound", width * 0.7f);
                }
                QUI.EndHorizontal();
            }

            QUI.BeginHorizontal(width);
            {
                QUI.Space(SPACE_8);
                QUI.Space(BIND_LINE_HEIGHT + (SPACE_8 + SPACE_2) * showEzBinds.faded);
                if(QUI.BeginFadeGroup(showEzBinds.faded))
                {
                    tempWidth = width - BIND_LINE_HEIGHT - SPACE_8 - SPACE_2 - SPACE_8;
                    QUI.BeginVertical(tempWidth);
                    {
                        if(ezBinds.IsNullOrEmpty())
                        {
                            DrawInfoMessage("BindNoEzBind", tempWidth);
                        }
                        else
                        {
                            foundMatchEzBind = false;
                            for(int i = 0; i < ezBinds.Length; i++)
                            {
                                if(ezBinds[i] == null)
                                {
                                    refreshEzBindOnInspectorUpdate = true;
                                    continue;
                                }

                                if(SearchPatternAnimBool.target)
                                {
                                    switch(searchFor)
                                    {
                                        case BindSearchFor.GameObject:
                                            if(!Regex.IsMatch(ezBinds[i].gameObject.name, SearchPattern, RegexOptions.IgnoreCase)) { continue; }
                                            break;
                                        case BindSearchFor.BindName:
                                            if(ezBinds[i].bindsData == null || ezBinds[i].bindsData.Count == 0) { continue; }
                                            bool breakLoop = true;
                                            for(int k = 0; k < ezBinds[i].bindsData.Count; k++) { if(Regex.IsMatch(ezBinds[i].bindsData[k].bindName, SearchPattern, RegexOptions.IgnoreCase)) { breakLoop = false; break; } }
                                            if(breakLoop) { continue; }
                                            break;
                                    }
                                    foundMatchEzBind = true;
                                }

                                QUI.BeginHorizontal(tempWidth);
                                {
                                    QUI.BeginVertical((tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                    {
                                        QUI.FlexibleSpace();
                                        if(QUI.GhostButton(ezBinds[i].gameObject.name, QColors.Color.Green, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT, BIND_LINE_HEIGHT - SPACE_4))
                                        {
                                            Selection.activeObject = ezBinds[i].gameObject;
                                        }
                                        QUI.FlexibleSpace();
                                    }
                                    QUI.EndVertical();

                                    if(ezBinds[i].bindsData == null || ezBinds[i].bindsData.Count == 0)
                                    {
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                        DrawInfoMessage("BindNoBindsHaveBeenFound", tempWidth - (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT);
                                    }
                                    else
                                    {
                                        QUI.BeginVertical(tempWidth - (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT - BIND_HORIZONTAL_SPACE, BIND_LINE_HEIGHT);
                                        {
                                            QUI.FlexibleSpace();
                                            QUI.BeginHorizontal();
                                            {
                                                QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                                QUI.Label("Bind Name", Style.Text.Small, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_BIND_NAME_WIDTH_PERCENT, 12);
                                                QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                                QUI.SetGUIColor(QUI.IsProSkin ? QColors.OrangeLight.Color : QColors.OrangeDark.Color);
                                                QUI.Label("Source", Style.Text.Small, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_COMPONENT_NAME_WIDTH_PERCENT, 12);
                                                QUI.ResetColors();
                                                QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                                QUI.SetGUIColor(QUI.IsProSkin ? QColors.PurpleLight.Color : QColors.PurpleDark.Color);
                                                QUI.Label("Observers", Style.Text.Small, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_VARIABLE_NAME_WIDTH_PERCENT, 12);
                                                QUI.ResetColors();
                                            }
                                            QUI.EndHorizontal();
                                            QUI.Space(2);
                                        }
                                        QUI.EndVertical();
                                    }
                                    QUI.FlexibleSpace();
                                }
                                QUI.EndHorizontal();

                                if(ezBinds[i].bindsData == null || ezBinds[i].bindsData.Count == 0)
                                {
                                    continue;
                                }

                                QUI.Space(-3);

                                QUI.BeginHorizontal(tempWidth);
                                {
                                    QUI.Space(SPACE_2);
                                    QUI.DrawLine(QColors.Color.Green, (tempWidth - SPACE_2) * showEzBinds.faded);
                                }
                                QUI.EndHorizontal();

                                for(int j = 0; j < ezBinds[i].bindsData.Count; j++)
                                {
                                    if(SearchPatternAnimBool.target)
                                    {
                                        switch(searchFor)
                                        {
                                            case BindSearchFor.BindName: if(!Regex.IsMatch(ezBinds[i].bindsData[j].bindName, SearchPattern, RegexOptions.IgnoreCase)) { continue; } break;
                                            case BindSearchFor.Variable: continue;
                                            default: continue;
                                        }

                                        foundMatchEzBind = true;
                                    }

                                    QUI.BeginHorizontal(tempWidth);
                                    {
                                        QUI.Space((tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT);
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                        //HasBind
                                        if(!string.IsNullOrEmpty(ezBinds[i].bindsData[j].bindName) && !ezBinds[i].bindsData[j].bindName.Equals(EzBindEditor.UNNAMED_BIND))
                                        {
                                            QUI.Label(ezBinds[i].bindsData[j].bindName, Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_BIND_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                        }
                                        else
                                        {
                                            DrawInfoMessage("BindMissingBindName", (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_BIND_NAME_WIDTH_PERCENT);
                                        }
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                        //HasSource
                                        QUI.SetGUIColor(QUI.IsProSkin ? QColors.OrangeLight.Color : QColors.OrangeDark.Color);
                                        QUI.Label(BindInfo.BindHasSource(ezBinds[i].bindsData[j]) ? "Added" : "None", Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_COMPONENT_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                        QUI.ResetColors();
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                        //HasObservers
                                        QUI.SetGUIColor(QUI.IsProSkin ? QColors.PurpleLight.Color : QColors.PurpleDark.Color);
                                        QUI.Label(ezBinds[i].bindsData[j].observers.Count != 0 ? ezBinds[i].bindsData[j].observers.Count + " Observers" : "None", Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_VARIABLE_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                        QUI.ResetColors();
                                        QUI.ResetColors();
                                        QUI.FlexibleSpace();
                                    }
                                    QUI.EndHorizontal();
                                    QUI.BeginHorizontal(tempWidth);
                                    {
                                        QUI.Space((tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT);
                                        QUI.DrawLine(QColors.Color.Green, (tempWidth - (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT) * showEzBinds.faded);
                                    }
                                    QUI.EndHorizontal();
                                }
                                QUI.Space(SPACE_16 * showEzBinds.faded);
                            }

                            if(SearchPatternAnimBool.target && !foundMatchEzBind)
                            {
                                DrawInfoMessage("BindSearchNoResults", tempWidth);
                            }
                        }
                    }
                    QUI.EndVertical();
                }
                QUI.EndFadeGroup();
            }
            QUI.EndHorizontal();
        }

        void DrawBindEzBindRuntime(float width)
        {
            QUI.BeginHorizontal(width);
            {
                QUI.Space(SPACE_4);
                QUI.DrawTexture(EZResources.pageIconEzBind.texture, BIND_LINE_HEIGHT, BIND_LINE_HEIGHT);
                QUI.Space(SPACE_2);
                if(QUI.GhostBar("EzBind", QColors.Color.Green, showEzBinds, width - BIND_LINE_HEIGHT - SPACE_2, BIND_LINE_HEIGHT))
                {
                    showEzBinds.target = !showEzBinds.target;
                }

                if(SearchPatternAnimBool.target)
                {
                    if(searchFor == BindSearchFor.Variable)
                    {
                        showEzBinds.target = false;
                        showEzBinds.value = false;
                    }
                    else
                    {
                        showEzBinds.target = true;
                    }
                }
            }
            QUI.EndHorizontal();

            if(ezBinds != null && ezBinds.Length > 1)
            {
                QUI.Space(-BIND_LINE_HEIGHT);
                QUI.BeginHorizontal(width);
                {
                    QUI.Space(width * 0.3f);
                    DrawInfoMessage("BindMultipleEzBindFound", width * 0.7f);
                }
                QUI.EndHorizontal();
            }

            QUI.BeginHorizontal(width);
            {
                QUI.Space(SPACE_8);
                QUI.Space(BIND_LINE_HEIGHT + (SPACE_8 + SPACE_2) * showEzBinds.faded);
                if(QUI.BeginFadeGroup(showEzBinds.faded))
                {
                    tempWidth = width - BIND_LINE_HEIGHT - SPACE_8 - SPACE_2 - SPACE_8;
                    QUI.BeginVertical(tempWidth);
                    {
                        if(ezBinds.IsNullOrEmpty())
                        {
                            DrawInfoMessage("BindNoEzBind", tempWidth);
                        }
                        else
                        {
                            foundMatchEzBind = false;
                            for(int i = 0; i < ezBinds.Length; i++)
                            {
                                if(ezBinds[i] == null)
                                {
                                    refreshEzBindOnInspectorUpdate = true;
                                    continue;
                                }

                                if(SearchPatternAnimBool.target)
                                {
                                    switch(searchFor)
                                    {
                                        case BindSearchFor.GameObject:
                                            if(!Regex.IsMatch(ezBinds[i].gameObject.name, SearchPattern, RegexOptions.IgnoreCase)) { continue; }
                                            break;
                                        case BindSearchFor.BindName:
                                            if(ezBinds[i].BindsHolder == null || ezBinds[i].BindsHolder.Count == 0) { continue; }
                                            bool breakLoop = true;
                                            foreach(var kvp in ezBinds[i].BindsHolder) { if(Regex.IsMatch(kvp.Value.name, SearchPattern, RegexOptions.IgnoreCase)) { breakLoop = false; break; } }
                                            if(breakLoop) { continue; }
                                            break;
                                    }
                                    foundMatchEzBind = true;
                                }

                                QUI.BeginHorizontal(tempWidth);
                                {
                                    QUI.BeginVertical((tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                    {
                                        QUI.FlexibleSpace();
                                        if(QUI.GhostButton(ezBinds[i].gameObject.name, QColors.Color.Green, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT, BIND_LINE_HEIGHT - SPACE_4))
                                        {
                                            Selection.activeObject = ezBinds[i].gameObject;
                                        }
                                        QUI.FlexibleSpace();
                                    }
                                    QUI.EndVertical();

                                    if(ezBinds[i].BindsHolder == null || ezBinds[i].BindsHolder.Count == 0)
                                    {
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                        DrawInfoMessage("BindNoBindsHaveBeenFound", tempWidth - (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT);
                                    }
                                    else
                                    {
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                        QUI.Label("Bind Name", Style.Text.Small, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_BIND_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                        QUI.SetGUIColor(QUI.IsProSkin ? QColors.OrangeLight.Color : QColors.OrangeDark.Color);
                                        QUI.Label("Source (Value)", Style.Text.Small, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_COMPONENT_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                        QUI.ResetColors();
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                        QUI.SetGUIColor(QUI.IsProSkin ? QColors.PurpleLight.Color : QColors.PurpleDark.Color);
                                        QUI.Label("Observers (Count)", Style.Text.Small, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_VARIABLE_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                        QUI.ResetColors();
                                    }
                                    QUI.FlexibleSpace();
                                }
                                QUI.EndHorizontal();

                                if(ezBinds[i].BindsHolder == null || ezBinds[i].BindsHolder.Count == 0)
                                {
                                    continue;
                                }

                                QUI.Space(-3);
                                QUI.BeginHorizontal(tempWidth);
                                {
                                    QUI.Space(SPACE_2);
                                    QUI.DrawLine(QColors.Color.Green, (tempWidth - SPACE_2) * showEzBinds.faded);
                                }
                                QUI.EndHorizontal();

                                foreach(var kvp in ezBinds[i].BindsHolder)
                                {
                                    if(SearchPatternAnimBool.target)
                                    {
                                        switch(searchFor)
                                        {
                                            case BindSearchFor.BindName: if(!Regex.IsMatch(kvp.Value.name, SearchPattern, RegexOptions.IgnoreCase)) { continue; } break;
                                            case BindSearchFor.Variable: continue;
                                            default: continue;
                                        }

                                        foundMatchEzBind = true;
                                    }

                                    QUI.BeginHorizontal(tempWidth);
                                    {
                                        QUI.Space((tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT);
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                        //HasBind

                                        QUI.Label(kvp.Value.name, Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_BIND_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);

                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                        //HasSource
                                        QUI.SetGUIColor(QUI.IsProSkin ? QColors.OrangeLight.Color : QColors.OrangeDark.Color);
                                        QUI.Label(kvp.Value.HasSource ? kvp.Value.Value.ToString() : "---", Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_COMPONENT_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                        QUI.ResetColors();
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBinds.faded);
                                        //HasObservers
                                        QUI.SetGUIColor(QUI.IsProSkin ? QColors.PurpleLight.Color : QColors.PurpleDark.Color);
                                        QUI.Label(kvp.Value.ObserversCount + " Observers", Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_VARIABLE_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                        QUI.ResetColors();
                                        QUI.ResetColors();
                                        QUI.FlexibleSpace();
                                    }
                                    QUI.EndHorizontal();
                                    QUI.BeginHorizontal(tempWidth);
                                    {
                                        QUI.Space((tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT);
                                        QUI.DrawLine(QColors.Color.Green, (tempWidth - (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT) * showEzBinds.faded);
                                    }
                                    QUI.EndHorizontal();
                                }
                                QUI.Space(SPACE_16 * showEzBinds.faded);
                            }

                            if(SearchPatternAnimBool.target && !foundMatchEzBind)
                            {
                                DrawInfoMessage("BindSearchNoResults", tempWidth);
                            }
                        }
                    }
                    QUI.EndVertical();
                }
                QUI.EndFadeGroup();
            }
            QUI.EndHorizontal();
        }

        void DrawBindEzBindExtension(float width)
        {
            QUI.BeginHorizontal(width);
            {
                QUI.Space(SPACE_4);
                QUI.DrawTexture(EZResources.pageIconEzBindExtension.texture, BIND_LINE_HEIGHT, BIND_LINE_HEIGHT);
                QUI.Space(SPACE_2);
                if(QUI.GhostBar("EzBindExtension", QColors.Color.Blue, showEzBindExtensions, width - BIND_LINE_HEIGHT - SPACE_2, BIND_LINE_HEIGHT))
                {
                    showEzBindExtensions.target = !showEzBindExtensions.target;
                }

                if(SearchPatternAnimBool.target)
                {
                    if(searchFor == BindSearchFor.Variable)
                    {
                        showEzBindExtensions.target = false;
                        showEzBindExtensions.value = false;
                    }
                    else
                    {
                        showEzBindExtensions.target = true;
                    }
                }
            }
            QUI.EndHorizontal();
            QUI.BeginHorizontal(width);
            {
                QUI.Space(SPACE_8);
                QUI.Space(BIND_LINE_HEIGHT + (SPACE_8 + SPACE_2) * showEzBindExtensions.faded);
                if(QUI.BeginFadeGroup(showEzBindExtensions.faded))
                {
                    tempWidth = width - BIND_LINE_HEIGHT - SPACE_8 - SPACE_2 - SPACE_8;
                    QUI.BeginVertical(tempWidth);
                    {
                        if(ezBindExtensions.IsNullOrEmpty())
                        {
                            DrawInfoMessage("BindNoEzBindExtension", tempWidth);
                        }
                        else
                        {
                            foundMatchEzBind = false;
                            for(int i = 0; i < ezBindExtensions.Length; i++)
                            {
                                if(ezBindExtensions[i] == null)
                                {
                                    refreshEzBindOnInspectorUpdate = true;
                                    continue;
                                }

                                if(SearchPatternAnimBool.target)
                                {
                                    switch(searchFor)
                                    {
                                        case BindSearchFor.GameObject:
                                            if(!Regex.IsMatch(ezBindExtensions[i].gameObject.name, SearchPattern, RegexOptions.IgnoreCase)) { continue; }
                                            break;
                                        case BindSearchFor.BindName:
                                            if(ezBindExtensions[i].bindsData == null || ezBindExtensions[i].bindsData.Count == 0) { continue; }
                                            bool breakLoop = true;
                                            for(int k = 0; k < ezBindExtensions[i].bindsData.Count; k++) { if(Regex.IsMatch(ezBindExtensions[i].bindsData[k].bindName, SearchPattern, RegexOptions.IgnoreCase)) { breakLoop = false; break; } }
                                            if(breakLoop) { continue; }
                                            break;
                                    }
                                    foundMatchEzBind = true;
                                }

                                QUI.BeginHorizontal(tempWidth);
                                {
                                    QUI.BeginVertical((tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                    {
                                        QUI.FlexibleSpace();
                                        if(QUI.GhostButton(ezBindExtensions[i].gameObject.name, QColors.Color.Blue, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT, BIND_LINE_HEIGHT - SPACE_4))
                                        {
                                            Selection.activeObject = ezBindExtensions[i].gameObject;
                                        }
                                        QUI.FlexibleSpace();
                                    }
                                    QUI.EndVertical();

                                    if(ezBindExtensions[i].bindsData == null || ezBindExtensions[i].bindsData.Count == 0)
                                    {
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBindExtensions.faded);
                                        DrawInfoMessage("BindNoBindsHaveBeenFound", tempWidth - (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT);
                                    }
                                    else
                                    {
                                        QUI.BeginVertical(tempWidth - (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT - BIND_HORIZONTAL_SPACE, BIND_LINE_HEIGHT);
                                        {
                                            QUI.FlexibleSpace();
                                            QUI.BeginHorizontal();
                                            {
                                                QUI.Space(BIND_HORIZONTAL_SPACE * showEzBindExtensions.faded);
                                                QUI.Label("Bind Name", Style.Text.Small, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_BIND_NAME_WIDTH_PERCENT, 12);
                                                QUI.Space(BIND_HORIZONTAL_SPACE * showEzBindExtensions.faded);
                                                QUI.SetGUIColor(QUI.IsProSkin ? QColors.OrangeLight.Color : QColors.OrangeDark.Color);
                                                QUI.Label("Source", Style.Text.Small, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_COMPONENT_NAME_WIDTH_PERCENT, 12);
                                                QUI.ResetColors();
                                                QUI.Space(BIND_HORIZONTAL_SPACE * showEzBindExtensions.faded);
                                                QUI.SetGUIColor(QUI.IsProSkin ? QColors.PurpleLight.Color : QColors.PurpleDark.Color);
                                                QUI.Label("Observers", Style.Text.Small, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_VARIABLE_NAME_WIDTH_PERCENT, 12);
                                                QUI.ResetColors();
                                            }
                                            QUI.EndHorizontal();
                                            QUI.Space(2);
                                        }
                                        QUI.EndVertical();
                                    }
                                    QUI.FlexibleSpace();
                                }
                                QUI.EndHorizontal();

                                if(ezBindExtensions[i].bindsData == null || ezBindExtensions[i].bindsData.Count == 0)
                                {
                                    continue;
                                }

                                QUI.Space(-3);
                                QUI.BeginHorizontal(tempWidth);
                                {
                                    QUI.Space(SPACE_2);
                                    QUI.DrawLine(QColors.Color.Blue, (tempWidth - SPACE_2) * showEzBindExtensions.faded);
                                }
                                QUI.EndHorizontal();

                                for(int j = 0; j < ezBindExtensions[i].bindsData.Count; j++)
                                {

                                    if(SearchPatternAnimBool.target)
                                    {
                                        switch(searchFor)
                                        {
                                            case BindSearchFor.BindName: if(!Regex.IsMatch(ezBindExtensions[i].bindsData[j].bindName, SearchPattern, RegexOptions.IgnoreCase)) { continue; } break;
                                            case BindSearchFor.Variable: continue;
                                            default: continue;
                                        }

                                        foundMatchEzBind = true;
                                    }

                                    QUI.BeginHorizontal(tempWidth);
                                    {
                                        QUI.Space((tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT);
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBindExtensions.faded);
                                        //HasBind
                                        if(!string.IsNullOrEmpty(ezBindExtensions[i].bindsData[j].bindName) && !ezBindExtensions[i].bindsData[j].bindName.Equals(EzBindEditor.UNNAMED_BIND))
                                        {
                                            QUI.Label(ezBindExtensions[i].bindsData[j].bindName, Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_BIND_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                        }
                                        else
                                        {
                                            DrawInfoMessage("BindMissingBindName", (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_BIND_NAME_WIDTH_PERCENT);
                                        }
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBindExtensions.faded);
                                        //HasSource
                                        QUI.SetGUIColor(QUI.IsProSkin ? QColors.OrangeLight.Color : QColors.OrangeDark.Color);
                                        QUI.Label(BindInfo.BindHasSource(ezBindExtensions[i].bindsData[j]) ? "Added" : "None", Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_COMPONENT_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                        QUI.ResetColors();
                                        QUI.Space(BIND_HORIZONTAL_SPACE * showEzBindExtensions.faded);
                                        //HasObservers
                                        QUI.SetGUIColor(QUI.IsProSkin ? QColors.PurpleLight.Color : QColors.PurpleDark.Color);
                                        QUI.Label(ezBindExtensions[i].bindsData[j].observers.Count != 0 ? ezBindExtensions[i].bindsData[j].observers.Count + " Observers" : "None", Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_VARIABLE_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                        QUI.ResetColors();
                                        QUI.ResetColors();
                                        QUI.FlexibleSpace();
                                    }
                                    QUI.EndHorizontal();
                                    QUI.BeginHorizontal(tempWidth);
                                    {
                                        QUI.Space((tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT);
                                        QUI.DrawLine(QColors.Color.Blue, (tempWidth - (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT) * showEzBindExtensions.faded);
                                    }
                                    QUI.EndHorizontal();
                                }
                                QUI.Space(SPACE_16 * showEzBindExtensions.faded);
                            }

                            if(SearchPatternAnimBool.target && !foundMatchEzBind)
                            {
                                DrawInfoMessage("BindSearchNoResults", tempWidth);
                            }
                        }
                    }
                    QUI.EndVertical();
                }
                QUI.EndFadeGroup();
            }
            QUI.EndHorizontal();
        }

        void DrawBindEzBindExtensionRuntime(float width)
        {
            QUI.BeginHorizontal(width);
            {
                QUI.Space(SPACE_4);
                QUI.DrawTexture(EZResources.pageIconEzBindExtension.texture, BIND_LINE_HEIGHT, BIND_LINE_HEIGHT);
                QUI.Space(SPACE_2);
                if(QUI.GhostBar("EzBindExtension", QColors.Color.Blue, showEzBindExtensions, width - BIND_LINE_HEIGHT - SPACE_2, BIND_LINE_HEIGHT))
                {

                }
                showEzBindExtensions.target = false;
                showEzBindExtensions.value = false;
            }
            QUI.EndHorizontal();

            QUI.Space(-BIND_LINE_HEIGHT);
            QUI.BeginHorizontal(width);
            {
                QUI.FlexibleSpace();
                DrawInfoMessage("BindRuntimeInspector", width * 0.45f);
            }
            QUI.EndHorizontal();
        }

        void DrawBindEzBindAddSource(float width)
        {
            QUI.BeginHorizontal(width);
            {
                QUI.Space(SPACE_4);
                QUI.DrawTexture(EZResources.pageIconEzBindAddSource.texture, BIND_LINE_HEIGHT, BIND_LINE_HEIGHT);
                QUI.Space(SPACE_2);
                if(QUI.GhostBar("EzBindAddSource", QColors.Color.Orange, showEzBindAddSources, width - BIND_LINE_HEIGHT - SPACE_2, BIND_LINE_HEIGHT))
                {
                    showEzBindAddSources.target = !showEzBindAddSources.target;
                }

                if(SearchPatternAnimBool.target)
                {
                    showEzBindAddSources.target = true;
                }
            }
            QUI.EndHorizontal();
            QUI.BeginHorizontal(width);
            {
                QUI.Space(SPACE_8);
                QUI.Space(BIND_LINE_HEIGHT + (SPACE_8 + SPACE_2) * showEzBindAddSources.faded);
                if(QUI.BeginFadeGroup(showEzBindAddSources.faded))
                {
                    tempWidth = width - BIND_LINE_HEIGHT - SPACE_8 - SPACE_2 - SPACE_8;
                    QUI.BeginVertical(tempWidth);
                    {
                        if(ezBindAddSources.IsNullOrEmpty())
                        {
                            DrawInfoMessage("BindNoEzBindAddSource", tempWidth);
                        }
                        else
                        {
                            DrawBindSourcesAndObserversTableColumnsTitles(tempWidth, 16, QUI.IsProSkin ? QColors.OrangeLight.Color : QColors.OrangeDark.Color);
                            QUI.DrawLine(QColors.Color.Orange, tempWidth * showEzBindAddSources.faded);

                            foundMatchEzBind = false;
                            for(int i = 0; i < ezBindAddSources.Length; i++)
                            {
                                if(ezBindAddSources[i] == null)
                                {
                                    refreshEzBindOnInspectorUpdate = true;
                                    continue;
                                }

                                if(SearchPatternAnimBool.target)
                                {
                                    switch(searchFor)
                                    {
                                        case BindSearchFor.GameObject:
                                            if(!Regex.IsMatch(ezBindAddSources[i].gameObject.name, SearchPattern, RegexOptions.IgnoreCase)) { continue; }
                                            break;
                                        case BindSearchFor.BindName:
                                            if(!Regex.IsMatch(ezBindAddSources[i].bindName, SearchPattern, RegexOptions.IgnoreCase)) { continue; }
                                            break;
                                        case BindSearchFor.Variable:
                                            if(!Regex.IsMatch(ezBindAddSources[i].source.variableName, SearchPattern, RegexOptions.IgnoreCase)) { continue; }
                                            break;
                                        default:
                                            continue;
                                    }
                                    foundMatchEzBind = true;
                                }

                                QUI.BeginHorizontal(tempWidth, BIND_LINE_HEIGHT);
                                {
                                    QUI.BeginVertical((tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                    {
                                        QUI.Space(1);
                                        if(QUI.GhostButton(ezBindAddSources[i].gameObject.name, QColors.Color.Orange, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT, BIND_LINE_HEIGHT - SPACE_2))
                                        {
                                            Selection.activeObject = ezBindAddSources[i].gameObject;
                                        }
                                        QUI.Space(1);
                                    }
                                    QUI.EndVertical();
                                    QUI.Space(BIND_HORIZONTAL_SPACE * showEzBindAddSources.faded);
                                    if(ezBindAddSources[i].bindName.IsNullOrEmpty() || ezBindAddSources[i].bindName.Equals(EzBindEditor.UNNAMED_BIND))
                                    {
                                        DrawInfoMessage("BindMissingBindName", (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_BIND_NAME_WIDTH_PERCENT);
                                    }
                                    else
                                    {
                                        QUI.Label(ezBindAddSources[i].bindName, Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_BIND_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                    }
                                    QUI.Space(BIND_HORIZONTAL_SPACE * showEzBindAddSources.faded);
                                    if(ezBindAddSources[i].source.component == null)
                                    {
                                        DrawInfoMessage("BindMissingComponent", (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_COMPONENT_NAME_WIDTH_PERCENT);
                                    }
                                    else
                                    {
                                        QUI.Label(ezBindAddSources[i].source.component.GetType().Name, Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_COMPONENT_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                    }
                                    QUI.Space(BIND_HORIZONTAL_SPACE * showEzBindAddSources.faded);
                                    if(ezBindAddSources[i].source.variableName.IsNullOrEmpty() || ezBindAddSources[i].source.variableName.Equals("None"))
                                    {
                                        DrawInfoMessage("BindMissingVariable", (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_VARIABLE_NAME_WIDTH_PERCENT);
                                    }
                                    else
                                    {
                                        QUI.Label(ezBindAddSources[i].source.variableName, Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_VARIABLE_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                    }
                                    QUI.FlexibleSpace();
                                }
                                QUI.EndHorizontal();
                                QUI.Space(1);
                                QUI.DrawLine(QColors.Color.Orange, tempWidth * showEzBindAddSources.faded);
                            }

                            if(SearchPatternAnimBool.target && !foundMatchEzBind)
                            {
                                DrawInfoMessage("BindSearchNoResults", tempWidth);
                            }
                        }
                    }
                    QUI.EndVertical();
                }
                QUI.EndFadeGroup();
            }
            QUI.EndHorizontal();
        }

        void DrawBindEzBindAddObserver(float width)
        {
            QUI.BeginHorizontal(width);
            {
                QUI.Space(SPACE_4);
                QUI.DrawTexture(EZResources.pageIconEzBindAddObserver.texture, BIND_LINE_HEIGHT, BIND_LINE_HEIGHT);
                QUI.Space(SPACE_2);
                if(QUI.GhostBar("EzBindAddObserver", QColors.Color.Purple, showEzBindAddObservers, width - BIND_LINE_HEIGHT - SPACE_2, BIND_LINE_HEIGHT))
                {
                    showEzBindAddObservers.target = !showEzBindAddObservers.target;
                }

                if(SearchPatternAnimBool.target)
                {
                    showEzBindAddObservers.target = true;
                }
            }
            QUI.EndHorizontal();
            QUI.BeginHorizontal(width);
            {
                QUI.Space(SPACE_8);
                QUI.Space(BIND_LINE_HEIGHT + (SPACE_8 + SPACE_2) * showEzBindAddObservers.faded);
                if(QUI.BeginFadeGroup(showEzBindAddObservers.faded))
                {
                    tempWidth = width - BIND_LINE_HEIGHT - SPACE_8 - SPACE_2 - SPACE_8;
                    QUI.BeginVertical(tempWidth);
                    {
                        if(ezBindAddObservers.IsNullOrEmpty())
                        {
                            DrawInfoMessage("BindNoEzBindAddObserver", tempWidth);
                        }
                        else
                        {
                            DrawBindSourcesAndObserversTableColumnsTitles(tempWidth, 16, QUI.IsProSkin ? QColors.PurpleLight.Color : QColors.PurpleDark.Color);
                            QUI.DrawLine(QColors.Color.Purple, tempWidth * showEzBindAddObservers.faded);

                            foundMatchEzBind = false;
                            for(int i = 0; i < ezBindAddObservers.Length; i++)
                            {
                                if(ezBindAddObservers[i] == null)
                                {
                                    refreshEzBindOnInspectorUpdate = true;
                                    continue;
                                }

                                if(SearchPatternAnimBool.target)
                                {
                                    switch(searchFor)
                                    {
                                        case BindSearchFor.GameObject:
                                            if(!Regex.IsMatch(ezBindAddObservers[i].gameObject.name, SearchPattern, RegexOptions.IgnoreCase)) { continue; }
                                            break;
                                        case BindSearchFor.BindName:
                                            if(!Regex.IsMatch(ezBindAddObservers[i].bindName, SearchPattern, RegexOptions.IgnoreCase)) { continue; }
                                            break;
                                        case BindSearchFor.Variable:
                                            if(!Regex.IsMatch(ezBindAddObservers[i].observer.variableName, SearchPattern, RegexOptions.IgnoreCase)) { continue; }
                                            break;
                                        default:
                                            continue;
                                    }
                                    foundMatchEzBind = true;
                                }

                                QUI.BeginHorizontal(tempWidth);
                                {
                                    QUI.BeginVertical((tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                    {
                                        QUI.FlexibleSpace();
                                        if(QUI.GhostButton(ezBindAddObservers[i].gameObject.name, QColors.Color.Purple, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT, BIND_LINE_HEIGHT - SPACE_4))
                                        {
                                            Selection.activeObject = ezBindAddObservers[i].gameObject;
                                        }
                                        QUI.FlexibleSpace();
                                    }
                                    QUI.EndVertical();
                                    QUI.Space(BIND_HORIZONTAL_SPACE * showEzBindAddObservers.faded);
                                    if(ezBindAddObservers[i].bindName.IsNullOrEmpty() || ezBindAddObservers[i].bindName.Equals(EzBindEditor.UNNAMED_BIND))
                                    {
                                        DrawInfoMessage("BindMissingBindName", (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_BIND_NAME_WIDTH_PERCENT);
                                    }
                                    else
                                    {
                                        QUI.Label(ezBindAddObservers[i].bindName, Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_BIND_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                    }
                                    QUI.Space(BIND_HORIZONTAL_SPACE * showEzBindAddObservers.faded);
                                    if(ezBindAddObservers[i].observer.component == null)
                                    {
                                        DrawInfoMessage("BindMissingComponent", (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_COMPONENT_NAME_WIDTH_PERCENT);
                                    }
                                    else
                                    {
                                        QUI.Label(ezBindAddObservers[i].observer.component.GetType().Name, Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_COMPONENT_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                    }
                                    QUI.Space(BIND_HORIZONTAL_SPACE * showEzBindAddObservers.faded);
                                    if(ezBindAddObservers[i].observer.variableName.IsNullOrEmpty() || ezBindAddObservers[i].observer.variableName.Equals("None"))
                                    {
                                        DrawInfoMessage("BindMissingVariable", (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_VARIABLE_NAME_WIDTH_PERCENT);
                                    }
                                    else
                                    {
                                        QUI.Label(ezBindAddObservers[i].observer.variableName, Style.Text.Normal, (tempWidth - BIND_HORIZONTAL_SPACE * 3) * BIND_VARIABLE_NAME_WIDTH_PERCENT, BIND_LINE_HEIGHT);
                                    }
                                    QUI.FlexibleSpace();
                                }
                                QUI.EndHorizontal();
                                QUI.Space(1);
                                QUI.DrawLine(QColors.Color.Purple, tempWidth * showEzBindAddObservers.faded);
                            }

                            if(SearchPatternAnimBool.target && !foundMatchEzBind)
                            {
                                DrawInfoMessage("BindSearchNoResults", tempWidth);
                            }
                        }
                    }
                    QUI.EndVertical();
                }
                QUI.EndFadeGroup();
            }
            QUI.EndHorizontal();
        }

        void DrawBindSourcesAndObserversTableColumnsTitles(float width, float height, Color accentColor)
        {
            QUI.SetGUIColor(accentColor);
            QUI.BeginHorizontal(width, height);
            {
                QUI.Label("GameObject", Style.Text.Small, (width - BIND_HORIZONTAL_SPACE * 3) * BIND_GAME_OBJECT_BUTTON_WIDTH_PERCENT, height);
                QUI.Space(BIND_HORIZONTAL_SPACE);
                QUI.Label("Bind Name", Style.Text.Small, (width - BIND_HORIZONTAL_SPACE * 3) * BIND_BIND_NAME_WIDTH_PERCENT, height);
                QUI.Space(BIND_HORIZONTAL_SPACE);
                QUI.Label("Component", Style.Text.Small, (width - BIND_HORIZONTAL_SPACE * 3) * BIND_COMPONENT_NAME_WIDTH_PERCENT, height);
                QUI.Space(BIND_HORIZONTAL_SPACE);
                QUI.Label("Variable", Style.Text.Small, (width - BIND_HORIZONTAL_SPACE * 3) * BIND_VARIABLE_NAME_WIDTH_PERCENT, height);
                QUI.FlexibleSpace();
            }
            QUI.EndHorizontal();
            QUI.ResetColors();
        }
#endif
    }
}
