﻿// Copyright (c) 2016 - 2018 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using QuickEditor;

namespace Ez
{
    public partial class EZResources
    {
        //EZ BIND
        public static QTexture editorHeaderEzBind = new QTexture(EDITORHEADERS, "editorHeaderEzBind" + QResources.IsProSkinTag);
        public static QTexture editorHeaderEzBindExtension = new QTexture(EDITORHEADERS, "editorHeaderEzBindExtension" + QResources.IsProSkinTag);
        public static QTexture editorHeaderEzLocalBind = new QTexture(EDITORHEADERS, "editorHeaderEzLocalBind" + QResources.IsProSkinTag);
        public static QTexture editorHeaderEzBindAddSource = new QTexture(EDITORHEADERS, "editorHeaderEzBindAddSource" + QResources.IsProSkinTag);
        public static QTexture editorHeaderEzBindAddObserver = new QTexture(EDITORHEADERS, "editorHeaderEzBindAddObserver" + QResources.IsProSkinTag);
        public static QTexture pageIconEzBind = new QTexture(PAGEICONS, "pageIconEzBind" + QResources.IsProSkinTag);
        public static QTexture pageIconEzBindExtension = new QTexture(PAGEICONS, "pageIconEzBindExtension" + QResources.IsProSkinTag);
        public static QTexture pageIconEzBindAddSource = new QTexture(PAGEICONS, "pageIconEzBindAddSource" + QResources.IsProSkinTag);
        public static QTexture pageIconEzBindAddObserver = new QTexture(PAGEICONS, "pageIconEzBindAddObserver" + QResources.IsProSkinTag);
    }
}
