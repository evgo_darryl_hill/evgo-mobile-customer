﻿// Copyright (c) 2016 - 2018 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using System.Collections.Generic;
using System.Linq;
using Ez.Binding.Internal;
using QuickEditor;
using QuickEngine.Extensions;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace Ez.Binding
{
    [CustomEditor(typeof(EzBindAddSource))]
    public class EzBindAddSourceEditor : QEditor
    {
        EzBindAddSource EzBindAddSource { get { return (EzBindAddSource) target; } }

        QLabel qLabel;
        GUIStyle NormalTextStyle, SmallTextStyle;
        float tempWidth;
        string tempString;

        private SerializedProperty bindName;
        private BindInfo bindInfo;

        protected override void SerializedObjectFindProperties()
        {
            base.SerializedObjectFindProperties();

            bindName = serializedObject.FindProperty("bindName");

            var bindData = new BindData() { bindName = bindName.stringValue, source = EzBindAddSource.source };
            bindInfo = new BindInfo(bindData, null);

            bindInfo.VersionChangeCheck();
        }

        protected override void GenerateInfoMessages()
        {
            base.GenerateInfoMessages();

            infoMessage.Add("NoBinds", new InfoMessage() { title = "No binds have been found...", message = "", show = new AnimBool(false, Repaint), type = InfoMessageType.Info });
            infoMessage.Add("UnnamedBind", new InfoMessage() { title = "This bind is disabled...", message = "You need to rename this bind's name in order for it to work.", show = new AnimBool(true, Repaint), type = InfoMessageType.Error });
            infoMessage.Add("SelectEzBind", new InfoMessage() { title = "Select the EzBind gameObject to see all the Binds and their values", message = "", show = new AnimBool(true, Repaint), type = InfoMessageType.Info });
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            requiresContantRepaint = true;
            QUI.MarkSceneDirty(true);
        }

        public override void OnInspectorGUI()
        {
            qLabel = new QLabel();
            NormalTextStyle = QStyles.GetTextStyle(Style.Text.Normal);
            SmallTextStyle = QStyles.GetTextStyle(Style.Text.Small);

            DrawHeader(EZResources.editorHeaderEzBindAddSource.texture, WIDTH_420, HEIGHT_42);

            if(EditorApplication.isPlaying)
            {
                DrawRuntimeEditor(WIDTH_420);
                QUI.Space(SPACE_4);
                GUI.enabled = false;
            }

            serializedObject.Update();
            DrawEditor(WIDTH_420);
            if(!EditorApplication.isPlaying)
            {
                serializedObject.ApplyModifiedProperties();
            }
            else
            {
                GUI.enabled = true;
            }
            QUI.Space(SPACE_8);
        }

        void DrawRuntimeEditor(float width)
        {
            if(QUI.SlicedButton("Select EzBind", QColors.Color.Green, width, 20))
            {
                Selection.activeGameObject = FindObjectOfType<EzBind>().gameObject;
            }
            QUI.Space(SPACE_4);
            DrawInfoMessage("SelectEzBind", width);
        }

        void DrawEditor(float width)
        {
            NormalTextStyle.wordWrap = true;
            SmallTextStyle.wordWrap = true;

            DrawBindName(width);
            QUI.Space(SPACE_8);
            DrawSource(width);

            NormalTextStyle.wordWrap = false;
            SmallTextStyle.wordWrap = false;
        }

        void DrawBindName(float width)
        {
            QUI.BeginHorizontal(width);
            {
                qLabel.text = "Bind Name";
                QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, EzBindLocalBindsEditor.LABEL_HEIGHT);
                QUI.BeginChangeCheck();

                tempString = bindName.stringValue;
                EzBindLocalBindsEditor.SetBackgroundRedIfCondition(string.IsNullOrEmpty(tempString));
                tempString = QUI.TextField(tempString, width - qLabel.x - SPACE_4);
                QUI.ResetColors();
                if(QUI.EndChangeCheck())
                {
                    tempString = tempString.RemoveAllTypesOfWhitespaces();
                    Undo.RecordObject(EzBindAddSource, "UpdateBindName");
                    if(tempString.Equals(EzBindLocalBindsEditor.UNNAMED_LOCAL_BIND)) { bindName.stringValue = string.Empty; }
                    else { bindName.stringValue = tempString; }
                }
            }
            QUI.EndHorizontal();
        }

        void DrawSource(float width)
        {
            float backgroundHeight = bindInfo.bindData.source.useHolder ? 78 : 60;

            QUI.Box(QStyles.GetBackgroundStyle(Style.BackgroundType.Low, QColors.Color.Orange), width, backgroundHeight);
            QUI.Space(-backgroundHeight);
            QUI.Space(SPACE_2);
            // Draw GO
            QUI.BeginHorizontal(width);
            {
                QUI.Space(EzBindLocalBindsEditor.LABEL_INDENT);
                qLabel.text = "Source";
                QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, EzBindLocalBindsEditor.LABEL_HEIGHT);
                tempWidth = width - qLabel.x - SPACE_16 - EzBindLocalBindsEditor.LABEL_INDENT - 10;  //this will be the ObjectField width

                if(bindInfo.bindData.source.gameObject != EzBindAddSource.gameObject) { bindInfo.UpdateSourceWithGO(EzBindAddSource.gameObject, null, null, null); }
                if(!EditorApplication.isPlaying)
                {
                    GUI.enabled = false;
                }
                QUI.ObjectField(bindInfo.bindData.source.gameObject, typeof(GameObject), true, tempWidth);
                if(!EditorApplication.isPlaying)
                {
                    GUI.enabled = true;
                }
                if(QUI.ButtonReset())
                {
                    Undo.RecordObject(EzBindAddSource, "ResetSource");
                    bindInfo.bindData.source.Reset();
                }
                QUI.FlexibleSpace();
            }
            QUI.EndHorizontal();

            QUI.Space(SPACE_2);
            // Draw Component
            QUI.BeginHorizontal(width);
            {
                QUI.Space(EzBindLocalBindsEditor.LABEL_INDENT);
                qLabel.text = "Component";
                QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, EzBindLocalBindsEditor.LABEL_HEIGHT);
                if(EzBindAddSource.gameObject == null)
                {
                    qLabel.text = "Reference a GameObject";
                    QUI.Label(qLabel.text, Style.Text.Help, qLabel.x, EzBindLocalBindsEditor.LABEL_HEIGHT);
                }
                else
                {
                    int tempComponentIndex = bindInfo.GetSourceComponentIndex();
                    QUI.BeginChangeCheck();
                    EzBindLocalBindsEditor.SetBackgroundRedIfCondition(tempComponentIndex == 0);
                    tempComponentIndex = EditorGUILayout.Popup(tempComponentIndex, bindInfo.GetSourceComponentNamesArray(), GUILayout.Width(width - qLabel.x - EzBindLocalBindsEditor.LABEL_INDENT - SPACE_8));
                    QUI.ResetColors();
                    if(QUI.EndChangeCheck())
                    {
                        Undo.RecordObject(EzBindAddSource, "UpdateComponent");
                        bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.GetSourceComponentsArray()[tempComponentIndex], null, null);
                    }
                }
                QUI.FlexibleSpace();
            }
            QUI.EndHorizontal();

            QUI.Space(SPACE_2);

            tempWidth = width;
            if(bindInfo.SourceMayUseHolder())
            {
                tempWidth -= SPACE_16;
            }

            // Draw Holder
            QUI.BeginHorizontal(tempWidth);
            {
                QUI.Space(EzBindLocalBindsEditor.LABEL_INDENT);
                qLabel.text = "Variable";
                QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, EzBindLocalBindsEditor.LABEL_HEIGHT);
                if(bindInfo.bindData.source.component == null)
                {
                    qLabel.text = "Select a Component";
                    QUI.Label(qLabel.text, Style.Text.Help, qLabel.x, EzBindLocalBindsEditor.LABEL_HEIGHT);
                }
                else
                {
                    int tempHolderNameIndex = bindInfo.GetSourceHolderNameIndex();
                    QUI.BeginChangeCheck();
                    EzBindLocalBindsEditor.SetBackgroundRedIfCondition(tempHolderNameIndex == 0);
                    tempHolderNameIndex = EditorGUILayout.Popup(tempHolderNameIndex, bindInfo.GetSourceHolderNamesArray(), GUILayout.Width((tempWidth - qLabel.x - EzBindLocalBindsEditor.LABEL_INDENT - SPACE_8)));
                    QUI.ResetColors();
                    if(QUI.EndChangeCheck())
                    {
                        Undo.RecordObject(EzBindAddSource, "UpdateVariable");
                        bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.bindData.source.component, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null);
                    }

                    if(tempWidth != width)
                    {
                        QUI.Space(-2);
                        if(bindInfo.bindData.source.useHolder)
                        {
                            if(QUI.ButtonMinus())
                            {
                                Undo.RecordObject(EzBindAddSource, "DoNotUseHolder");
                                bindInfo.bindData.source.useHolder = false;
                                bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.bindData.source.component, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null);
                            }
                        }
                        else
                        {
                            if(QUI.ButtonPlus())
                            {
                                Undo.RecordObject(EzBindAddSource, "UseHolder");
                                bindInfo.bindData.source.useHolder = true;
                                bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.bindData.source.component, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null);
                            }
                        }
                    }
                }
                QUI.FlexibleSpace();
            }
            QUI.EndHorizontal();

            // Draw Variable under Holder (if using holder)
            if(bindInfo.SourceMayUseHolder() && bindInfo.bindData.source.useHolder)
            {
                QUI.Space(SPACE_2);
                QUI.BeginHorizontal(tempWidth);
                {
                    QUI.Space(qLabel.x + EzBindLocalBindsEditor.LABEL_INDENT + SPACE_4);

                    int tempVarNameIndex = bindInfo.GetSourceVariableNameIndex();
                    QUI.BeginChangeCheck();
                    EzBindLocalBindsEditor.SetBackgroundRedIfCondition(tempVarNameIndex == 0);
                    tempVarNameIndex = EditorGUILayout.Popup(tempVarNameIndex, bindInfo.GetSourceVariableNamesArray(), GUILayout.Width((tempWidth - qLabel.x - EzBindLocalBindsEditor.LABEL_INDENT - SPACE_8)));
                    QUI.ResetColors();
                    if(QUI.EndChangeCheck())
                    {
                        Undo.RecordObject(EzBindAddSource, "UpdateVariable");
                        bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.bindData.source.component, bindInfo.GetSourceHolderNamesArray()[bindInfo.GetSourceHolderNameIndex()], bindInfo.GetSourceVariableNamesArray()[tempVarNameIndex]);
                    }
                    QUI.FlexibleSpace();
                }
                QUI.EndHorizontal();
            }
        }
    }
}
