﻿// Copyright (c) 2016 - 2018 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using System.Collections.Generic;
using System.Linq;
using Ez.Binding.Internal;
using QuickEditor;
using QuickEngine.Extensions;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace Ez.Binding
{
    [CustomEditor(typeof(EzBindAddObserver))]
    public class EzBindAddObserverEditor : QEditor
    {
        EzBindAddObserver EzBindAddObserver { get { return (EzBindAddObserver) target; } }

        QLabel qLabel;
        GUIStyle NormalTextStyle, SmallTextStyle;
        float tempWidth;
        string tempString;

        private SerializedProperty bindName, updateWhenDisabled;
        private BindInfo bindInfo;

        protected override void SerializedObjectFindProperties()
        {
            base.SerializedObjectFindProperties();

            bindName = serializedObject.FindProperty("bindName");
            updateWhenDisabled = serializedObject.FindProperty("updateWhenDisabled");

            var bindData = new BindData() { bindName = bindName.stringValue, observers = new List<BoundItem>() { EzBindAddObserver.observer } };
            bindInfo = new BindInfo(bindData, null);

            bindInfo.VersionChangeCheck();
        }

        protected override void GenerateInfoMessages()
        {
            base.GenerateInfoMessages();

            infoMessage.Add("NoBinds", new InfoMessage() { title = "No binds have been found...", message = "", show = new AnimBool(false, Repaint), type = InfoMessageType.Info });
            infoMessage.Add("UnnamedBind", new InfoMessage() { title = "This bind is disabled...", message = "You need to rename this bind's name in order for it to work.", show = new AnimBool(true, Repaint), type = InfoMessageType.Error });
            infoMessage.Add("SelectEzBind", new InfoMessage() { title = "Select the EzBind gameObject to see all the Binds and their values", message = "", show = new AnimBool(true, Repaint), type = InfoMessageType.Info });
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            requiresContantRepaint = true;
            QUI.MarkSceneDirty(true);
        }

        public override void OnInspectorGUI()
        {
            qLabel = new QLabel();
            NormalTextStyle = QStyles.GetTextStyle(Style.Text.Normal);
            SmallTextStyle = QStyles.GetTextStyle(Style.Text.Small);

            DrawHeader(EZResources.editorHeaderEzBindAddObserver.texture, WIDTH_420, HEIGHT_42);

            if(EditorApplication.isPlaying)
            {
                DrawRuntimeEditor(WIDTH_420);
                QUI.Space(SPACE_4);
                GUI.enabled = false;
            }

            serializedObject.Update();
            DrawEditor(WIDTH_420);
            if(!EditorApplication.isPlaying)
            {
                serializedObject.ApplyModifiedProperties();
            }
            else
            {
                GUI.enabled = true;
            }
            QUI.Space(SPACE_4);
        }

        void DrawRuntimeEditor(float width)
        {
            if(QUI.SlicedButton("Select EzBind", QColors.Color.Green, width, 20))
            {
                Selection.activeGameObject = FindObjectOfType<EzBind>().gameObject;
            }
            QUI.Space(SPACE_4);
            DrawInfoMessage("SelectEzBind", width);
        }

        void DrawEditor(float width)
        {
            NormalTextStyle.wordWrap = true;
            SmallTextStyle.wordWrap = true;

            DrawBindName(width);
            QUI.Space(SPACE_8);
            DrawObserver(width);
            QUI.Space(SPACE_8);
            DrawOptions(width);

            NormalTextStyle.wordWrap = false;
            SmallTextStyle.wordWrap = false;
        }

        void DrawBindName(float width)
        {
            QUI.BeginHorizontal(width);
            {
                qLabel.text = "Bind Name";
                QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, EzBindLocalBindsEditor.LABEL_HEIGHT);
                QUI.BeginChangeCheck();

                EzBindLocalBindsEditor.SetBackgroundRedIfCondition(string.IsNullOrEmpty(tempString));
                tempString = bindName.stringValue;
                tempString = QUI.TextField(tempString, width - qLabel.x - SPACE_4);
                QUI.ResetColors();
                if(QUI.EndChangeCheck())
                {
                    tempString = tempString.RemoveAllTypesOfWhitespaces();
                    Undo.RecordObject(EzBindAddObserver, "UpdateBindName");
                    if(tempString.Equals(EzBindLocalBindsEditor.UNNAMED_LOCAL_BIND)) { bindName.stringValue = string.Empty; }
                    else { bindName.stringValue = tempString; }
                }
            }
            QUI.EndHorizontal();
        }

        void DrawObserver(float width)
        {
            float observersBackgroundHeight = bindInfo.bindData.observers[0].useHolder ? 78 : 60;

            QUI.Box(QStyles.GetBackgroundStyle(Style.BackgroundType.Low, QColors.Color.Purple), width, observersBackgroundHeight);
            QUI.Space(-observersBackgroundHeight);
            QUI.Space(SPACE_2);
            // Draw GO
            QUI.BeginHorizontal(width);
            {
                QUI.Space(EzBindLocalBindsEditor.LABEL_INDENT);
                qLabel.text = "Observer";
                QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, EzBindLocalBindsEditor.LABEL_HEIGHT);
                tempWidth = width - qLabel.x - SPACE_16 - EzBindLocalBindsEditor.LABEL_INDENT - 10;  //this will be the ObjectField width
                if(bindInfo.bindData.observers[0].gameObject != EzBindAddObserver.gameObject) { bindInfo.UpdateObserverWithGO(0, EzBindAddObserver.gameObject, null, null, null); }
                if(!EditorApplication.isPlaying)
                {
                    GUI.enabled = false;
                }
                QUI.ObjectField(bindInfo.bindData.observers[0].gameObject, typeof(GameObject), true, tempWidth);
                if(!EditorApplication.isPlaying)
                {
                    GUI.enabled = true;
                }
                if(QUI.ButtonReset())
                {
                    Undo.RecordObject(EzBindAddObserver, "ResetObserver");
                    bindInfo.bindData.observers[0].Reset();
                }
                QUI.FlexibleSpace();
            }
            QUI.EndHorizontal();

            QUI.Space(SPACE_2);
            // Draw Component
            QUI.BeginHorizontal(width);
            {
                QUI.Space(EzBindLocalBindsEditor.LABEL_INDENT);
                qLabel.text = "Component";
                QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, EzBindLocalBindsEditor.LABEL_HEIGHT);
                if(EzBindAddObserver.gameObject == null)
                {
                    qLabel.text = "Reference a GameObject";
                    QUI.Label(qLabel.text, Style.Text.Help, qLabel.x, EzBindLocalBindsEditor.LABEL_HEIGHT);
                }
                else
                {
                    int tempComponentIndex = bindInfo.GetObserverComponentIndex(0);
                    QUI.BeginChangeCheck();
                    EzBindLocalBindsEditor.SetBackgroundRedIfCondition(tempComponentIndex == 0);
                    tempComponentIndex = EditorGUILayout.Popup(tempComponentIndex, bindInfo.GetObserverComponentNamesArray(0), GUILayout.Width(width - qLabel.x - EzBindLocalBindsEditor.LABEL_INDENT - SPACE_8));
                    QUI.ResetColors();
                    if(QUI.EndChangeCheck())
                    {
                        Undo.RecordObject(EzBindAddObserver, "UpdateComponent");
                        bindInfo.UpdateObserverWithGO(0, bindInfo.bindData.observers[0].gameObject, bindInfo.GetObserverComponentsArray(0)[tempComponentIndex], null, null);
                    }
                }
                QUI.FlexibleSpace();
            }
            QUI.EndHorizontal();

            QUI.Space(SPACE_2);

            tempWidth = width;
            if(bindInfo.ObserverMayUseHolder(0))
            {
                tempWidth -= SPACE_16;
            }
            // Draw Holder
            QUI.BeginHorizontal(tempWidth);
            {
                QUI.Space(EzBindLocalBindsEditor.LABEL_INDENT);
                qLabel.text = "Variable";
                QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, EzBindLocalBindsEditor.LABEL_HEIGHT);
                if(bindInfo.bindData.observers[0].component == null)
                {
                    qLabel.text = "Select a Component";
                    QUI.Label(qLabel.text, Style.Text.Help, qLabel.x, EzBindLocalBindsEditor.LABEL_HEIGHT);
                }
                else
                {
                    int tempHolderNameIndex = bindInfo.GetObserverHolderNameIndex(0);
                    QUI.BeginChangeCheck();
                    EzBindLocalBindsEditor.SetBackgroundRedIfCondition(tempHolderNameIndex == 0);
                    tempHolderNameIndex = EditorGUILayout.Popup(tempHolderNameIndex, bindInfo.GetObserverHolderNamesArray(0), GUILayout.Width((tempWidth - qLabel.x - EzBindLocalBindsEditor.LABEL_INDENT - SPACE_8)));
                    QUI.ResetColors();
                    if(QUI.EndChangeCheck())
                    {
                        Undo.RecordObject(EzBindAddObserver, "UpdateVariable");
                        bindInfo.UpdateObserverWithGO(0, bindInfo.bindData.observers[0].gameObject, bindInfo.bindData.observers[0].component, bindInfo.GetObserverHolderNamesArray(0)[tempHolderNameIndex], null);
                    }

                    if(tempWidth != width)
                    {
                        QUI.Space(-2);
                        if(bindInfo.bindData.observers[0].useHolder)
                        {
                            if(QUI.ButtonMinus())
                            {
                                Undo.RecordObject(EzBindAddObserver, "DoNotUseHolder");
                                bindInfo.bindData.observers[0].useHolder = false;
                                bindInfo.UpdateObserverWithGO(0, bindInfo.bindData.observers[0].gameObject, bindInfo.bindData.observers[0].component, bindInfo.GetObserverHolderNamesArray(0)[tempHolderNameIndex], null);
                            }
                        }
                        else
                        {
                            if(QUI.ButtonPlus())
                            {
                                Undo.RecordObject(EzBindAddObserver, "UseHolder");
                                bindInfo.bindData.observers[0].useHolder = true;
                                bindInfo.UpdateObserverWithGO(0, bindInfo.bindData.observers[0].gameObject, bindInfo.bindData.observers[0].component, bindInfo.GetObserverHolderNamesArray(0)[tempHolderNameIndex], null);
                            }
                        }
                    }
                }
                QUI.FlexibleSpace();
            }
            QUI.EndHorizontal();

            // Draw Variable under Holder (if using holder)
            if(bindInfo.ObserverMayUseHolder(0) && bindInfo.bindData.observers[0].useHolder)
            {
                QUI.Space(SPACE_2);
                QUI.BeginHorizontal(tempWidth);
                {
                    QUI.Space(qLabel.x + EzBindLocalBindsEditor.LABEL_INDENT + SPACE_2);

                    int tempVarNameIndex = bindInfo.GetObserverVariableNameIndex(0);
                    QUI.BeginChangeCheck();
                    EzBindLocalBindsEditor.SetBackgroundRedIfCondition(tempVarNameIndex == 0);
                    tempVarNameIndex = EditorGUILayout.Popup(tempVarNameIndex, bindInfo.GetObserverVariableNamesArray(0), GUILayout.Width((tempWidth - qLabel.x - EzBindLocalBindsEditor.LABEL_INDENT - SPACE_8)));
                    QUI.ResetColors();
                    if(QUI.EndChangeCheck())
                    {
                        Undo.RecordObject(EzBindAddObserver, "UpdateVariable");
                        bindInfo.UpdateObserverWithGO(0, bindInfo.bindData.observers[0].gameObject, bindInfo.bindData.observers[0].component, bindInfo.GetObserverHolderNamesArray(0)[bindInfo.GetObserverHolderNameIndex(0)], bindInfo.GetObserverVariableNamesArray(0)[tempVarNameIndex]);
                    }
                    QUI.FlexibleSpace();
                }
                QUI.EndHorizontal();
            }
        }

        void DrawOptions(float width)
        {
            QUI.BeginHorizontal(width);
            {
                QUI.Toggle(updateWhenDisabled);
                QUI.Space(SPACE_4);
                QUI.Label("Update this Observer when this GameObject is disabled", Style.Text.Normal, width - 12, QUI.SingleLineHeight);
                QUI.FlexibleSpace();
            }
            QUI.EndHorizontal();
        }
    }
}
