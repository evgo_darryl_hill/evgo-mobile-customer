﻿// Copyright (c) 2016 - 2018 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using Ez.Binding.Vars;
using QuickEditor;
using QuickEngine.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;
using UnityEngine.Events;

namespace Ez.Binding
{
    [CustomEditor(typeof(EzBind))]
    [DisallowMultipleComponent]
    public class EzBindEditor : QEditor
    {

#if EZ_DATA_MANAGER
        DataManager.EzDataManager ezDataManager;
#endif

        EzBind EzBind { get { return (EzBind) target; } }

        public const string UNNAMED_BIND = "UnnamedBind";
        public const int RENAME_BIND_BUTTON_WIDTH = 60;
        public const int LABEL_HEIGHT = 16;
        public const int LABEL_INDENT = 6;

        SerializedProperty bindsData;

        Color AccentColorGray { get { return QUI.IsProSkin ? QColors.UnityLight.Color : QColors.UnityLight.Color; } }
        QLabel qLabel;
        GUIStyle NormalTextStyle, SmallTextStyle;
        float tempWidth;

        public List<BindInfo> bindsInfo = new List<BindInfo>();
        private bool initialized = false;

        Dictionary<string, Bind> BindingVariables;

        protected override void InitializeVariables()
        {
            base.InitializeVariables();

            if(EzBind.bindsData == null) { EzBind.bindsData = new List<BindData>(); }
            if(bindsInfo == null) { bindsInfo = new List<BindInfo>(); }
            if(!initialized || EzBind.bindsData.Count != bindsInfo.Count)
            {
                bindsInfo = new List<BindInfo>();
                for(int i = 0; i < EzBind.bindsData.Count; i++)
                {
                    bindsInfo.Add(new BindInfo(EzBind.bindsData[i], Repaint));
                    bindsInfo[i].VersionChangeCheck();
                }
                initialized = true;
            }

            BindingVariables = new Dictionary<string, Bind>();

            FindEzDataManager();
        }

        protected override void SerializedObjectFindProperties()
        {
            base.SerializedObjectFindProperties();

            bindsData = serializedObject.FindProperty("bindsData");
        }

        protected override void GenerateInfoMessages()
        {
            base.GenerateInfoMessages();

            infoMessage.Add("NoBinds", new InfoMessage() { title = "No binds have been found...", message = "", show = new AnimBool(false, Repaint), type = InfoMessageType.Info });
            infoMessage.Add("UnnamedBind", new InfoMessage() { title = "This bind is disabled...", message = "You need to rename this bind's name in order for it to work.", show = new AnimBool(true, Repaint), type = InfoMessageType.Error });
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            requiresContantRepaint = true;
            QUI.MarkSceneDirty(true);
        }

        public override void OnInspectorGUI()
        {
            qLabel = new QLabel();
            NormalTextStyle = QStyles.GetTextStyle(Style.Text.Normal);
            SmallTextStyle = QStyles.GetTextStyle(Style.Text.Small);
            NormalTextStyle.wordWrap = true;
            SmallTextStyle.wordWrap = true;

            DrawHeader(EZResources.editorHeaderEzBind.texture, WIDTH_420, HEIGHT_42);

            if(EditorApplication.isPlaying)
            {
                DrawRuntimeEditor(WIDTH_420);
                QUI.Space(SPACE_4);
                return;
            }
            serializedObject.Update();
            DrawBinds(WIDTH_420);
            serializedObject.ApplyModifiedProperties();
            QUI.Space(SPACE_4);

            NormalTextStyle.wordWrap = false;
            SmallTextStyle.wordWrap = false;
        }

        void FindEzDataManager()
        {
#if EZ_DATA_MANAGER
            ezDataManager = FindObjectOfType<DataManager.EzDataManager>();
#endif
        }

        void DrawRuntimeEditor(float width)
        {
            BindingVariables = EzBind.BindsHolder;

            infoMessage["NoBinds"].show.target = BindingVariables == null || BindingVariables.Count == 0;
            DrawInfoMessage("NoBinds", width);
            if(infoMessage["NoBinds"].show.target) { return; }

            foreach(var bindVariable in BindingVariables)
            {
                QUI.Box(QStyles.GetBackgroundStyle(Style.BackgroundType.Low, QColors.Color.Gray), width, 20);
                QUI.Space(-20);

                QUI.BeginHorizontal(width);
                {
                    QUI.Space(SPACE_8);

                    qLabel.text = bindVariable.Key + ":";
                    qLabel.style = Style.Text.Small;
                    QUI.Label(qLabel.text, Style.Text.Small, qLabel.x, 20);
                    QUI.Space(-SPACE_2);
                    qLabel.text = (bindVariable.Value == null || bindVariable.Value.Value == null) ? "---" : bindVariable.Value.Value.ToString();
                    qLabel.style = Style.Text.Help;
                    QUI.Label(qLabel.text, Style.Text.Help, qLabel.x, 20);

                    QUI.FlexibleSpace();

                    qLabel.text = (bindVariable.Value.HasSource ? "" : "No Source  |");
                    qLabel.style = Style.Text.Small;
                    QUI.Label(qLabel.text, Style.Text.Small, qLabel.x, 20);

                    QUI.Space(SPACE_4);

                    qLabel.text = bindVariable.Value.ObserversCount + " Observers";
                    qLabel.style = Style.Text.Small;
                    QUI.Label(qLabel.text, Style.Text.Small, qLabel.x, 20);

                    QUI.Space(SPACE_8);
                }
                QUI.EndHorizontal();

                QUI.Space(SPACE_8);
            }
        }

        void DrawBinds(float width)
        {
            if(QUI.SlicedButton("Add Bind", QColors.Color.Green, width, 20)) { AddBind(); }
            QUI.Space(SPACE_4);

            if(bindsInfo.Count != EzBind.bindsData.Count) { InitializeVariables(); }

            infoMessage["NoBinds"].show.target = bindsInfo.IsNullOrEmpty();
            DrawInfoMessage("NoBinds", width);
            if(infoMessage["NoBinds"].show.target) { return; }

            for(int i = 0; i < bindsInfo.Count; i++)
            {
                DrawBind(bindsInfo[i], i, width);
                QUI.Space(SPACE_8);
            }
        }

        void DrawBind(BindInfo bindInfo, int index, float width)
        {
            qLabel.text = bindInfo.BindName; //bind name
            QUI.BeginHorizontal(width, 16);
            {
                if(QUI.GhostBar(qLabel.text, (bindInfo.BindName.IsNullOrEmpty() || bindInfo.BindName.Equals(UNNAMED_BIND) ? QColors.Color.Red : QColors.Color.Gray), bindInfo.show, width - SPACE_16 - (RENAME_BIND_BUTTON_WIDTH * bindInfo.show.faded), 20))
                {
                    bindInfo.show.target = !bindInfo.show.target;
                    if(!bindInfo.show.target)
                    {
                        bindInfo.renameBind.target = false;
                    }
                    else
                    {
                        if(bindInfo.BindName.IsNullOrEmpty() || bindInfo.BindName.Equals(UNNAMED_BIND))
                        {
                            bindInfo.newBindName = bindInfo.BindName;
                            bindInfo.renameBind.target = true;
                        }
                    }
                }
                if(bindInfo.show.faded > 0.6f)
                {
                    if(QUI.GhostButton("Rename", QColors.Color.Gray, RENAME_BIND_BUTTON_WIDTH * bindInfo.show.faded, 20, true))
                    {
                        bindInfo.newBindName = bindInfo.BindName;
                        if(bindInfo.BindName.IsNullOrEmpty() || bindInfo.BindName.Equals(UNNAMED_BIND))
                        {
                            bindInfo.renameBind.target = true;
                            return;
                        }
                        bindInfo.renameBind.target = !bindInfo.renameBind.target;
                    }
                }
                QUI.FlexibleSpace();
                QUI.BeginVertical(16, 20);
                {
                    QUI.Space(SPACE_2);
                    if(QUI.ButtonCancel())
                    {
                        if(QUI.DisplayDialog("Delete Bind", "Are you sure you want to delete this bind (" + bindInfo.BindName + ")?", "Yes", "No"))
                        {
                            DeleteBind(bindInfo);
                        }
                    }
                }
                QUI.EndVertical();
            }
            QUI.EndHorizontal();
            QUI.Space(-21);
            QUI.BeginHorizontal(width);  //bind info (shown on the GhostBar)
            {
                QUI.FlexibleSpace();

                qLabel.text = (bindInfo.HasSource ? "" : "No Source  |");
                qLabel.style = Style.Text.Small;
                QUI.Label(qLabel.text, Style.Text.Small, qLabel.x, 20);

                QUI.Space(SPACE_4);

                qLabel.text = bindInfo.ObserversCount + " Observers";
                qLabel.style = Style.Text.Small;
                QUI.Label(qLabel.text, Style.Text.Small, qLabel.x, 20);

                QUI.Space(24 + (RENAME_BIND_BUTTON_WIDTH * bindInfo.show.faded));
            }
            QUI.EndHorizontal();

            QUI.BeginHorizontal(width);
            {
                QUI.Space(SPACE_8);
                if(QUI.BeginFadeGroup(bindInfo.show.faded))
                {
                    QUI.BeginVertical(width - SPACE_8);
                    {
                        DrawBindInfo(bindInfo, index, width - SPACE_8);
                    }
                    QUI.EndVertical();
                }
                QUI.EndFadeGroup();
            }
            QUI.EndHorizontal();
        }

        void DrawBindInfo(BindInfo bindInfo, int index, float width)
        {
            QUI.Space(SPACE_8 * bindInfo.show.faded);

            if(QUI.BeginFadeGroup(bindInfo.renameBind.faded))
            {
                QUI.BeginVertical(width);
                {
                    QUI.Box(QStyles.GetBackgroundStyle(Style.BackgroundType.Low, QColors.Color.Gray), width, 20);
                    QUI.Space(-20);

                    QUI.BeginHorizontal(width);
                    {
                        QUI.Space(LABEL_INDENT);
                        qLabel.text = "Rename bind to";
                        qLabel.style = Style.Text.Normal;
                        QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, 20);
                        QUI.SetNextControlName("renameBind" + index);
                        bindInfo.newBindName = QUI.TextField(bindInfo.newBindName, AccentColorGray, width - SPACE_4 - qLabel.x - 32 - 12);

                        if(bindInfo.renameBind.target
                         && !bindInfo.renameBind.value)
                        {
                            QUI.FocusTextInControl("renameBind" + index);
                        }

                        if(QUI.ButtonCancel() ||
                            QUI.DetectKeyUp(Event.current, KeyCode.Escape))
                        {
                            bindInfo.newBindName = bindInfo.BindName;
                            bindInfo.renameBind.target = false;
                            QUI.ExitGUI();
                        }

                        if(QUI.ButtonOk()
                            || (QUI.DetectKeyUp(Event.current, KeyCode.Return) && (QUI.GetNameOfFocusedControl() == "renameBind" + index)))
                        {
                            bindInfo.newBindName = bindInfo.newBindName.RemoveAllTypesOfWhitespaces();
                            if(bindInfo.newBindName.IsNullOrEmpty())
                            {
                                QUI.DisplayDialog("Rename Bind", "You cannot set an empty bind name.", "Ok");
                                QUI.ExitGUI();
                                return;
                            }
                            if(bindInfo.newBindName.Equals(UNNAMED_BIND))
                            {
                                QUI.DisplayDialog("Rename Bind", "You cannot set the '" + UNNAMED_BIND + "' bind name.", "Ok");
                                QUI.ExitGUI();
                                return;
                            }
                            Undo.RecordObject(EzBind, "RenameBind");
                            bindInfo.renameBind.target = false;
                            bindInfo.RenameBind();
                            QUI.ExitGUI();
                        }
                        QUI.FlexibleSpace();
                    }
                    QUI.EndHorizontal();
                    QUI.Space(6 * bindInfo.renameBind.faded);
                }
                QUI.EndVertical();
            }
            QUI.EndFadeGroup();

            if(bindInfo.BindName.IsNullOrEmpty() || bindInfo.BindName.Equals(UNNAMED_BIND))
            {
                QUI.Space(SPACE_2 * bindInfo.show.faded);
                DrawInfoMessage("UnnamedBind", width);
                QUI.Space(SPACE_8 * bindInfo.show.faded);
                return;
            }


            DrawBindSource(bindInfo, width - SPACE_8);
            QUI.Space(SPACE_8 * bindInfo.show.faded);
            DrawBindOnValueChanged(bindInfo, index, width - SPACE_8);
            QUI.Space(SPACE_4 * bindInfo.show.faded);
            DrawBindObservers(bindInfo, width);
            QUI.Space(SPACE_16 * bindInfo.show.faded);
        }

        void DrawBindSource(BindInfo bindInfo, float width)
        {
            int boxHeight;
            if(bindInfo.bindData.source.useScriptableObject)
            {
                if(bindInfo.bindData.source.scriptableObject == null) { boxHeight = 24; }
                else if(!bindInfo.bindData.source.useHolder) { boxHeight = 42; }
                else { boxHeight = 60; }
            }
            else
            {
                if(bindInfo.bindData.source.gameObject == null) { boxHeight = 24; }
                else if(bindInfo.bindData.source.component == null) { boxHeight = 42; }
                else if(!bindInfo.bindData.source.useHolder) { boxHeight = 60; }
                else { boxHeight = 78; }
            }
            QUI.Box(QStyles.GetBackgroundStyle(Style.BackgroundType.Low, QColors.Color.Orange), width + SPACE_8, boxHeight);
            QUI.Space(-boxHeight);
            QUI.Space(SPACE_2);

            QUI.BeginHorizontal(width);
            {
                QUI.Space(LABEL_INDENT);
                qLabel.text = "Source";
                qLabel.style = Style.Text.Normal;
                QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, LABEL_HEIGHT);

                Object tempObj;
                if(bindInfo.bindData.source.useScriptableObject) { tempObj = bindInfo.bindData.source.scriptableObject; }
                else { tempObj = bindInfo.bindData.source.gameObject; }

                tempWidth = width - qLabel.x - SPACE_16 - LABEL_INDENT - SPACE_4; //this will be the ObjectField width - it is used to add/remove space for the EzDataManager button
#if EZ_DATA_MANAGER || EZ_PLAYMAKER_SUPPORT || EZ_BOLT_SUPPORT
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                if(ezDataManager != null) //if EzDataManager was found in this scene - adjust the ObjectField width
                {
#endif
                    tempWidth -= SPACE_16;
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                }
#endif
#endif
                tempWidth *= bindInfo.show.faded;
                QUI.BeginChangeCheck();
                SetBackgroundRedIfCondition(tempObj == null);
                tempObj = QUI.ObjectField(tempObj, typeof(Object), true, tempWidth);
                QUI.ResetColors();
                // Draw GO/SO selection
                if(QUI.EndChangeCheck())
                {
                    if(tempObj == null)
                    {
                        Undo.RecordObject(EzBind, "UpdateSource");
                        bindInfo.ResetSource();
                    }
                    else if(typeof(GameObject).IsInstanceOfType(tempObj))
                    {
                        Undo.RecordObject(EzBind, "UpdateSource");
                        bindInfo.UpdateSourceWithGO((GameObject) tempObj, null, null, null);
                    }
                    else if(typeof(ScriptableObject).IsInstanceOfType(tempObj))
                    {
                        Undo.RecordObject(EzBind, "UpdateSource");
                        bindInfo.UpdateSourceWithSO((ScriptableObject) tempObj, null, null);
                    }
                    else
                    {
                        QUI.DisplayDialog("Invalid selection", "Only GameObjects and ScriptableObjects are allowed!", "OK");
                    }
                }
                if(QUI.ButtonReset())
                {
                    Undo.RecordObject(EzBind, "ResetSource");
                    bindInfo.ResetSource();
                }
#if EZ_DATA_MANAGER || EZ_PLAYMAKER_SUPPORT || EZ_BOLT_SUPPORT
#pragma warning disable 0219
                bool addSeparator = false;
#pragma warning restore 0219
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                if(ezDataManager != null) //if EzDataManager was found in this scene - show the extra button that allows setting EzDataManager as the source
                {
#endif
                    if(QUI.ButtonData())
                    {
                        GenericMenu menu = new GenericMenu();
#if EZ_DATA_MANAGER
                        if(ezDataManager != null) //if EzDataManager was found in this scene - show the extra button that allows setting EzDataManager as the source
                        {
                            menu.AddItem(new GUIContent("EzDataManager"), false, () => { SetEzDataManagerAsSource(EzBind, ezDataManager.gameObject, bindInfo); });
                            addSeparator = true;
                        }
#endif
#if EZ_PLAYMAKER_SUPPORT
                    if(addSeparator) { menu.AddSeparator(""); }
                    menu.AddItem(new GUIContent("PlayMaker Globals"), false, () => { SetPlayMakerGlobalsAsSource(EzBind, bindInfo); });
                    addSeparator = true;
#endif
#if EZ_BOLT_SUPPORT
                    if(addSeparator) { menu.AddSeparator(""); }
                    menu.AddItem(new GUIContent("Bolt Variables/Scene"), false, () => { SetBoltVarsAsSource(EzBind, bindInfo, 1); });
                    menu.AddItem(new GUIContent("Bolt Variables/Application"), false, () => { SetBoltVarsAsSource(EzBind, bindInfo, 2); });
                    menu.AddItem(new GUIContent("Bolt Variables/Saved"), false, () => { SetBoltVarsAsSource(EzBind, bindInfo, 3); });
#endif
                        menu.ShowAsContext();
                    }
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                }
#endif
#endif
                QUI.FlexibleSpace();
            }
            QUI.EndHorizontal();
            // Draw Component (GO only)
            if(!bindInfo.bindData.source.useScriptableObject && bindInfo.bindData.source.gameObject != null)
            {
                QUI.Space(SPACE_2);
                QUI.BeginHorizontal(width);
                {
                    QUI.Space(LABEL_INDENT);
                    qLabel.text = "Component";
                    qLabel.style = Style.Text.Normal;
                    QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, LABEL_HEIGHT);

                    int tempComponentIndex = bindInfo.GetSourceComponentIndex();
                    QUI.BeginChangeCheck();
                    SetBackgroundRedIfCondition(tempComponentIndex == 0);
                    tempComponentIndex = EditorGUILayout.Popup(tempComponentIndex, bindInfo.GetSourceComponentNamesArray(), GUILayout.Width((width - qLabel.x - LABEL_INDENT) * bindInfo.show.faded));
                    QUI.ResetColors();
                    if(QUI.EndChangeCheck())
                    {
                        Undo.RecordObject(EzBind, "UpdateComponent");
                        bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.GetSourceComponentsArray()[tempComponentIndex], null, null);
                    }
                    QUI.FlexibleSpace();
                }
                QUI.EndHorizontal();
            }

            tempWidth = width;
            // if an usable holder is selected, make room for the button
            if(bindInfo.SourceMayUseHolder())
            {
                tempWidth -= SPACE_16;
            }

            // Draw Holder & Variable
            if((bindInfo.bindData.source.useScriptableObject && bindInfo.bindData.source.scriptableObject != null)
            || (!bindInfo.bindData.source.useScriptableObject && bindInfo.bindData.source.component != null))
            {
                QUI.Space(SPACE_2);
                QUI.BeginHorizontal(tempWidth);
                {
                    QUI.Space(LABEL_INDENT);
                    qLabel.text = "Variable";
                    qLabel.style = Style.Text.Normal;
                    QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, LABEL_HEIGHT);

                    int tempHolderNameIndex = bindInfo.GetSourceHolderNameIndex();
                    QUI.BeginChangeCheck();
                    SetBackgroundRedIfCondition(tempHolderNameIndex == 0);
                    tempHolderNameIndex = EditorGUILayout.Popup(tempHolderNameIndex, bindInfo.GetSourceHolderNamesArray(), GUILayout.Width((tempWidth - qLabel.x - LABEL_INDENT) * bindInfo.show.faded));
                    QUI.ResetColors();
                    if(QUI.EndChangeCheck())
                    {
                        Undo.RecordObject(EzBind, "UpdateVariable");
                        if(bindInfo.bindData.source.useScriptableObject) { bindInfo.UpdateSourceWithSO(bindInfo.bindData.source.scriptableObject, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null); }
                        else { bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.bindData.source.component, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null); }
                    }

                    if(tempWidth != width)
                    {
                        QUI.Space(-2);
                        if(bindInfo.bindData.source.useHolder)
                        {
                            if(QUI.ButtonMinus())
                            {
                                Undo.RecordObject(EzBind, "DoNotUseHolder");
                                bindInfo.bindData.source.useHolder = false;
                                if(bindInfo.bindData.source.useScriptableObject) { bindInfo.UpdateSourceWithSO(bindInfo.bindData.source.scriptableObject, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null); }
                                else { bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.bindData.source.component, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null); }
                            }
                        }
                        else
                        {
                            if(QUI.ButtonPlus())
                            {
                                Undo.RecordObject(EzBind, "UseHolder");
                                bindInfo.bindData.source.useHolder = true;
                                if(bindInfo.bindData.source.useScriptableObject) { bindInfo.UpdateSourceWithSO(bindInfo.bindData.source.scriptableObject, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null); }
                                else { bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.bindData.source.component, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null); }
                            }
                        }
                    }
                    QUI.FlexibleSpace();
                }
                QUI.EndHorizontal();

                // Draw Variable under Holder (if using holder)
                if(bindInfo.SourceMayUseHolder() && bindInfo.bindData.source.useHolder)
                {
                    QUI.Space(SPACE_2);
                    QUI.BeginHorizontal(tempWidth);
                    {
                        //QUI.Space(LABEL_INDENT);
                        //qLabel.text = "Variable";
                        //qLabel.style = Style.Text.Normal;
                        //QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, LABEL_HEIGHT);
                        QUI.Space(qLabel.x + LABEL_INDENT + SPACE_4);

                        int tempVarNameIndex = bindInfo.GetSourceVariableNameIndex();
                        QUI.BeginChangeCheck();
                        SetBackgroundRedIfCondition(tempVarNameIndex == 0);
                        tempVarNameIndex = EditorGUILayout.Popup(tempVarNameIndex, bindInfo.GetSourceVariableNamesArray(), GUILayout.Width((tempWidth - qLabel.x - LABEL_INDENT) * bindInfo.show.faded));
                        QUI.ResetColors();
                        if(QUI.EndChangeCheck())
                        {
                            Undo.RecordObject(EzBind, "UpdateVariable");
                            if(bindInfo.bindData.source.useScriptableObject) { bindInfo.UpdateSourceWithSO(bindInfo.bindData.source.scriptableObject, bindInfo.GetSourceHolderNamesArray()[bindInfo.GetSourceHolderNameIndex()], bindInfo.GetSourceVariableNamesArray()[tempVarNameIndex]); }
                            else { bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.bindData.source.component, bindInfo.GetSourceHolderNamesArray()[bindInfo.GetSourceHolderNameIndex()], bindInfo.GetSourceVariableNamesArray()[tempVarNameIndex]); }
                        }
                        QUI.FlexibleSpace();
                    }
                    QUI.EndHorizontal();
                }
            }
        }

#if EZ_DATA_MANAGER
        public static void SetEzDataManagerAsSource(Component binder, GameObject ezDataManager, BindInfo bindInfo)
        {
            Undo.RecordObject(binder, "SetSource");
            bindInfo.UpdateSourceWithGO(ezDataManager, ezDataManager.GetComponent<DataManager.EzDataManager>(), null, null);
        }

        public static void SetEzDataManagerAsObserver(Component binder, GameObject ezDataManager, GenericMenu menu, BindInfo bindInfo, int index)
        {
            menu.AddItem(new GUIContent("EzDataManager"), false, () =>
            {
                Undo.RecordObject(binder, "SetObserver");
                bindInfo.UpdateObserverWithGO(index, ezDataManager, ezDataManager.GetComponent<DataManager.EzDataManager>(), null, null);
            });
        }
#endif

#if EZ_PLAYMAKER_SUPPORT
        public static void SetPlayMakerGlobalsAsSource(Component binder, BindInfo bindInfo)
        {
            Undo.RecordObject(binder, "SetSource");
            bindInfo.UpdateSourceWithSO(PlayMakerGlobals.Instance, BindInfo.playmakerVarsString, null);
            bindInfo.bindData.source.useHolder = true;
            bindInfo.UpdateSourceWithSO(PlayMakerGlobals.Instance, BindInfo.playmakerVarsString, null);
        }

        public static void SetPlayMakerGlobalsAsObserver(Component binder, GenericMenu menu, BindInfo bindInfo, int index)
        {
            menu.AddItem(new GUIContent("PlayMaker Globals"), false, () =>
            {
                Undo.RecordObject(binder, "SetObserver");
                bindInfo.UpdateObserverWithSO(index, PlayMakerGlobals.Instance, BindInfo.playmakerVarsString, null);
                bindInfo.bindData.observers[index].useHolder = true;
                bindInfo.UpdateObserverWithSO(index, PlayMakerGlobals.Instance, BindInfo.playmakerVarsString, null);
            });
        }
#endif

#if EZ_BOLT_SUPPORT
        /// <summary>
        /// Option: 1 = scene, 2 = application, 3 = saved
        /// </summary>
        /// <param name="option">1 = scene, 2 = application, 3 = saved</param>
        public static void SetBoltVarsAsSource(Component binder, BindInfo bindInfo, int option)
        {
            switch(option)
            {
                case 1:
                    var sceneVars = Bolt.SceneVariables.Instance(binder.gameObject.scene).gameObject;
                    if(sceneVars == null) { Debug.Log("Could not find Bolt Scene Variables!"); return; }
                    Undo.RecordObject(binder, "SetSource");
                    bindInfo.UpdateSourceWithGO(sceneVars, sceneVars.GetComponent<Bolt.Variables>(), BindInfo.boltVarsString, null);
                    bindInfo.bindData.source.useHolder = true;
                    bindInfo.UpdateSourceWithGO(sceneVars, sceneVars.GetComponent<Bolt.Variables>(), BindInfo.boltVarsString, null);
                    return;
                case 2:
                    Undo.RecordObject(binder, "SetSource");
                    bindInfo.UpdateSourceWithSO(Bolt.ApplicationVariables.asset, BindInfo.boltVarsString, null);
                    bindInfo.bindData.source.useHolder = true;
                    bindInfo.UpdateSourceWithSO(Bolt.ApplicationVariables.asset, BindInfo.boltVarsString, null);
                    return;
                case 3:
                    Undo.RecordObject(binder, "SetSource");
                    bindInfo.UpdateSourceWithSO(Bolt.SavedVariables.asset, BindInfo.boltVarsString, null);
                    bindInfo.bindData.source.useHolder = true;
                    bindInfo.UpdateSourceWithSO(Bolt.SavedVariables.asset, BindInfo.boltVarsString, null);
                    return;
            }
        }

        public static void SetBoltVarsAsObsever(Component binder, GenericMenu menu, BindInfo bindInfo, int index)
        {
            menu.AddItem(new GUIContent("Bolt Variables/Scene"), false, () =>
            {
                var sceneVars = Bolt.SceneVariables.Instance(binder.gameObject.scene).gameObject;
                if(sceneVars == null) { Debug.Log("Could not find Bolt Scene Variables!"); return; }
                Undo.RecordObject(binder, "SetObserver");
                bindInfo.UpdateObserverWithGO(index, sceneVars, sceneVars.GetComponent<Bolt.Variables>(), BindInfo.boltVarsString, null);
                bindInfo.bindData.observers[index].useHolder = true;
                bindInfo.UpdateObserverWithGO(index, sceneVars, sceneVars.GetComponent<Bolt.Variables>(), BindInfo.boltVarsString, null);
            });
            menu.AddItem(new GUIContent("Bolt Variables/Application"), false, () =>
            {
                Undo.RecordObject(binder, "SetObserver");
                bindInfo.UpdateObserverWithSO(index, Bolt.ApplicationVariables.asset, BindInfo.boltVarsString, null);
                bindInfo.bindData.observers[index].useHolder = true; ;
                bindInfo.UpdateObserverWithSO(index, Bolt.ApplicationVariables.asset, BindInfo.boltVarsString, null);
            });
            menu.AddItem(new GUIContent("Bolt Variables/Saved"), false, () =>
            {
                Undo.RecordObject(binder, "SetObserver");
                bindInfo.UpdateObserverWithSO(index, Bolt.SavedVariables.asset, BindInfo.boltVarsString, null);
                bindInfo.bindData.observers[index].useHolder = true;
                bindInfo.UpdateObserverWithSO(index, Bolt.SavedVariables.asset, BindInfo.boltVarsString, null);
            });
        }
#endif

        void DrawBindOnValueChanged(BindInfo bindInfo, int index, float width)
        {
            QUI.BeginHorizontal(width);
            {
                QUI.Space(-1);
                QUI.SetGUIBackgroundColor(AccentColorGray);
                qLabel.text = "Listeners - OnValueChanged";
                QUI.PropertyField(bindsData.GetArrayElementAtIndex(index).FindPropertyRelative("OnValueChanged"), true, qLabel.content, width + 9);
                QUI.ResetColors();
            }
            QUI.EndHorizontal();
        }

        void DrawBindObservers(BindInfo bindInfo, float width)
        {
            int observersBackgroundHeight = 16 + bindInfo.GetInspectorObserverBoxHeight();
            QUI.Box(QStyles.GetBackgroundStyle(Style.BackgroundType.Low, QColors.Color.Purple), width, observersBackgroundHeight);
            QUI.Space(-observersBackgroundHeight);

            if(QUI.SlicedButton("Add Observer", QColors.Color.Purple, width))
            {
                Undo.RecordObject(EzBind, "AddObserver");
                bindInfo.AddObserver();
            }

            if(!bindInfo.HasObservers) { return; }

            QUI.Space(SPACE_2);

            for(int i = 0; i < bindInfo.bindData.observers.Count; i++)
            {
                // Draw GO/SO selection
                QUI.BeginHorizontal(width);
                {
                    QUI.Space(LABEL_INDENT);
                    qLabel.text = "Observer " + i;
                    qLabel.style = Style.Text.Normal;
                    QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, LABEL_HEIGHT);

                    Object tempObj;
                    if(bindInfo.bindData.observers[i].useScriptableObject)
                    {
                        tempObj = bindInfo.bindData.observers[i].scriptableObject;
                    }
                    else
                    {
                        tempObj = bindInfo.bindData.observers[i].gameObject;
                    }

                    tempWidth = width - qLabel.x - SPACE_16 - SPACE_16 - LABEL_INDENT - 12;  //this will be the ObjectField width - it is used to add/remove space for the EzDataManager button

#if EZ_DATA_MANAGER || EZ_PLAYMAKER_SUPPORT || EZ_BOLT_SUPPORT
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                    if(ezDataManager != null) //if EzDataManager was found in this scene - adjust the ObjectField width
                    {
#endif
                        tempWidth -= SPACE_16;
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                    }
#endif
#endif
                    tempWidth *= bindInfo.show.faded;
                    QUI.BeginChangeCheck();
                    SetBackgroundRedIfCondition(tempObj == null);
                    tempObj = QUI.ObjectField(tempObj, typeof(Object), true, tempWidth);
                    QUI.ResetColors();
                    if(QUI.EndChangeCheck())
                    {
                        if(tempObj == null)
                        {
                            Undo.RecordObject(EzBind, "UpdateObserver");
                            bindInfo.ResetObserver(i);
                        }
                        else if(typeof(GameObject).IsInstanceOfType(tempObj))
                        {
                            Undo.RecordObject(EzBind, "UpdateObserver");
                            bindInfo.UpdateObserverWithGO(i, (GameObject) tempObj, null, null, null);
                        }
                        else if(typeof(ScriptableObject).IsInstanceOfType(tempObj))
                        {
                            Undo.RecordObject(EzBind, "UpdateObserver");
                            bindInfo.UpdateObserverWithSO(i, (ScriptableObject) tempObj, null, null);
                        }
                        else
                        {
                            QUI.DisplayDialog("Invalid selection", "Only GameObjects and ScriptableObjects are allowed!", "OK");
                        }
                    }
                    if(QUI.ButtonMinus())
                    {
                        Undo.RecordObject(EzBind, "RemoveObserver");
                        bindInfo.RemoveObserverAt(i);
                        QUI.ExitGUI();
                    }
                    if(QUI.ButtonReset())
                    {
                        Undo.RecordObject(EzBind, "ResetObserver");
                        bindInfo.ResetObserver(i);
                    }
#if EZ_DATA_MANAGER || EZ_PLAYMAKER_SUPPORT || EZ_BOLT_SUPPORT
#pragma warning disable 0219
                    bool addSeparator = false;
#pragma warning restore 0219
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                    if(ezDataManager != null) //if EzDataManager was found in this scene - show the extra button that allows setting EzDataManager as the source
                    {
#endif
                        if(QUI.ButtonData())
                        {
                            GenericMenu menu = new GenericMenu();
#if EZ_DATA_MANAGER
                            if(ezDataManager != null) //if EzDataManager was found in this scene - show the extra button that allows setting EzDataManager as the source
                            {
                                SetEzDataManagerAsObserver(EzBind, ezDataManager.gameObject, menu, bindInfo, i);
                                addSeparator = true;
                            }
#endif
#if EZ_PLAYMAKER_SUPPORT
                        if(addSeparator) { menu.AddSeparator(""); }
                        SetPlayMakerGlobalsAsObserver(EzBind, menu, bindInfo, i);
                        addSeparator = true;
#endif
#if EZ_BOLT_SUPPORT
                        if(addSeparator) { menu.AddSeparator(""); }
                        SetBoltVarsAsObsever(EzBind, menu, bindInfo, i);
#endif
                            menu.ShowAsContext();
                        }
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                    }
#endif
#endif
                    QUI.FlexibleSpace();
                }
                QUI.EndHorizontal();
                // Draw Component (GO only)
                if(!bindInfo.bindData.observers[i].useScriptableObject && bindInfo.bindData.observers[i].gameObject != null)
                {
                    QUI.Space(SPACE_2);
                    QUI.BeginHorizontal(width);
                    {
                        QUI.Space(qLabel.x + LABEL_INDENT + SPACE_2);

                        int tempComponentIndex = bindInfo.GetObserverComponentIndex(i);
                        QUI.BeginChangeCheck();
                        SetBackgroundRedIfCondition(tempComponentIndex == 0);
                        tempComponentIndex = EditorGUILayout.Popup(tempComponentIndex, bindInfo.GetObserverComponentNamesArray(i), GUILayout.Width((width - qLabel.x - LABEL_INDENT - 6) * bindInfo.show.faded));
                        QUI.ResetColors();
                        if(QUI.EndChangeCheck())
                        {
                            Undo.RecordObject(EzBind, "UpdateComponent");
                            bindInfo.UpdateObserverWithGO(i, bindInfo.bindData.observers[i].gameObject, bindInfo.GetObserverComponentsArray(i)[tempComponentIndex], null, null);
                        }
                        QUI.FlexibleSpace();
                    }
                    QUI.EndHorizontal();
                    QUI.Space(-5);
                }

                tempWidth = width;
                // if an usable holder is selected, make room for the button
                if(bindInfo.ObserverMayUseHolder(i))
                {
                    tempWidth -= SPACE_16;
                }
                // Draw Holder & Variable
                if((bindInfo.bindData.observers[i].useScriptableObject && bindInfo.bindData.observers[i].scriptableObject != null)
                || (!bindInfo.bindData.observers[i].useScriptableObject && bindInfo.bindData.observers[i].component != null))
                {
                    QUI.Space(SPACE_2);
                    QUI.BeginHorizontal(tempWidth);
                    {
                        QUI.Space(qLabel.x + LABEL_INDENT + SPACE_2);

                        int tempHolderNameIndex = bindInfo.GetObserverHolderNameIndex(i);
                        QUI.BeginChangeCheck();
                        SetBackgroundRedIfCondition(tempHolderNameIndex == 0);
                        tempHolderNameIndex = EditorGUILayout.Popup(tempHolderNameIndex, bindInfo.GetObserverHolderNamesArray(i), GUILayout.Width((tempWidth - qLabel.x - LABEL_INDENT - 6) * bindInfo.show.faded));
                        QUI.ResetColors();
                        if(QUI.EndChangeCheck())
                        {
                            Undo.RecordObject(EzBind, "UpdateVariable");
                            if(bindInfo.bindData.observers[i].useScriptableObject) { bindInfo.UpdateObserverWithSO(i, bindInfo.bindData.observers[i].scriptableObject, bindInfo.GetObserverHolderNamesArray(i)[tempHolderNameIndex], null); }
                            else { bindInfo.UpdateObserverWithGO(i, bindInfo.bindData.observers[i].gameObject, bindInfo.bindData.observers[i].component, bindInfo.GetObserverHolderNamesArray(i)[tempHolderNameIndex], null); }
                        }

                        if(tempWidth != width)
                        {
                            QUI.Space(-2);
                            if(bindInfo.bindData.observers[i].useHolder)
                            {
                                if(QUI.ButtonMinus())
                                {
                                    Undo.RecordObject(EzBind, "DoNotUseHolder");
                                    bindInfo.bindData.observers[i].useHolder = false;
                                    if(bindInfo.bindData.observers[i].useScriptableObject) { bindInfo.UpdateObserverWithSO(i, bindInfo.bindData.observers[i].scriptableObject, bindInfo.GetObserverHolderNamesArray(i)[tempHolderNameIndex], null); }
                                    else { bindInfo.UpdateObserverWithGO(i, bindInfo.bindData.observers[i].gameObject, bindInfo.bindData.observers[i].component, bindInfo.GetObserverHolderNamesArray(i)[tempHolderNameIndex], null); }
                                }
                            }
                            else
                            {
                                if(QUI.ButtonPlus())
                                {
                                    Undo.RecordObject(EzBind, "UseHolder");
                                    bindInfo.bindData.observers[i].useHolder = true;
                                    if(bindInfo.bindData.observers[i].useScriptableObject) { bindInfo.UpdateObserverWithSO(i, bindInfo.bindData.observers[i].scriptableObject, bindInfo.GetObserverHolderNamesArray(i)[tempHolderNameIndex], null); }
                                    else { bindInfo.UpdateObserverWithGO(i, bindInfo.bindData.observers[i].gameObject, bindInfo.bindData.observers[i].component, bindInfo.GetObserverHolderNamesArray(i)[tempHolderNameIndex], null); }
                                }
                            }
                        }

                    }
                    QUI.EndHorizontal();

                    // Draw Variable under Holder (if using holder)
                    if(bindInfo.ObserverMayUseHolder(i) && bindInfo.bindData.observers[i].useHolder)
                    {
                        QUI.Space(-1);
                        QUI.BeginHorizontal(tempWidth);
                        {
                            QUI.Space(qLabel.x + LABEL_INDENT + SPACE_2);

                            int tempVarNameIndex = bindInfo.GetObserverVariableNameIndex(i);
                            QUI.BeginChangeCheck();
                            SetBackgroundRedIfCondition(tempVarNameIndex == 0);
                            tempVarNameIndex = EditorGUILayout.Popup(tempVarNameIndex, bindInfo.GetObserverVariableNamesArray(i), GUILayout.Width((tempWidth - qLabel.x - LABEL_INDENT - 6) * bindInfo.show.faded));
                            QUI.ResetColors();
                            if(QUI.EndChangeCheck())
                            {
                                Undo.RecordObject(EzBind, "UpdateVariable");
                                if(bindInfo.bindData.observers[i].useScriptableObject) { bindInfo.UpdateObserverWithSO(i, bindInfo.bindData.observers[i].scriptableObject, bindInfo.GetObserverHolderNamesArray(i)[bindInfo.GetObserverHolderNameIndex(i)], bindInfo.GetObserverVariableNamesArray(i)[tempVarNameIndex]); }
                                else { bindInfo.UpdateObserverWithGO(i, bindInfo.bindData.observers[i].gameObject, bindInfo.bindData.observers[i].component, bindInfo.GetObserverHolderNamesArray(i)[bindInfo.GetObserverHolderNameIndex(i)], bindInfo.GetObserverVariableNamesArray(i)[tempVarNameIndex]); }
                            }
                            QUI.FlexibleSpace();
                        }
                        QUI.EndHorizontal();
                    }

                    QUI.Space(-SPACE_2);
                }

                QUI.Space((bindInfo.bindData.observers[i].component == null && bindInfo.bindData.observers[i].scriptableObject == null) ? 0 : -SPACE_2);

                QUI.Space(SPACE_2 * bindInfo.show.faded);

                if(i != bindInfo.ObserversCount - 1)
                {
                    QUI.Space(4 * bindInfo.show.faded);
                    QUI.DrawLine(QColors.Color.Purple, width);
                    QUI.Space(1);
                }
            }
        }

        void AddBind()
        {
            Undo.RecordObject(EzBind, "AddBind");
            if(EzBind.bindsData == null)
            {
                EzBind.bindsData = new List<BindData>();
                bindsInfo = new List<BindInfo>();
            }
            var newBindData = new BindData() { bindName = UNNAMED_BIND };
            EzBind.bindsData.Add(newBindData);
            bindsInfo.Add(new BindInfo(newBindData, Repaint) { newBindName = UNNAMED_BIND });
            InitializeVariables();
        }
        void DeleteBind(BindInfo bindInfo)
        {
            if(EzBind.bindsData == null || EzBind.bindsData.Count == 0) { return; }
            Undo.RecordObject(EzBind, "DeleteBind");
            EzBind.bindsData.Remove(bindInfo.bindData);
            bindsInfo.Remove(bindInfo);
            InitializeVariables();
        }

        public static void SetBackgroundRedIfCondition(bool condition)
        {
            if(condition) { QUI.SetGUIBackgroundColor(QUI.IsProSkin ? QColors.Red.Color : QColors.RedLight.Color); }
        }
    }

    #region Classes : BindInfo
    public class BindInfo
    {
        public readonly BindData bindData;
        public string newBindName;
        public AnimBool show, renameBind;

        public string BindName { get { return bindData.bindName; } }

        public const string DEFAULT_SELECTION_NAME = "None";
#if EZ_DATA_MANAGER
        public readonly string[] ignoreTypesAndNames = new string[] { "List`1", "[]", "HideFlags", "runInEditMode", "useGUILayout", "CTGSTRT_" };
#else
        public readonly string[] ignoreTypesAndNames = new string[] { "List`1", "[]", "HideFlags", "runInEditMode", "useGUILayout" };
#endif
#if EZ_PLAYMAKER_SUPPORT
        public const string playmakerVarsString = "FsmVariables Variables";
#endif
#if EZ_BOLT_SUPPORT
        public const string boltVarsString = "VariableDeclarations declarations";
#endif
        public const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static;

        private Component[] sourceComponentsArray = null;
        private string[] sourceComponentNamesArray = null;
        private System.Type[] sourceHolderTypesArray = null;
        private object[] sourceHolderValuesArray = null;
        private string[] sourceHolderNamesArray = null;
        private string[] sourceVariableNamesArray = null;

        private List<Component[]> observerComponentsArrays = new List<Component[]>();
        private List<string[]> observerComponentNamesArrays = new List<string[]>();
        private List<object[]> observerHolderValuesArrays = new List<object[]>();
        private List<System.Type[]> observerHolderTypesArrays = new List<System.Type[]>();
        private List<string[]> observerHolderNamesArrays = new List<string[]>();
        private List<string[]> observerVariableNamesArrays = new List<string[]>();

        private List<Component> auxComponentList;
        private List<System.Type> auxHolderTypesList;
        private List<object> auxHolderValuesList;
        private List<string> auxStringList;

        private Component[] auxComponentArray;
        private string[] auxStringArray;
        private object[] auxHolderValuesArray;
        private System.Type[] auxHolderTypesArray;

        public bool HasSource { get { return BindHasSource(bindData); } }
        public bool HasObservers { get { return ObserversCount != 0; } }
        public int ObserversCount { get { return bindData.observers.IsNullOrEmpty() ? 0 : bindData.observers.Count; } }

        public BindInfo(BindData bindData, UnityAction callback)
        {
            this.bindData = bindData;
            this.newBindName = bindData.bindName;
            show = new AnimBool(false, callback);
            renameBind = new AnimBool(false, callback);

            UpdateSourceArrays();

            // init observers
            if(!bindData.observers.IsNullOrEmpty())
            {
                for(int i = 0; i < bindData.observers.Count; i++) { UpdateObserverArrays(i); }
            }
        }

        public void VersionChangeCheck()
        {
            VersionCheckHolder(bindData.source);
            foreach(var obs in bindData.observers) { VersionCheckHolder(obs); }
        }
        /// <summary>
        /// Version update checking with the introduction of holders...
        /// </summary>
        private void VersionCheckHolder(BoundItem item)
        {
            if(item == null) { return; }
            if(item.holderName.IsNullOrEmpty() && !item.variableName.IsNullOrEmpty()) { item.holderName = item.variableName; }
        }

        public static bool BindHasSource(BindData bindData)
        {
            return bindData != null
                   && bindData.source != null
                   && ((bindData.source.gameObject != null && bindData.source.component != null) || (bindData.source.scriptableObject != null))
                   && !string.IsNullOrEmpty(bindData.source.variableName)
                   && !bindData.source.variableName.Equals(DEFAULT_SELECTION_NAME);
        }

        public void RenameBind()
        {
            bindData.bindName = newBindName.RemoveAllTypesOfWhitespaces();
        }

        #region BindInfoSource
        /// <summary>
        /// Resets the bind source.
        /// </summary>
        public void ResetSource()
        {
            bindData.source.Reset(); ResetSourceComponentArray(); ResetSourceHolderArrays(); ResetSourceVariableArray();
        }

        /// <summary>
        /// Updates the source with a new gameObject. If gameObject == null, resets the source.
        /// </summary>
        public void UpdateSourceWithGO(GameObject gameObject, Component component, string holderName, string variableName)
        {
            if(bindData.source.useScriptableObject) { ResetSource(); bindData.source.useScriptableObject = false; }

            if(gameObject == null) { bindData.source.Reset(); ResetSourceComponentArray(); ResetSourceHolderArrays(); ResetSourceVariableArray(); bindData.source.variableName = DEFAULT_SELECTION_NAME; return; }
            else { if(gameObject != bindData.source.gameObject) { bindData.source.gameObject = gameObject; UpdateSourceComponentsArrays(); bindData.source.variableName = DEFAULT_SELECTION_NAME; } }

            if(component == null) { bindData.source.component = null; bindData.source.useHolder = false; bindData.source.holderName = DEFAULT_SELECTION_NAME; bindData.source.variableName = DEFAULT_SELECTION_NAME; ResetSourceHolderArrays(); ResetSourceVariableArray(); return; }
            else
            {
                if(component != bindData.source.component)
                {
                    bindData.source.component = component;
                    bindData.source.useHolder = false;
                    bindData.source.holderName = DEFAULT_SELECTION_NAME;
                    bindData.source.variableName = DEFAULT_SELECTION_NAME;
                    UpdateSourceHolderArrays(component);
                }
            }

            if(holderName.IsNullOrEmpty()) { bindData.source.useHolder = false; bindData.source.holderName = DEFAULT_SELECTION_NAME; bindData.source.variableName = DEFAULT_SELECTION_NAME; ResetSourceVariableArray(); return; }
            else
            {
                if(holderName != bindData.source.holderName)
                {
                    bindData.source.useHolder = false;
                    bindData.source.holderName = holderName;
                    bindData.source.variableName = holderName;
                    ResetSourceVariableArray();
                }
            }

            if(bindData.source.useHolder)
            {
                if(variableName.IsNullOrEmpty()) { bindData.source.variableName = DEFAULT_SELECTION_NAME; UpdateSourceVariableArray(GetSourceHolderReference()); }
                else if(variableName != bindData.source.variableName) { bindData.source.variableName = variableName; }
            }
            else
            {
                bindData.source.variableName = holderName;
            }
        }

        /// <summary>
        /// Updates the source with a new scriptableObject. If scriptableObject == null, resets the source.
        /// </summary>
        public void UpdateSourceWithSO(ScriptableObject scriptableObject, string holderName, string variableName)
        {
            if(!bindData.source.useScriptableObject) { ResetSource(); bindData.source.useScriptableObject = true; }

            if(scriptableObject == null) { bindData.source.scriptableObject = null; ResetSourceHolderArrays(); ResetSourceVariableArray(); bindData.source.variableName = DEFAULT_SELECTION_NAME; return; }
            else
            {
                if(scriptableObject != bindData.source.scriptableObject)
                {
                    bindData.source.scriptableObject = scriptableObject;
                    bindData.source.useHolder = false;
                    bindData.source.holderName = DEFAULT_SELECTION_NAME;
                    bindData.source.variableName = DEFAULT_SELECTION_NAME;
                    UpdateSourceHolderArrays(scriptableObject);
                }
            }

            if(holderName.IsNullOrEmpty()) { bindData.source.useHolder = false; bindData.source.holderName = DEFAULT_SELECTION_NAME; bindData.source.variableName = DEFAULT_SELECTION_NAME; ResetSourceVariableArray(); return; }
            else
            {
                if(holderName != bindData.source.holderName)
                {
                    bindData.source.useHolder = false;
                    bindData.source.holderName = holderName;
                    bindData.source.variableName = holderName;
                    ResetSourceVariableArray();
                }
            }

            if(bindData.source.useHolder)
            {
                if(variableName.IsNullOrEmpty()) { bindData.source.variableName = DEFAULT_SELECTION_NAME; UpdateSourceVariableArray(GetSourceHolderReference()); }
                else if(variableName != bindData.source.variableName) { bindData.source.variableName = variableName; }
            }
            else
            {
                bindData.source.variableName = holderName;
            }
        }

        public bool SourceMayUseHolder()
        {
#if EZ_PLAYMAKER_SUPPORT
            if(bindData.source.holderName == playmakerVarsString) { return true; }
#endif
#if EZ_BOLT_SUPPORT
            if(bindData.source.holderName == boltVarsString) { return true; }
#endif
            if(sourceHolderNamesArray.IsNullOrEmpty() || bindData.source.holderName.IsNullOrEmpty() || bindData.source.holderName == DEFAULT_SELECTION_NAME) { return false; }
            int index = GetSourceHolderNameIndex();
            if(index >= sourceHolderTypesArray.Length) { return false; }
            if(sourceHolderTypesArray[index] == null) { return false; }
            if(sourceHolderTypesArray[index].IsClass && (sourceHolderTypesArray[index] != typeof(string))) { return true; }
            return false;
        }

        /// <summary>
        /// Returns the index of the source's component within the componentArray[]
        /// </summary>
        public int GetSourceComponentIndex()
        {
            if(sourceComponentsArray.IsNullOrEmpty()) { UpdateSourceArrays(); }
            return GetComponentIndex(bindData.source.component, sourceComponentsArray);
        }

        /// <summary>
        /// Returns the array of available source components 
        /// </summary>
        public Component[] GetSourceComponentsArray()
        {
            if(sourceComponentsArray.IsNullOrEmpty()) { UpdateSourceArrays(); }
            return sourceComponentsArray;
        }

        /// <summary>
        /// Returns the array of available source component names
        /// </summary>
        public string[] GetSourceComponentNamesArray()
        {
            if(sourceComponentNamesArray.IsNullOrEmpty()) { UpdateSourceArrays(); }
            return sourceComponentNamesArray;
        }

        /// <summary>
        /// Returns the array of available source variable names
        /// </summary>
        public string[] GetSourceVariableNamesArray()
        {
            if(sourceVariableNamesArray.IsNullOrEmpty()) { UpdateSourceArrays(); }
            return sourceVariableNamesArray;
        }

        /// <summary>
        /// Returns the array of available source holder names
        /// </summary>
        /// <returns></returns>
        public string[] GetSourceHolderNamesArray()
        {
            if(sourceHolderNamesArray.IsNullOrEmpty()) { UpdateSourceArrays(); }
            return sourceHolderNamesArray;
        }

        public object GetSourceHolderReference()
        {
            if(sourceHolderValuesArray.IsNullOrEmpty()) { return null; }
            return sourceHolderValuesArray[GetSourceHolderNameIndex()];
        }

        /// <summary>
        /// Returns the index of the source's holderName within the sourceHolderNamesArray[]
        /// </summary>
        /// <returns></returns>
        public int GetSourceHolderNameIndex()
        {
            if(sourceHolderNamesArray.IsNullOrEmpty()) { UpdateSourceArrays(); }
            return GetNameIndex(bindData.source.holderName, sourceHolderNamesArray);
        }

        /// <summary>
        /// Returns the index of the source's variableName within the sourceVariableNamesArray[]
        /// </summary>
        public int GetSourceVariableNameIndex()
        {
            if(sourceVariableNamesArray.IsNullOrEmpty()) { UpdateSourceArrays(); }
            return GetNameIndex(bindData.source.variableName, sourceVariableNamesArray);
        }

        public void UpdateSourceArrays()
        {
            UpdateSourceComponentsArrays();
            if(bindData.source.component != null) { UpdateSourceHolderArrays(bindData.source.component); }
            else { UpdateSourceHolderArrays(bindData.source.scriptableObject); }
            UpdateSourceVariableArray(GetSourceHolderReference());
        }

        private void ResetSourceComponentArray()
        {
            sourceComponentsArray = null;
            sourceComponentNamesArray = null;
        }

        private void ResetSourceHolderArrays()
        {
            sourceHolderNamesArray = null;
            sourceHolderTypesArray = null;
            sourceHolderValuesArray = null;
        }

        private void UpdateSourceComponentsArrays()
        {
            UpdateComponentArrays(bindData.source.gameObject, out sourceComponentsArray, out sourceComponentNamesArray);
        }

        private void UpdateSourceHolderArrays(object parent)
        {
            UpdateHolderArrays(parent, out sourceHolderTypesArray, out sourceHolderNamesArray, out sourceHolderValuesArray);
        }

        private void ResetSourceVariableArray()
        {
            sourceVariableNamesArray = null;
        }

        private void UpdateSourceVariableArray(object variableParent)
        {
            sourceVariableNamesArray = UpdateVariableNamesArray(variableParent);
        }
        #endregion //BindInfo Source

        #region BindInfoObserver
        /// <summary>
        /// Returns the number of observers that are working with SO instead of GO. Used for inspector calculations.
        /// </summary>
        public int GetInspectorObserverBoxHeight()
        {
            int height = 0;
            for(int i = 0; i < bindData.observers.Count; i++)
            {
                if(bindData.observers[i].useScriptableObject)
                {
                    if(bindData.observers[i].scriptableObject == null)
                    {
                        height += 25;
                    }
                    else
                    {
                        height += 43;
                    }
                }
                else
                {
                    if(bindData.observers[i].gameObject == null)
                    {
                        height += 25;
                    }
                    else if(bindData.observers[i].component == null)
                    {
                        height += 42;
                    }
                    else
                    {
                        height += 60;
                    }
                }
                if(bindData.observers[i].useHolder) { height += 17; }
            }
            return height;
        }

        /// <summary>
        /// Add a new observer.
        /// </summary>
        public void AddObserver()
        {
            if(bindData == null) { return; }  // shouldn't happen
            if(bindData.observers == null) { bindData.observers = new List<BoundItem>(); }
            bindData.observers.Add(new BoundItem());
            observerComponentsArrays.Add(null);
            observerComponentNamesArrays.Add(null);
            observerHolderNamesArrays.Add(null);
            observerHolderValuesArrays.Add(null);
            observerHolderTypesArrays.Add(null);
            observerVariableNamesArrays.Add(null);
        }

        /// <summary>
        /// Removes the specified observer;
        /// </summary>
        /// <param name="index"></param>
        public void RemoveObserverAt(int index)
        {
            observerComponentsArrays.RemoveAt(index);
            observerComponentNamesArrays.RemoveAt(index);
            observerHolderNamesArrays.RemoveAt(index);
            observerHolderValuesArrays.RemoveAt(index);
            observerHolderTypesArrays.RemoveAt(index);
            observerVariableNamesArrays.RemoveAt(index);
            bindData.observers.RemoveAt(index);
        }

        /// <summary>
        /// Resets the bind observer.
        /// </summary>
        public void ResetObserver(int index)
        {
            bindData.observers[index].Reset(); ResetObserverComponentArray(index); ResetObserverHolderArrays(index); ResetObserverVariableArray(index);
        }

        /// <summary>
        /// Updates the observer with a new gameObject. If gameObject == null, resets the observer.
        /// </summary>
        public void UpdateObserverWithGO(int index, GameObject gameObject, Component component, string holderName, string variableName)
        {
            if(bindData.observers[index].useScriptableObject) { ResetObserver(index); bindData.observers[index].useScriptableObject = false; }

            if(gameObject == null) { bindData.observers[index].Reset(); ResetObserverComponentArray(index); ResetObserverHolderArrays(index); ResetObserverVariableArray(index); bindData.observers[index].variableName = DEFAULT_SELECTION_NAME; return; }
            else { if(gameObject != bindData.observers[index].gameObject) { bindData.observers[index].gameObject = gameObject; UpdateObserverComponentsArrays(index); bindData.observers[index].variableName = DEFAULT_SELECTION_NAME; } }

            if(component == null) { bindData.observers[index].component = null; bindData.observers[index].holderName = DEFAULT_SELECTION_NAME; bindData.observers[index].useHolder = false; bindData.observers[index].variableName = DEFAULT_SELECTION_NAME; ResetObserverHolderArrays(index); ResetObserverVariableArray(index); return; }
            else
            {
                if(component != bindData.observers[index].component)
                {
                    bindData.observers[index].component = component;
                    bindData.observers[index].useHolder = false;
                    bindData.observers[index].holderName = DEFAULT_SELECTION_NAME;
                    bindData.observers[index].variableName = DEFAULT_SELECTION_NAME;
                    UpdateObserverHolderArrays(index, component);
                }
            }

            if(holderName.IsNullOrEmpty()) { bindData.observers[index].useHolder = false; bindData.observers[index].holderName = DEFAULT_SELECTION_NAME; bindData.observers[index].variableName = DEFAULT_SELECTION_NAME; ResetObserverVariableArray(index); return; }
            else
            {
                if(holderName != bindData.observers[index].holderName)
                {
                    bindData.observers[index].useHolder = false;
                    bindData.observers[index].holderName = holderName;
                    bindData.observers[index].variableName = holderName;
                    ResetObserverVariableArray(index);
                }
            }

            if(bindData.observers[index].useHolder)
            {
                if(variableName.IsNullOrEmpty()) { bindData.observers[index].variableName = DEFAULT_SELECTION_NAME; UpdateObserverVariableArray(index, GetObserverHolderReference(index)); }
                else if(variableName != bindData.observers[index].variableName) { bindData.observers[index].variableName = variableName; }
            }
            else
            {
                bindData.observers[index].variableName = holderName;
            }
        }

        /// <summary>
        /// Updates the observer with a new scriptableObject. If scriptableObject == null, resets the observer.
        /// </summary>
        public void UpdateObserverWithSO(int index, ScriptableObject scriptableObject, string holderName, string variableName)
        {
            if(!bindData.observers[index].useScriptableObject) { ResetObserver(index); bindData.observers[index].useScriptableObject = true; }

            if(scriptableObject == null) { bindData.observers[index].scriptableObject = null; ResetObserverHolderArrays(index); ResetObserverVariableArray(index); bindData.observers[index].variableName = DEFAULT_SELECTION_NAME; return; }
            else
            {
                if(scriptableObject != bindData.observers[index].scriptableObject)
                {
                    bindData.observers[index].scriptableObject = scriptableObject;
                    bindData.observers[index].useHolder = false;
                    bindData.observers[index].holderName = DEFAULT_SELECTION_NAME;
                    bindData.observers[index].variableName = DEFAULT_SELECTION_NAME;
                    UpdateObserverHolderArrays(index, scriptableObject);
                }
            }

            if(holderName.IsNullOrEmpty()) { bindData.observers[index].useHolder = false; bindData.observers[index].holderName = DEFAULT_SELECTION_NAME; bindData.observers[index].variableName = DEFAULT_SELECTION_NAME; ResetObserverVariableArray(index); }
            else
            {
                if(holderName != bindData.observers[index].holderName)
                {
                    bindData.observers[index].useHolder = false;
                    bindData.observers[index].holderName = holderName;
                    bindData.observers[index].variableName = holderName;
                    ResetObserverVariableArray(index);
                }
            }

            if(bindData.observers[index].useHolder)
            {
                if(variableName.IsNullOrEmpty()) { bindData.observers[index].variableName = DEFAULT_SELECTION_NAME; UpdateObserverVariableArray(index, GetObserverHolderReference(index)); }
                else if(variableName != bindData.observers[index].variableName) { bindData.observers[index].variableName = variableName; }
            }
            else
            {
                bindData.observers[index].variableName = holderName;
            }
        }

        public bool ObserverMayUseHolder(int index)
        {
#if EZ_PLAYMAKER_SUPPORT
            if(bindData.observers[index].holderName == playmakerVarsString) { return true; }
#endif
#if EZ_BOLT_SUPPORT
            if(bindData.observers[index].holderName == boltVarsString) { return true; }
#endif
            if(observerHolderNamesArrays.IsNullOrEmpty() || bindData.observers[index].holderName.IsNullOrEmpty() || bindData.observers[index].holderName == DEFAULT_SELECTION_NAME) { return false; }
            int hNameIndex = GetObserverHolderNameIndex(index);
            if(hNameIndex >= observerHolderTypesArrays[index].Length) { return false; }
            if((observerHolderTypesArrays[index])[hNameIndex].IsClass && ((observerHolderTypesArrays[index])[hNameIndex] != typeof(string))) { return true; }
            return false;
        }

        /// <summary>
        /// Returns the index of the observer's component within the componentArray[]
        /// </summary>
        public int GetObserverComponentIndex(int index)
        {
            if(index >= observerComponentsArrays.Count) { observerComponentsArrays.Add(null); }
            if(observerComponentsArrays[index].IsNullOrEmpty()) { UpdateObserverArrays(index); }
            return GetComponentIndex(bindData.observers[index].component, observerComponentsArrays[index]);
        }

        /// <summary>
        /// Returns the array of available observer components 
        /// </summary>
        public Component[] GetObserverComponentsArray(int index)
        {
            if(index >= observerComponentsArrays.Count) { observerComponentsArrays.Add(null); }
            if(observerComponentsArrays[index].IsNullOrEmpty()) { UpdateObserverArrays(index); }
            return observerComponentsArrays[index];
        }

        /// <summary>
        /// Returns the array of available observer component names
        /// </summary>
        public string[] GetObserverComponentNamesArray(int index)
        {
            if(index >= observerComponentNamesArrays.Count) { observerComponentNamesArrays.Add(null); }
            if(observerComponentNamesArrays[index].IsNullOrEmpty()) { UpdateObserverArrays(index); }
            return observerComponentNamesArrays[index];
        }

        /// <summary>
        /// Returns the array of available observer variable names
        /// </summary>
        public string[] GetObserverVariableNamesArray(int index)
        {
            if(index >= observerVariableNamesArrays.Count) { observerVariableNamesArrays.Add(null); }
            if(observerVariableNamesArrays[index].IsNullOrEmpty()) { UpdateObserverArrays(index); }
            return observerVariableNamesArrays[index];
        }

        /// <summary>
        /// Returns the array of available observer holder names
        /// </summary>
        public string[] GetObserverHolderNamesArray(int index)
        {
            if(index >= observerHolderNamesArrays.Count) { observerHolderNamesArrays.Add(null); }
            if(observerHolderNamesArrays[index].IsNullOrEmpty()) { UpdateObserverArrays(index); }
            return observerHolderNamesArrays[index];
        }

        /// <summary>
        /// Returns the index of the observer's holderName within the { observerHolderNamesArray[] } 
        /// </summary>
        public int GetObserverHolderNameIndex(int index)
        {
            if(index >= observerHolderNamesArrays.Count) { observerHolderNamesArrays.Add(null); }
            return GetNameIndex(bindData.observers[index].holderName, observerHolderNamesArrays[index]);
        }

        public object GetObserverHolderReference(int index)
        {
            if(index >= observerHolderValuesArrays.Count) { observerHolderValuesArrays.Add(null); }
            if(observerHolderValuesArrays[index].IsNullOrEmpty()) { return null; }
            return (observerHolderValuesArrays[index])[GetObserverHolderNameIndex(index)];
        }

        /// <summary>
        /// Returns the index of the observer's variableName within the variableNamesArray[]
        /// </summary>
        public int GetObserverVariableNameIndex(int index)
        {
            if(index >= observerVariableNamesArrays.Count) { observerVariableNamesArrays.Add(null); }
            if(observerVariableNamesArrays[index].IsNullOrEmpty()) { UpdateObserverArrays(index); }
            return GetNameIndex(bindData.observers[index].variableName, observerVariableNamesArrays[index]);
        }

        public void UpdateObserverArrays(int index)
        {
            UpdateObserverComponentsArrays(index);
            if(bindData.observers[index].component != null) { UpdateObserverHolderArrays(index, bindData.observers[index].component); }
            else { UpdateObserverHolderArrays(index, bindData.observers[index].scriptableObject); }
            UpdateObserverVariableArray(index, GetObserverHolderReference(index));
        }

        private void ResetObserverComponentArray(int index)
        {
            if(index < observerComponentsArrays.Count) { observerComponentsArrays[index] = null; observerComponentNamesArrays[index] = null; }
            else { observerComponentsArrays.Add(null); observerComponentNamesArrays.Add(null); }
        }

        private void UpdateObserverComponentsArrays(int index)
        {
            UpdateComponentArrays(bindData.observers[index].gameObject, out auxComponentArray, out auxStringArray);
            if(index < observerComponentsArrays.Count) { observerComponentsArrays[index] = auxComponentArray; observerComponentNamesArrays[index] = auxStringArray; }
            else { observerComponentsArrays.Add(auxComponentArray); observerComponentNamesArrays.Add(auxStringArray); }
        }

        private void ResetObserverHolderArrays(int index)
        {
            if(index < observerHolderNamesArrays.Count) { observerHolderNamesArrays[index] = null; observerHolderTypesArrays[index] = null; observerHolderValuesArrays[index] = null; }
            else { observerHolderNamesArrays.Add(null); observerHolderTypesArrays.Add(null); observerHolderValuesArrays.Add(null); }
        }

        private void UpdateObserverHolderArrays(int index, object parent)
        {
            UpdateHolderArrays(parent, out auxHolderTypesArray, out auxStringArray, out auxHolderValuesArray);
            if(index < observerHolderNamesArrays.Count) { observerHolderNamesArrays[index] = auxStringArray; observerHolderTypesArrays[index] = auxHolderTypesArray; observerHolderValuesArrays[index] = auxHolderValuesArray; }
            else { observerHolderNamesArrays.Add(auxStringArray); observerHolderTypesArrays.Add(auxHolderTypesArray); observerHolderValuesArrays.Add(auxHolderValuesArray); }
        }

        private void ResetObserverVariableArray(int index)
        {
            if(index < observerVariableNamesArrays.Count) { observerVariableNamesArrays[index] = null; }
            else { observerVariableNamesArrays.Add(null); }
        }

        private void UpdateObserverVariableArray(int index, object variableParent)
        {
            if(index < observerVariableNamesArrays.Count) { observerVariableNamesArrays[index] = UpdateVariableNamesArray(variableParent); }
            else { observerVariableNamesArrays.Add(UpdateVariableNamesArray(variableParent)); }
        }
        #endregion // BindInfo Observer

        private void UpdateComponentArrays(GameObject targetGameObject, out Component[] componentsArray, out string[] componentNamesArray)
        {
            componentsArray = null;
            componentNamesArray = null;
            if(targetGameObject == null) { return; }
            auxComponentList = new List<Component>(targetGameObject.GetComponents<Component>());
            auxStringList = new List<string>() { DEFAULT_SELECTION_NAME };
            for(int i = 0; i < auxComponentList.Count; i++)
            {
                if(auxComponentList[i] == null) { continue; }
                auxStringList.Add(auxComponentList[i].GetType().Name);
            }
            componentNamesArray = auxStringList.ToArray();

            auxComponentList.Insert(0, null);
            componentsArray = auxComponentList.ToArray();
        }

        private void UpdateHolderArrays(object parent, out System.Type[] holderTypesArray, out string[] holderNamesArray, out object[] holderValuesArray)
        {
            holderNamesArray = null;
            holderTypesArray = null;
            holderValuesArray = null;
            if(parent == null) { return; }
            auxHolderTypesList = new List<System.Type>();
            auxStringList = new List<string>();
            auxHolderValuesList = new List<object>();

            auxHolderValuesList.AddRange
                    (
                       parent.GetType().GetFields(flags)
                       .Where(x => !ignoreTypesAndNames.Any(n => x.FieldType.Name.Contains(n)) &&
                                   //!ignoreNames.Any(n => GetTypeName(x.FieldType).Contains(n)) &&
                                   !ignoreTypesAndNames.Any(n => x.Name.Contains(n))) // Don't list methods in the ignoreTypeNames array (so we can exclude Unity specific types, etc.)
                       .Where(x => !x.IsInitOnly)
                       .Select(x => x.GetValue(parent))
                       .ToArray()
                    );
            auxHolderValuesList.AddRange
                   (
                      parent.GetType().GetProperties(flags)
                      .Where(x => x.CanRead && x.CanWrite)
                      .Where(x => !ignoreTypesAndNames.Any(n => x.PropertyType.Name.Contains(n)) &&
                              //!ignoreNames.Any(n => GetTypeName(x.PropertyType).Contains(n)) &&
                              !ignoreTypesAndNames.Any(n => x.Name.Contains(n)))
                      .Select(x => x.GetValue(parent, null))
                      .ToArray()
                   );

            auxHolderTypesList.AddRange
                    (
                       parent.GetType().GetFields(flags)
                       .Where(x => !ignoreTypesAndNames.Any(n => x.FieldType.Name.Contains(n)) &&
                                   //!ignoreNames.Any(n => GetTypeName(x.FieldType).Contains(n)) &&
                                   !ignoreTypesAndNames.Any(n => x.Name.Contains(n))) // Don't list methods in the ignoreTypeNames array (so we can exclude Unity specific types, etc.)
                       .Where(x => !x.IsInitOnly)
                       .Select(x => x.FieldType)
                       .ToArray()
                    );
            auxHolderTypesList.AddRange
                   (
                      parent.GetType().GetProperties(flags)
                      .Where(x => x.CanRead && x.CanWrite)
                      .Where(x => !ignoreTypesAndNames.Any(n => x.PropertyType.Name.Contains(n)) &&
                              //!ignoreNames.Any(n => GetTypeName(x.PropertyType).Contains(n)) &&
                              !ignoreTypesAndNames.Any(n => x.Name.Contains(n)))
                      .Select(x => x.PropertyType)
                      .ToArray()
                   );

            auxStringList.AddRange
                    (
                       parent.GetType().GetFields(flags)
                       .Where(x => !ignoreTypesAndNames.Any(n => x.FieldType.Name.Contains(n)) &&
                                   //!ignoreNames.Any(n => GetTypeName(x.FieldType).Contains(n)) &&
                                   !ignoreTypesAndNames.Any(n => x.Name.Contains(n))) // Don't list methods in the ignoreTypeNames array (so we can exclude Unity specific types, etc.)
                       .Where(x => !x.IsInitOnly)
                       .Select(x => GetTypeName(x.FieldType) + " " + x.Name)
                       .ToArray()
                    );
            auxStringList.AddRange
               (
                  parent.GetType().GetProperties(flags)
                  .Where(x => x.CanRead && x.CanWrite)
                  .Where(x => !ignoreTypesAndNames.Any(n => x.PropertyType.Name.Contains(n)) &&
                              //!ignoreNames.Any(n => GetTypeName(x.PropertyType).Contains(n)) &&
                              !ignoreTypesAndNames.Any(n => x.Name.Contains(n)))
                  .Select(x => GetTypeName(x.PropertyType) + " " + x.Name)
                  .ToArray()
               );

#if EZ_PLAYMAKER_SUPPORT
            if(parent.GetType() == typeof(PlayMakerFSM))
            {
                auxHolderValuesList.Insert(0, ((PlayMakerFSM) parent).FsmVariables);
                auxHolderTypesList.Insert(0, typeof(HutongGames.PlayMaker.FsmVariables));
                auxStringList.Insert(0, playmakerVarsString);
            }
#endif

            CustomQSort(auxStringList, auxHolderTypesList, auxHolderValuesList, 0, auxStringList.Count - 1);

            auxHolderValuesList.Insert(0, null);
            auxHolderTypesList.Insert(0, null);
            auxStringList.Insert(0, DEFAULT_SELECTION_NAME);

            holderValuesArray = auxHolderValuesList.ToArray();
            holderNamesArray = auxStringList.ToArray();
            holderTypesArray = auxHolderTypesList.ToArray();
        }

        private static void CustomQSort(List<string> names, List<System.Type> types, List<object> values, int start, int end)
        {
            if(start >= end) { return; }
            int i = start, j = end;
            string pivot = names[(start + end) / 2]; // pivot in the middle...
            while(i <= j)
            {
                while(names[i].CompareTo(pivot) < 0)
                {
                    i++;
                }

                while(names[j].CompareTo(pivot) > 0)
                {
                    j--;
                }

                if(i <= j)
                {
                    // Swap...
                    string tmpStr = names[i];
                    names[i] = names[j];
                    names[j] = tmpStr;

                    System.Type tmpT = types[i];
                    types[i] = types[j];
                    types[j] = tmpT;

                    object tmpVal = values[i];
                    values[i] = values[j];
                    values[j] = tmpVal;

                    i++;
                    j--;
                }
            }
            if(start < j)
            {
                CustomQSort(names, types, values, start, j);
            }

            if(i < end)
            {
                CustomQSort(names, types, values, i, end);
            }
        }

        private string[] UpdateVariableNamesArray(object target)
        {
            if(target == null || target.ToString().Equals("null")) { return new string[] { DEFAULT_SELECTION_NAME }; }

            auxStringList = new List<string>();

#if EZ_PLAYMAKER_SUPPORT
            if(target.GetType() == typeof(HutongGames.PlayMaker.FsmVariables))
            {
                // Get PlayMaker Globals
                HutongGames.PlayMaker.FsmVariables fsmVars = (HutongGames.PlayMaker.FsmVariables) target;

                auxStringList.AddRange
                    (
                        fsmVars.GetAllNamedVariables()
                        .Select(x => "Fsm" + x.VariableType.ToString() + " " + x.Name)
                    );

                auxStringList.Sort();
                auxStringList.Insert(0, DEFAULT_SELECTION_NAME);
                return auxStringList.ToArray();
            }
#endif
#if EZ_BOLT_SUPPORT
            if(target.GetType() == typeof(Bolt.VariableDeclarations))
            {
                Bolt.VariableDeclarations boltVars = (Bolt.VariableDeclarations) target;

                auxStringList.AddRange
                    (
                        boltVars.AsEnumerable()
                        .Select(x => "BoltVariable " + x.name)
                    );

                auxStringList.Sort();
                auxStringList.Insert(0, DEFAULT_SELECTION_NAME);
                return auxStringList.ToArray();
            }
#endif
            // get the regular fields/properties
            auxStringList.AddRange
                (
                   target.GetType().GetFields(flags)
                   .Where(x => !ignoreTypesAndNames.Any(n => x.FieldType.Name.Contains(n)) &&
                               //!ignoreNames.Any(n => GetTypeName(x.FieldType).Contains(n)) &&
                               !ignoreTypesAndNames.Any(n => x.Name.Contains(n))) // Don't list methods in the ignoreTypeNames array (so we can exclude Unity specific types, etc.)
                   .Where(x => !x.IsInitOnly)
                   .Select(x => GetTypeName(x.FieldType) + " " + x.Name)
                   .ToArray()
                );
            auxStringList.AddRange
               (
                  target.GetType().GetProperties(flags)
                  .Where(x => x.CanRead && x.CanWrite)
                  .Where(x => !ignoreTypesAndNames.Any(n => x.PropertyType.Name.Contains(n)) &&
                              //!ignoreNames.Any(n => GetTypeName(x.PropertyType).Contains(n)) &&
                              !ignoreTypesAndNames.Any(n => x.Name.Contains(n)))
                  .Select(x => GetTypeName(x.PropertyType) + " " + x.Name)
                  .ToArray()
               );

            auxStringList.Sort();

            auxStringList.Insert(0, DEFAULT_SELECTION_NAME);
            return auxStringList.ToArray();
        }

        public static string GetTypeName(System.Type t)
        {
            if(t == null) { return "null"; }
            if(t == typeof(int))
            {
                return "int";
            }
            else if(t == typeof(float))
            {
                return "float";
            }
            else if(t == typeof(string))
            {
                return "string";
            }
            else if(t == typeof(bool))
            {
                return "bool";
            }
            else
            {
                return t.IsSpecialName ? t.FullName : t.Name;
            }
        }

        private int GetComponentIndex(Component component, Component[] componentsArray)
        {
            if(component == null || componentsArray.IsNullOrEmpty()) { return 0; }
            for(int i = 0; i < componentsArray.Length; i++)
            {
                if(component == componentsArray[i]) { return i; }
            }
            return 0;
        }

        private int GetNameIndex(string name, string[] nameArray)
        {
            if(name.IsNullOrEmpty() || nameArray.IsNullOrEmpty()) { return 0; }
            for(int i = 0; i < nameArray.Length; i++) { if(name.Equals(nameArray[i])) { return i; } }
            return 0;
        }
    }
    #endregion
}
