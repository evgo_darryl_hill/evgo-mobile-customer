﻿// Copyright (c) 2016 - 2018 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using System.Collections.Generic;
using Ez.Binding.Internal;
using QuickEditor;
using QuickEngine.Extensions;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace Ez.Binding
{
    [CustomEditor(typeof(EzBindExtension))]
    [DisallowMultipleComponent]
    public class EzBindExtensionEditor : QEditor
    {
#if EZ_DATA_MANAGER
        DataManager.EzDataManager ezDataManager;
#endif

        EzBindExtension ezBindExtension { get { return (EzBindExtension) target; } }

        SerializedProperty bindsData;

        Color AccentColorGray { get { return QUI.IsProSkin ? QColors.UnityLight.Color : QColors.UnityLight.Color; } }
        QLabel qLabel;
        GUIStyle NormalTextStyle, SmallTextStyle;
        float tempWidth;

        public List<BindInfo> bindsInfo = new List<BindInfo>();
        private bool initialized = false;

        protected override void InitializeVariables()
        {
            base.InitializeVariables();

            if(ezBindExtension.bindsData == null) { ezBindExtension.bindsData = new List<BindData>(); }
            if(bindsInfo == null) { bindsInfo = new List<BindInfo>(); }
            if(!initialized || ezBindExtension.bindsData.Count != bindsInfo.Count)
            {
                bindsInfo = new List<BindInfo>();
                for(int i = 0; i < ezBindExtension.bindsData.Count; i++)
                {
                    bindsInfo.Add(new BindInfo(ezBindExtension.bindsData[i], Repaint));
                    bindsInfo[i].VersionChangeCheck();
                }
                initialized = true;
            }

            FindEzDataManager();
        }

        protected override void SerializedObjectFindProperties()
        {
            base.SerializedObjectFindProperties();

            bindsData = serializedObject.FindProperty("bindsData");
        }

        protected override void GenerateInfoMessages()
        {
            base.GenerateInfoMessages();

            infoMessage.Add("NoBinds", new InfoMessage() { title = "No binds have been found...", message = "", show = new AnimBool(false, Repaint), type = InfoMessageType.Info });
            infoMessage.Add("UnnamedBind", new InfoMessage() { title = "This bind is disabled...", message = "You need to rename this bind's name in order for it to work.", show = new AnimBool(true, Repaint), type = InfoMessageType.Error });
            infoMessage.Add("SelectEzBind", new InfoMessage() { title = "Select the EzBind gameObject to see all the Binds and their values", message = "", show = new AnimBool(true, Repaint), type = InfoMessageType.Info });
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            requiresContantRepaint = true;
            QUI.MarkSceneDirty(true);
        }

        public override void OnInspectorGUI()
        {
            qLabel = new QLabel();
            NormalTextStyle = QStyles.GetTextStyle(Style.Text.Normal);
            SmallTextStyle = QStyles.GetTextStyle(Style.Text.Small);
            NormalTextStyle.wordWrap = true;
            SmallTextStyle.wordWrap = true;

            DrawHeader(EZResources.editorHeaderEzBindExtension.texture, WIDTH_420, HEIGHT_42);

            if(EditorApplication.isPlaying)
            {
                DrawRuntimeEditor(WIDTH_420);
                QUI.Space(SPACE_4);
                return;
            }
            serializedObject.Update();
            DrawBinds(WIDTH_420);
            serializedObject.ApplyModifiedProperties();
            QUI.Space(SPACE_4);

            NormalTextStyle.wordWrap = false;
            SmallTextStyle.wordWrap = false;
        }

        void FindEzDataManager()
        {
#if EZ_DATA_MANAGER
            ezDataManager = FindObjectOfType<DataManager.EzDataManager>();
#endif
        }

        void DrawRuntimeEditor(float width)
        {
            if(QUI.SlicedButton("Select EzBind", QColors.Color.Green, width, 20))
            {
                Selection.activeGameObject = FindObjectOfType<EzBind>().gameObject;
            }
            QUI.Space(SPACE_4);
            DrawInfoMessage("SelectEzBind", width);
        }

        void DrawBinds(float width)
        {
            if(QUI.SlicedButton("Add Bind", QColors.Color.Blue, width, 20)) { AddBind(); }
            QUI.Space(SPACE_4);

            if(bindsInfo.Count != ezBindExtension.bindsData.Count) { InitializeVariables(); }

            infoMessage["NoBinds"].show.target = bindsInfo == null || bindsInfo.Count == 0;
            DrawInfoMessage("NoBinds", width);
            if(infoMessage["NoBinds"].show.target) { return; }

            for(int i = 0; i < bindsInfo.Count; i++)
            {
                DrawBind(bindsInfo[i], i, width);
                QUI.Space(SPACE_8);
            }
        }

        void DrawBind(BindInfo bindInfo, int index, float width)
        {
            qLabel.text = bindInfo.BindName; //bind name
            QUI.BeginHorizontal(width, 16);
            {
                if(QUI.GhostBar(qLabel.text, (bindInfo.BindName.IsNullOrEmpty() || bindInfo.BindName.Equals(EzBindEditor.UNNAMED_BIND) ? QColors.Color.Red : QColors.Color.Gray), bindInfo.show, width - SPACE_16 - (EzBindEditor.RENAME_BIND_BUTTON_WIDTH * bindInfo.show.faded), 20))
                {
                    bindInfo.show.target = !bindInfo.show.target;
                    if(!bindInfo.show.target)
                    {
                        bindInfo.renameBind.target = false;
                    }
                    else
                    {
                        if(bindInfo.BindName.IsNullOrEmpty() || bindInfo.BindName.Equals(EzBindEditor.UNNAMED_BIND))
                        {
                            bindInfo.newBindName = bindInfo.BindName;
                            bindInfo.renameBind.target = true;
                        }
                    }
                }
                if(bindInfo.show.faded > 0.6f)
                {
                    if(QUI.GhostButton("Rename", QColors.Color.Gray, EzBindEditor.RENAME_BIND_BUTTON_WIDTH * bindInfo.show.faded, 20, true))
                    {
                        bindInfo.newBindName = bindInfo.BindName;
                        if(bindInfo.BindName.IsNullOrEmpty() || bindInfo.BindName.Equals(EzBindEditor.UNNAMED_BIND))
                        {
                            bindInfo.renameBind.target = true;
                            return;
                        }
                        bindInfo.renameBind.target = !bindInfo.renameBind.target;
                    }
                }
                QUI.FlexibleSpace();
                QUI.BeginVertical(16, 20);
                {
                    QUI.Space(SPACE_2);
                    if(QUI.ButtonCancel())
                    {
                        if(QUI.DisplayDialog("Delete Bind", "Are you sure you want to delete this bind (" + bindInfo.BindName + ")?", "Yes", "No"))
                        {
                            DeleteBind(bindInfo);
                        }
                    }
                }
                QUI.EndVertical();
            }
            QUI.EndHorizontal();
            QUI.Space(-21);
            QUI.BeginHorizontal(width);  //bind info (shown on the GhostBar)
            {
                QUI.FlexibleSpace();

                qLabel.text = (bindInfo.HasSource ? "" : "No Source  |");
                qLabel.style = Style.Text.Small;
                QUI.Label(qLabel.text, Style.Text.Small, qLabel.x, 20);

                QUI.Space(SPACE_4);

                qLabel.text = bindInfo.ObserversCount + " Observers";
                qLabel.style = Style.Text.Small;
                QUI.Label(qLabel.text, Style.Text.Small, qLabel.x, 20);

                QUI.Space(24 + (EzBindEditor.RENAME_BIND_BUTTON_WIDTH * bindInfo.show.faded));
            }
            QUI.EndHorizontal();

            QUI.BeginHorizontal(width);
            {
                QUI.Space(SPACE_8);
                if(QUI.BeginFadeGroup(bindInfo.show.faded))
                {
                    QUI.BeginVertical(width - SPACE_8);
                    {
                        DrawBindInfo(bindInfo, index, width - SPACE_8);
                    }
                    QUI.EndVertical();
                }
                QUI.EndFadeGroup();
            }
            QUI.EndHorizontal();
        }

        void DrawBindInfo(BindInfo bindInfo, int index, float width)
        {
            QUI.Space(SPACE_8 * bindInfo.show.faded);

            if(QUI.BeginFadeGroup(bindInfo.renameBind.faded))
            {
                QUI.BeginVertical(width);
                {
                    QUI.Box(QStyles.GetBackgroundStyle(Style.BackgroundType.Low, QColors.Color.Gray), width, 20);
                    QUI.Space(-20);

                    QUI.BeginHorizontal(width);
                    {
                        QUI.Space(EzBindEditor.LABEL_INDENT);
                        qLabel.text = "Rename bind to";
                        qLabel.style = Style.Text.Normal;
                        GUI.SetNextControlName("renameBind" + index);
                        QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, 20);
                        bindInfo.newBindName = QUI.TextField(bindInfo.newBindName, AccentColorGray, width - SPACE_4 - qLabel.x - 32 - 12);

                        if(QUI.ButtonCancel() ||
                            Event.current.isKey &&
                            Event.current.keyCode == KeyCode.Escape &&
                            Event.current.type == EventType.KeyUp)
                        {
                            bindInfo.newBindName = bindInfo.BindName;
                            bindInfo.renameBind.target = false;
                            QUI.ExitGUI();
                        }

                        if(QUI.ButtonOk() ||
                            Event.current.isKey &&
                            Event.current.keyCode == KeyCode.Return &&
                            Event.current.type == EventType.KeyUp &&
                            GUI.GetNameOfFocusedControl() == "renameBind" + index)
                        {
                            bindInfo.newBindName = bindInfo.newBindName.RemoveAllTypesOfWhitespaces();
                            if(bindInfo.newBindName.IsNullOrEmpty())
                            {
                                QUI.DisplayDialog("Rename Bind", "You cannot set an empty bind name.", "Ok");
                                QUI.ExitGUI();
                                return;
                            }
                            if(bindInfo.newBindName.Equals(EzBindEditor.UNNAMED_BIND))
                            {
                                QUI.DisplayDialog("Rename Bind", "You cannot set the '" + EzBindEditor.UNNAMED_BIND + "' bind name.", "Ok");
                                QUI.ExitGUI();
                                return;
                            }
                            Undo.RecordObject(ezBindExtension, "RenameBind");
                            bindInfo.renameBind.target = false;
                            bindInfo.RenameBind();
                            QUI.ExitGUI();
                        }
                        QUI.FlexibleSpace();
                    }
                    QUI.EndHorizontal();
                    QUI.Space(6 * bindInfo.renameBind.faded);
                }
                QUI.EndVertical();
            }
            QUI.EndFadeGroup();

            if(bindInfo.BindName.IsNullOrEmpty() || bindInfo.BindName.Equals(EzBindEditor.UNNAMED_BIND))
            {
                QUI.Space(SPACE_2 * bindInfo.show.faded);
                DrawInfoMessage("UnnamedBind", width);
                QUI.Space(SPACE_8 * bindInfo.show.faded);
                return;
            }

            DrawBindSource(bindInfo, width - SPACE_8);
            QUI.Space(SPACE_8 * bindInfo.show.faded);
            DrawBindOnValueChanged(bindInfo, index, width - SPACE_8);
            QUI.Space(SPACE_4 * bindInfo.show.faded);
            DrawBindObservers(bindInfo, width);
            QUI.Space(SPACE_16 * bindInfo.show.faded);
        }

        void DrawBindSource(BindInfo bindInfo, float width)
        {
            int boxHeight;
            if(bindInfo.bindData.source.useScriptableObject)
            {
                if(bindInfo.bindData.source.scriptableObject == null) { boxHeight = 24; }
                else if(!bindInfo.bindData.source.useHolder) { boxHeight = 42; }
                else { boxHeight = 60; }
            }
            else
            {
                if(bindInfo.bindData.source.gameObject == null) { boxHeight = 24; }
                else if(bindInfo.bindData.source.component == null) { boxHeight = 42; }
                else if(!bindInfo.bindData.source.useHolder) { boxHeight = 60; }
                else { boxHeight = 78; }
            }
            QUI.Box(QStyles.GetBackgroundStyle(Style.BackgroundType.Low, QColors.Color.Orange), width + SPACE_8, boxHeight);
            QUI.Space(-boxHeight);
            QUI.Space(SPACE_2);

            QUI.BeginHorizontal(width);
            {
                QUI.Space(EzBindEditor.LABEL_INDENT);
                qLabel.text = "Source";
                qLabel.style = Style.Text.Normal;
                QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, EzBindEditor.LABEL_HEIGHT);

                Object tempObj;
                if(bindInfo.bindData.source.useScriptableObject) { tempObj = bindInfo.bindData.source.scriptableObject; }
                else { tempObj = bindInfo.bindData.source.gameObject; }

                tempWidth = width - qLabel.x - SPACE_16 - EzBindEditor.LABEL_INDENT - SPACE_4; //this will be the ObjectField width - it is used to add/remove space for the EzDataManager button
#if EZ_DATA_MANAGER || EZ_PLAYMAKER_SUPPORT || EZ_BOLT_SUPPORT
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                if(ezDataManager != null) //if EzDataManager was found in this scene - adjust the ObjectField width
                {
#endif
                    tempWidth -= SPACE_16;
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                }
#endif
#endif
                tempWidth *= bindInfo.show.faded;
                QUI.BeginChangeCheck();
                EzBindEditor.SetBackgroundRedIfCondition(tempObj == null);
                tempObj = QUI.ObjectField(tempObj, typeof(Object), true, tempWidth);
                QUI.ResetColors();
                // Draw GO/SO selection
                if(QUI.EndChangeCheck())
                {
                    if(tempObj == null)
                    {
                        Undo.RecordObject(ezBindExtension, "UpdateSource");
                        bindInfo.ResetSource();
                    }
                    else if(typeof(GameObject).IsInstanceOfType(tempObj))
                    {
                        Undo.RecordObject(ezBindExtension, "UpdateSource");
                        bindInfo.UpdateSourceWithGO((GameObject) tempObj, null, null, null);
                    }
                    else if(typeof(ScriptableObject).IsInstanceOfType(tempObj))
                    {
                        Undo.RecordObject(ezBindExtension, "UpdateSource");
                        bindInfo.UpdateSourceWithSO((ScriptableObject) tempObj, null, null);
                    }
                    else
                    {
                        QUI.DisplayDialog("Invalid selection", "Only GameObjects and ScriptableObjects are allowed!", "OK");
                    }
                }
                if(QUI.ButtonReset())
                {
                    Undo.RecordObject(ezBindExtension, "ResetSource");
                    bindInfo.ResetSource();
                }
#if EZ_DATA_MANAGER || EZ_PLAYMAKER_SUPPORT || EZ_BOLT_SUPPORT
#pragma warning disable 0219
                bool addSeparator = false;
#pragma warning restore 0219
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                if(ezDataManager != null) //if EzDataManager was found in this scene - show the extra button that allows setting EzDataManager as the source
                {
#endif
                    if(QUI.ButtonData())
                    {
                        GenericMenu menu = new GenericMenu();
#if EZ_DATA_MANAGER
                        if(ezDataManager != null) //if EzDataManager was found in this scene - show the extra button that allows setting EzDataManager as the source
                        {
                            menu.AddItem(new GUIContent("EzDataManager"), false, () => { EzBindEditor.SetEzDataManagerAsSource(ezBindExtension, ezDataManager.gameObject, bindInfo); });
                            addSeparator = true;
                        }
#endif
#if EZ_PLAYMAKER_SUPPORT
                    if(addSeparator) { menu.AddSeparator(""); }
                    menu.AddItem(new GUIContent("PlayMaker Globals"), false, () => { EzBindEditor.SetPlayMakerGlobalsAsSource(ezBindExtension, bindInfo); });
                    addSeparator = true;
#endif
#if EZ_BOLT_SUPPORT
                    if(addSeparator) { menu.AddSeparator(""); }
                    menu.AddItem(new GUIContent("Bolt Variables/Scene"), false, () => { EzBindEditor.SetBoltVarsAsSource(ezBindExtension, bindInfo, 1); });
                    menu.AddItem(new GUIContent("Bolt Variables/Application"), false, () => { EzBindEditor.SetBoltVarsAsSource(ezBindExtension, bindInfo, 2); });
                    menu.AddItem(new GUIContent("Bolt Variables/Saved"), false, () => { EzBindEditor.SetBoltVarsAsSource(ezBindExtension, bindInfo, 3); });
#endif
                        menu.ShowAsContext();
                    }
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                }
#endif
#endif
                QUI.FlexibleSpace();
            }
            QUI.EndHorizontal();
            // Draw Component (GO only)
            if(!bindInfo.bindData.source.useScriptableObject && bindInfo.bindData.source.gameObject != null)
            {
                QUI.Space(SPACE_2);
                QUI.BeginHorizontal(width);
                {
                    QUI.Space(EzBindEditor.LABEL_INDENT);
                    qLabel.text = "Component";
                    qLabel.style = Style.Text.Normal;
                    QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, EzBindEditor.LABEL_HEIGHT);

                    int tempComponentIndex = bindInfo.GetSourceComponentIndex();
                    QUI.BeginChangeCheck();
                    EzBindEditor.SetBackgroundRedIfCondition(tempComponentIndex == 0);
                    tempComponentIndex = EditorGUILayout.Popup(tempComponentIndex, bindInfo.GetSourceComponentNamesArray(), GUILayout.Width((width - qLabel.x - EzBindEditor.LABEL_INDENT) * bindInfo.show.faded));
                    QUI.ResetColors();
                    if(QUI.EndChangeCheck())
                    {
                        Undo.RecordObject(ezBindExtension, "UpdateComponent");
                        bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.GetSourceComponentsArray()[tempComponentIndex], null, null);
                    }
                    QUI.FlexibleSpace();
                }
                QUI.EndHorizontal();
            }

            tempWidth = width;
            // if an usable holder is selected, make room for the button
            if(bindInfo.SourceMayUseHolder())
            {
                tempWidth -= SPACE_16;
            }

            // Draw Holder & Variable
            if((bindInfo.bindData.source.useScriptableObject && bindInfo.bindData.source.scriptableObject != null)
            || (!bindInfo.bindData.source.useScriptableObject && bindInfo.bindData.source.component != null))
            {
                QUI.Space(SPACE_2);
                QUI.BeginHorizontal(tempWidth);
                {
                    QUI.Space(EzBindEditor.LABEL_INDENT);
                    qLabel.text = "Variable";
                    qLabel.style = Style.Text.Normal;
                    QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, EzBindEditor.LABEL_HEIGHT);

                    int tempHolderNameIndex = bindInfo.GetSourceHolderNameIndex();
                    QUI.BeginChangeCheck();
                    EzBindEditor.SetBackgroundRedIfCondition(tempHolderNameIndex == 0);
                    tempHolderNameIndex = EditorGUILayout.Popup(tempHolderNameIndex, bindInfo.GetSourceHolderNamesArray(), GUILayout.Width((tempWidth - qLabel.x - EzBindEditor.LABEL_INDENT) * bindInfo.show.faded));
                    QUI.ResetColors();
                    if(QUI.EndChangeCheck())
                    {
                        Undo.RecordObject(ezBindExtension, "UpdateVariable");
                        if(bindInfo.bindData.source.useScriptableObject) { bindInfo.UpdateSourceWithSO(bindInfo.bindData.source.scriptableObject, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null); }
                        else { bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.bindData.source.component, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null); }
                    }

                    if(tempWidth != width)
                    {
                        QUI.Space(-2);
                        if(bindInfo.bindData.source.useHolder)
                        {
                            if(QUI.ButtonMinus())
                            {
                                Undo.RecordObject(ezBindExtension, "DoNotUseHolder");
                                bindInfo.bindData.source.useHolder = false;
                                if(bindInfo.bindData.source.useScriptableObject) { bindInfo.UpdateSourceWithSO(bindInfo.bindData.source.scriptableObject, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null); }
                                else { bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.bindData.source.component, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null); }
                            }
                        }
                        else
                        {
                            if(QUI.ButtonPlus())
                            {
                                Undo.RecordObject(ezBindExtension, "UseHolder");
                                bindInfo.bindData.source.useHolder = true;
                                if(bindInfo.bindData.source.useScriptableObject) { bindInfo.UpdateSourceWithSO(bindInfo.bindData.source.scriptableObject, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null); }
                                else { bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.bindData.source.component, bindInfo.GetSourceHolderNamesArray()[tempHolderNameIndex], null); }
                            }
                        }
                    }
                    QUI.FlexibleSpace();
                }
                QUI.EndHorizontal();

                // Draw Variable under Holder (if using holder)
                if(bindInfo.SourceMayUseHolder() && bindInfo.bindData.source.useHolder)
                {
                    QUI.Space(SPACE_2);
                    QUI.BeginHorizontal(tempWidth);
                    {
                        //QUI.Space(EzBindEditor.LABEL_INDENT);
                        //qLabel.text = "Variable";
                        //qLabel.style = Style.Text.Normal;
                        //QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, EzBindEditor.LABEL_HEIGHT);
                        QUI.Space(qLabel.x + EzBindEditor.LABEL_INDENT + SPACE_4);

                        int tempVarNameIndex = bindInfo.GetSourceVariableNameIndex();
                        QUI.BeginChangeCheck();
                        EzBindEditor.SetBackgroundRedIfCondition(tempVarNameIndex == 0);
                        tempVarNameIndex = EditorGUILayout.Popup(tempVarNameIndex, bindInfo.GetSourceVariableNamesArray(), GUILayout.Width((tempWidth - qLabel.x - EzBindEditor.LABEL_INDENT) * bindInfo.show.faded));
                        QUI.ResetColors();
                        if(QUI.EndChangeCheck())
                        {
                            Undo.RecordObject(ezBindExtension, "UpdateVariable");
                            if(bindInfo.bindData.source.useScriptableObject) { bindInfo.UpdateSourceWithSO(bindInfo.bindData.source.scriptableObject, bindInfo.GetSourceHolderNamesArray()[bindInfo.GetSourceHolderNameIndex()], bindInfo.GetSourceVariableNamesArray()[tempVarNameIndex]); }
                            else { bindInfo.UpdateSourceWithGO(bindInfo.bindData.source.gameObject, bindInfo.bindData.source.component, bindInfo.GetSourceHolderNamesArray()[bindInfo.GetSourceHolderNameIndex()], bindInfo.GetSourceVariableNamesArray()[tempVarNameIndex]); }
                        }
                        QUI.FlexibleSpace();
                    }
                    QUI.EndHorizontal();
                }
            }
        }

        void DrawBindOnValueChanged(BindInfo bindInfo, int index, float width)
        {
            QUI.BeginHorizontal(width);
            {
                QUI.Space(-1);
                QUI.SetGUIBackgroundColor(AccentColorGray);
                qLabel.text = "Listeners - OnValueChanged";
                QUI.PropertyField(bindsData.GetArrayElementAtIndex(index).FindPropertyRelative("OnValueChanged"), true, qLabel.content, width + 9);
                QUI.ResetColors();
            }
            QUI.EndHorizontal();
        }


        void DrawBindObservers(BindInfo bindInfo, float width)
        {
            int observersBackgroundHeight = 16 + bindInfo.GetInspectorObserverBoxHeight();
            QUI.Box(QStyles.GetBackgroundStyle(Style.BackgroundType.Low, QColors.Color.Purple), width, observersBackgroundHeight);
            QUI.Space(-observersBackgroundHeight);

            if(QUI.SlicedButton("Add Observer", QColors.Color.Purple, width))
            {
                Undo.RecordObject(ezBindExtension, "AddObserver");
                bindInfo.AddObserver();
            }

            if(!bindInfo.HasObservers) { return; }

            QUI.Space(SPACE_2);

            for(int i = 0; i < bindInfo.bindData.observers.Count; i++)
            {
                // Draw GO/SO selection
                QUI.BeginHorizontal(width);
                {
                    QUI.Space(EzBindEditor.LABEL_INDENT);
                    qLabel.text = "Observer " + i;
                    qLabel.style = Style.Text.Normal;
                    QUI.Label(qLabel.text, Style.Text.Normal, qLabel.x, EzBindEditor.LABEL_HEIGHT);

                    Object tempObj;
                    if(bindInfo.bindData.observers[i].useScriptableObject)
                    {
                        tempObj = bindInfo.bindData.observers[i].scriptableObject;
                    }
                    else
                    {
                        tempObj = bindInfo.bindData.observers[i].gameObject;
                    }

                    tempWidth = width - qLabel.x - SPACE_16 - SPACE_16 - EzBindEditor.LABEL_INDENT - 12;  //this will be the ObjectField width - it is used to add/remove space for the EzDataManager button

#if EZ_DATA_MANAGER || EZ_PLAYMAKER_SUPPORT || EZ_BOLT_SUPPORT
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                    if(ezDataManager != null) //if EzDataManager was found in this scene - adjust the ObjectField width
                    {
#endif
                        tempWidth -= SPACE_16;
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                    }
#endif
#endif
                    tempWidth *= bindInfo.show.faded;
                    QUI.BeginChangeCheck();
                    EzBindEditor.SetBackgroundRedIfCondition(tempObj == null);
                    tempObj = QUI.ObjectField(tempObj, typeof(Object), true, tempWidth);
                    QUI.ResetColors();
                    if(QUI.EndChangeCheck())
                    {
                        if(tempObj == null)
                        {
                            Undo.RecordObject(ezBindExtension, "UpdateObserver");
                            bindInfo.ResetObserver(i);
                        }
                        else if(typeof(GameObject).IsInstanceOfType(tempObj))
                        {
                            Undo.RecordObject(ezBindExtension, "UpdateObserver");
                            bindInfo.UpdateObserverWithGO(i, (GameObject) tempObj, null, null, null);
                        }
                        else if(typeof(ScriptableObject).IsInstanceOfType(tempObj))
                        {
                            Undo.RecordObject(ezBindExtension, "UpdateObserver");
                            bindInfo.UpdateObserverWithSO(i, (ScriptableObject) tempObj, null, null);
                        }
                        else
                        {
                            QUI.DisplayDialog("Invalid selection", "Only GameObjects and ScriptableObjects are allowed!", "OK");
                        }
                    }
                    if(QUI.ButtonMinus())
                    {
                        Undo.RecordObject(ezBindExtension, "RemoveObserver");
                        bindInfo.RemoveObserverAt(i);
                        QUI.ExitGUI();
                    }
                    if(QUI.ButtonReset())
                    {
                        Undo.RecordObject(ezBindExtension, "ResetObserver");
                        bindInfo.ResetObserver(i);
                    }
#if EZ_DATA_MANAGER || EZ_PLAYMAKER_SUPPORT || EZ_BOLT_SUPPORT
#pragma warning disable 0219
                    bool addSeparator = false;
#pragma warning restore 0219
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                    if(ezDataManager != null) //if EzDataManager was found in this scene - show the extra button that allows setting EzDataManager as the source
                    {
#endif
                        if(QUI.ButtonData())
                        {
                            GenericMenu menu = new GenericMenu();
#if EZ_DATA_MANAGER
                            if(ezDataManager != null) //if EzDataManager was found in this scene - show the extra button that allows setting EzDataManager as the source
                            {
                                EzBindEditor.SetEzDataManagerAsObserver(ezBindExtension, ezDataManager.gameObject, menu, bindInfo, i);
                                addSeparator = true;
                            }
#endif
#if EZ_PLAYMAKER_SUPPORT
                        if(addSeparator) { menu.AddSeparator(""); }
                        EzBindEditor.SetPlayMakerGlobalsAsObserver(ezBindExtension, menu, bindInfo, i);
                        addSeparator = true;
#endif
#if EZ_BOLT_SUPPORT
                        if(addSeparator) { menu.AddSeparator(""); }
                        EzBindEditor.SetBoltVarsAsObsever(ezBindExtension, menu, bindInfo, i);
#endif
                            menu.ShowAsContext();
                        }
#if EZ_DATA_MANAGER && !EZ_PLAYMAKER_SUPPORT && !EZ_BOLT_SUPPORT
                    }
#endif
#endif
                    QUI.FlexibleSpace();
                }
                QUI.EndHorizontal();
                // Draw Component (GO only)
                if(!bindInfo.bindData.observers[i].useScriptableObject && bindInfo.bindData.observers[i].gameObject != null)
                {
                    QUI.Space(SPACE_2);
                    QUI.BeginHorizontal(width);
                    {
                        QUI.Space(qLabel.x + EzBindEditor.LABEL_INDENT + SPACE_2);

                        int tempComponentIndex = bindInfo.GetObserverComponentIndex(i);
                        QUI.BeginChangeCheck();
                        EzBindEditor.SetBackgroundRedIfCondition(tempComponentIndex == 0);
                        tempComponentIndex = EditorGUILayout.Popup(tempComponentIndex, bindInfo.GetObserverComponentNamesArray(i), GUILayout.Width((width - qLabel.x - EzBindEditor.LABEL_INDENT - 6) * bindInfo.show.faded));
                        QUI.ResetColors();
                        if(QUI.EndChangeCheck())
                        {
                            Undo.RecordObject(ezBindExtension, "UpdateComponent");
                            bindInfo.UpdateObserverWithGO(i, bindInfo.bindData.observers[i].gameObject, bindInfo.GetObserverComponentsArray(i)[tempComponentIndex], null, null);
                        }
                        QUI.FlexibleSpace();
                    }
                    QUI.EndHorizontal();
                    QUI.Space(-5);
                }

                tempWidth = width;
                // if an usable holder is selected, make room for the button
                if(bindInfo.ObserverMayUseHolder(i))
                {
                    tempWidth -= SPACE_16;
                }
                // Draw Holder & Variable
                if((bindInfo.bindData.observers[i].useScriptableObject && bindInfo.bindData.observers[i].scriptableObject != null)
                || (!bindInfo.bindData.observers[i].useScriptableObject && bindInfo.bindData.observers[i].component != null))
                {
                    QUI.Space(SPACE_2);
                    QUI.BeginHorizontal(tempWidth);
                    {
                        QUI.Space(qLabel.x + EzBindEditor.LABEL_INDENT + SPACE_2);

                        int tempHolderNameIndex = bindInfo.GetObserverHolderNameIndex(i);
                        QUI.BeginChangeCheck();
                        EzBindEditor.SetBackgroundRedIfCondition(tempHolderNameIndex == 0);
                        tempHolderNameIndex = EditorGUILayout.Popup(tempHolderNameIndex, bindInfo.GetObserverHolderNamesArray(i), GUILayout.Width((tempWidth - qLabel.x - EzBindEditor.LABEL_INDENT - 6) * bindInfo.show.faded));
                        QUI.ResetColors();
                        if(QUI.EndChangeCheck())
                        {
                            Undo.RecordObject(ezBindExtension, "UpdateVariable");
                            if(bindInfo.bindData.observers[i].useScriptableObject) { bindInfo.UpdateObserverWithSO(i, bindInfo.bindData.observers[i].scriptableObject, bindInfo.GetObserverHolderNamesArray(i)[tempHolderNameIndex], null); }
                            else { bindInfo.UpdateObserverWithGO(i, bindInfo.bindData.observers[i].gameObject, bindInfo.bindData.observers[i].component, bindInfo.GetObserverHolderNamesArray(i)[tempHolderNameIndex], null); }
                        }

                        if(tempWidth != width)
                        {
                            QUI.Space(-2);
                            if(bindInfo.bindData.observers[i].useHolder)
                            {
                                if(QUI.ButtonMinus())
                                {
                                    Undo.RecordObject(ezBindExtension, "DoNotUseHolder");
                                    bindInfo.bindData.observers[i].useHolder = false;
                                    if(bindInfo.bindData.observers[i].useScriptableObject) { bindInfo.UpdateObserverWithSO(i, bindInfo.bindData.observers[i].scriptableObject, bindInfo.GetObserverHolderNamesArray(i)[tempHolderNameIndex], null); }
                                    else { bindInfo.UpdateObserverWithGO(i, bindInfo.bindData.observers[i].gameObject, bindInfo.bindData.observers[i].component, bindInfo.GetObserverHolderNamesArray(i)[tempHolderNameIndex], null); }
                                }
                            }
                            else
                            {
                                if(QUI.ButtonPlus())
                                {
                                    Undo.RecordObject(ezBindExtension, "UseHolder");
                                    bindInfo.bindData.observers[i].useHolder = true;
                                    if(bindInfo.bindData.observers[i].useScriptableObject) { bindInfo.UpdateObserverWithSO(i, bindInfo.bindData.observers[i].scriptableObject, bindInfo.GetObserverHolderNamesArray(i)[tempHolderNameIndex], null); }
                                    else { bindInfo.UpdateObserverWithGO(i, bindInfo.bindData.observers[i].gameObject, bindInfo.bindData.observers[i].component, bindInfo.GetObserverHolderNamesArray(i)[tempHolderNameIndex], null); }
                                }
                            }
                        }

                    }
                    QUI.EndHorizontal();

                    // Draw Variable under Holder (if using holder)
                    if(bindInfo.ObserverMayUseHolder(i) && bindInfo.bindData.observers[i].useHolder)
                    {
                        QUI.Space(-1);
                        QUI.BeginHorizontal(tempWidth);
                        {
                            QUI.Space(qLabel.x + EzBindEditor.LABEL_INDENT + SPACE_2);

                            int tempVarNameIndex = bindInfo.GetObserverVariableNameIndex(i);
                            QUI.BeginChangeCheck();
                            EzBindEditor.SetBackgroundRedIfCondition(tempVarNameIndex == 0);
                            tempVarNameIndex = EditorGUILayout.Popup(tempVarNameIndex, bindInfo.GetObserverVariableNamesArray(i), GUILayout.Width((tempWidth - qLabel.x - EzBindEditor.LABEL_INDENT - 6) * bindInfo.show.faded));
                            QUI.ResetColors();
                            if(QUI.EndChangeCheck())
                            {
                                Undo.RecordObject(ezBindExtension, "UpdateVariable");
                                if(bindInfo.bindData.observers[i].useScriptableObject) { bindInfo.UpdateObserverWithSO(i, bindInfo.bindData.observers[i].scriptableObject, bindInfo.GetObserverHolderNamesArray(i)[bindInfo.GetObserverHolderNameIndex(i)], bindInfo.GetObserverVariableNamesArray(i)[tempVarNameIndex]); }
                                else { bindInfo.UpdateObserverWithGO(i, bindInfo.bindData.observers[i].gameObject, bindInfo.bindData.observers[i].component, bindInfo.GetObserverHolderNamesArray(i)[bindInfo.GetObserverHolderNameIndex(i)], bindInfo.GetObserverVariableNamesArray(i)[tempVarNameIndex]); }
                            }
                            QUI.FlexibleSpace();
                        }
                        QUI.EndHorizontal();
                    }

                    QUI.Space(-SPACE_2);
                }

                QUI.Space((bindInfo.bindData.observers[i].component == null && bindInfo.bindData.observers[i].scriptableObject == null) ? 0 : -SPACE_2);

                QUI.Space(SPACE_2 * bindInfo.show.faded);

                if(i != bindInfo.ObserversCount - 1)
                {
                    QUI.Space(4 * bindInfo.show.faded);
                    QUI.DrawLine(QColors.Color.Purple, width);
                    QUI.Space(1);
                }
            }
        }

        void AddBind()
        {
            Undo.RecordObject(ezBindExtension, "AddBind");
            if(ezBindExtension.bindsData == null)
            {
                ezBindExtension.bindsData = new List<BindData>();
                bindsInfo = new List<BindInfo>();
            }
            var newBindData = new BindData() { bindName = EzBindEditor.UNNAMED_BIND };
            ezBindExtension.bindsData.Add(newBindData);
            bindsInfo.Add(new BindInfo(newBindData, Repaint) { newBindName = EzBindEditor.UNNAMED_BIND });
            InitializeVariables();
        }
        void DeleteBind(BindInfo bindInfo)
        {
            if(ezBindExtension.bindsData == null || ezBindExtension.bindsData.Count == 0) { return; }
            Undo.RecordObject(ezBindExtension, "DeleteBind");
            ezBindExtension.bindsData.Remove(bindInfo.bindData);
            bindsInfo.Remove(bindInfo);
            InitializeVariables();
        }
    }
}
