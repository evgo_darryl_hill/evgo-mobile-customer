﻿// Copyright (c) 2016 - 2018 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using QuickEditor;
using UnityEditor;

#pragma warning disable 0162
namespace Ez.Internal
{
    [InitializeOnLoad]
    public class BindSymbolLoader
    {
        static BindSymbolLoader()
        {
            EditorApplication.update += RunOnce;
        }

        static void RunOnce()
        {
            EditorApplication.update -= RunOnce;
            CreateMissingFolders();
            LoadSymbol();
        }

        static void CreateMissingFolders()
        {
            if(!AssetDatabase.IsValidFolder(EZT.PATH + "/Bind/Editor")) { AssetDatabase.CreateFolder(EZT.PATH + "/Bind", "Editor"); }
            if(!AssetDatabase.IsValidFolder(EZT.PATH + "/Bind/Editor/Resources")) { AssetDatabase.CreateFolder(EZT.PATH + "/Bind/Editor", "Resources"); }
            if(!AssetDatabase.IsValidFolder(EZT.PATH + "/Bind/Editor/Resources/EZT")) { AssetDatabase.CreateFolder(EZT.PATH + "/Bind/Editor/Resources", "EZT"); }
            if(!AssetDatabase.IsValidFolder(EZT.PATH + "/Bind/Editor/Resources/EZT/Bind")) { AssetDatabase.CreateFolder(EZT.PATH + "/Bind/Editor/Resources/EZT", "Bind"); }
            if(!AssetDatabase.IsValidFolder(EZT.PATH + "/Bind/Editor/Resources/EZT/Bind/Version")) { AssetDatabase.CreateFolder(EZT.PATH + "/Bind/Editor/Resources/EZT/Bind", "Version"); }
        }

        static void LoadSymbol()
        {
#if EZ_SOURCE
            return;
#endif
            QUtils.AddScriptingDefineSymbol(EZT.SYMBOL_EZ_BIND);
        }
    }
}
#pragma warning restore 0162
