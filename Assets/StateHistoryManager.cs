﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace EVgo
{
    //state history helper
    public enum eUIDirection
    {
        backwards, jump, forwards
    }

    public static class StateHistoryManager
    {
        public static void UpdateStateHistory(List<string> aHistory, string stateName, eUIDirection aDirection)  //update with this new state
        {
            if (aHistory.LastOrDefault() != stateName)
            {
                switch (aDirection)
                {
                    case eUIDirection.backwards:
                        aHistory.RemoveAt(aHistory.Count - 1);
                        break;

                    case eUIDirection.forwards:
                    case eUIDirection.jump:
                        aHistory.Add(stateName);
                        break;
                }
            }
        }
    }

}