﻿using UnityEngine;

namespace EVgo
{
    [CreateAssetMenu(menuName = "Settings/API", fileName = "RestAPISettings", order = 52)]

    public class APISettings : ScriptableObject
    {
        [SerializeField] private string section = "locations/";
        [SerializeField] private string specifics = "mapping?latitude=33.923&longitude=-118.388&distance=10&metric=MILES";

        // key value pairs  to map keywords to values 


    }
}
