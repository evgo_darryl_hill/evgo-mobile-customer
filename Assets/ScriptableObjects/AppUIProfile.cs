﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EVgo
{
    [CreateAssetMenu(menuName = "Settings/UIProfile", fileName = "AppUIProfile", order = 52)]

    //public abstract class AppUIProfile : ScriptableObject
    public class AppUIProfile : ScriptableObject
    {
        public AppUserType userType;
        //public string startState;
        //public string lastState;
        public string fontName;
        public string fontSize;  // small, medium large
        public string style;
        public Color accentColor;
        public string motif;

        //public abstract void Initialize(GameObject obj);  // replaces awake or start callbacks
        //public abstract void ReadyAction();   // any method with no objects  
        //public abstract void IsValidAction();
    }
}
