﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Diagnostics.Contracts;

public class ScriptableObjectPersistance : MonoBehaviour
{

    [Header("Meta")]
    public string persisterName;
    [Header("ScriptableObjects")]
    public List<ScriptableObject> objectsToPersist;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void OnEnable()
    {

        for (int i = 0; i < objectsToPersist.Count; i++)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath
                                        + string.Format("/{0}_{1}.pso", persisterName, i), FileMode.OpenOrCreate);
            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), objectsToPersist[i]);
            file.Close();
        }
    }

    void OnDisable()
    {
        for (int i = 0; i < objectsToPersist.Count; i++)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath
                                          + string.Format("/{0}_{1}.pso", persisterName, i));
            var json = JsonUtility.ToJson(objectsToPersist[i]);
            bf.Serialize(file, json);
            file.Close();
        }

    }
}
