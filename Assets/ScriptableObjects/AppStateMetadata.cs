﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Talespin;
using UnityEngine.Events;

namespace EVgo
{
    public enum DataState
    {
        none,
        first,
        second,
        third,
        count
    }

    [System.Serializable]
    [CreateAssetMenu(menuName = "Settings/AppStateMetadata", fileName = "AppStateMetadata", order = 60)] // adds to menu
    public class AppStateMetadata : ScriptableObject  // can have struct within a struct
    {
        public AppStateMetadata()
        {

        }

        [SerializeField] private DataState dataState;
        public DataState DataState { get; set; } = DataState.third;

        //public static event System.Action<string> OnAppStateChangedTo = delegate { };
        public static event UnityAction<string> OnAppStateChangedTo = delegate { };
        [SerializeField] private string currentState;
        public string CurrentState
        {
            get { return currentState; }
            set
            {
                currentState = value;
                OnAppStateChangedTo.Invoke(value);
            }
        }

        public static event UnityAction<bool> OnAppLockChanged = delegate { };
        [SerializeField] private bool stateIsLocked;
        public bool StateIsLocked
        {
            get { return stateIsLocked; }
            set
            {
                stateIsLocked = value;
                OnAppLockChanged.Invoke(value);
            }
        }

        public event System.Action<ServerSchema.ChargingLocationInfo> OnDataChargingLocation = delegate { };  // initializes so we dont have to check for null 
        [SerializeField] private ServerSchema.ChargingLocationInfo selectedLocation;
        public ServerSchema.ChargingLocationInfo SelectedLocation
        {
            get
            {
                return selectedLocation;
            }
            set
            {
                selectedLocation = value; OnDataChargingLocation(value);
            }
        }

        public event System.Action<ServerSchema.Station> OnDataChargerSet = delegate { };  // initializes so we dont have to check for null 
        [SerializeField] private ServerSchema.Station selectedCharger;
        public ServerSchema.Station SelectedCharger
        {
            get
            {
                return selectedCharger;
            }
            set
            {
                selectedCharger = value; OnDataChargerSet(value);
            }
        }

        public event System.Action<ServerSchema.Station> OnDataConnectorSet = delegate { };  // initializes so we dont have to check for null 
        [SerializeField] private ServerSchema.Station selectedConnector;
        public ServerSchema.Station SelectedConnector
        {
            get
            {
                return selectedConnector;
            }
            set
            {
                selectedConnector = value; OnDataConnectorSet(value);
            }
        }
    }
}

