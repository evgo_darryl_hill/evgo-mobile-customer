﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//  USAGE:  
// UserSettings data = UserSettings.CreateInstance(username: "on-the-fly", latitude: -5f, longitude: -5f, metric: MetricType.Miles, searchRadius: 10);

namespace EVgo
{
    //TODO:  move to some other location 
    public enum ChargerType
    {
        Combo, Chademo
    }

    public enum UsageType
    {

        Light, Moderate, Heavy
    }

    public enum MetricType
    {
        Miles, Kilometers
    }

    [CreateAssetMenu(menuName = "Settings/User", fileName = "UserSettings", order = 51)] // adds to menu

    [System.Serializable]
    public class UserSettings : ScriptableObject
    {
        [SerializeField] private string username = "";
        public string Username { get { return username; } set { username = value; } }

        [SerializeField] private MetricType metric = MetricType.Miles;
        public MetricType Metric { get { return metric; } set { metric = value; } }

        [SerializeField] private bool onlyAvailable = true;
        public bool OnlyAvailable { get { return OnlyAvailable; } set { onlyAvailable = value; } }

        [SerializeField] private bool inNetworkOnly = true;
        public bool InNetworkOnly { get { return inNetworkOnly; } set { inNetworkOnly = value; } }

        [SerializeField] private float latitude = 0f;
        public float Latitude { get { return latitude; } set { latitude = value; } }

        [SerializeField] private float longitude = 0.0f;
        public float Longitude { get { return longitude; } set { longitude = value; } }

        [SerializeField] private float maxSearchRadius = 5;
        public float MaxSearchRadius { get { return maxSearchRadius; } set { maxSearchRadius = value; } }

        [SerializeField] private string vehicle = "";
        public string Vehicle { get { return vehicle; } set { vehicle = value; } }

        [SerializeField] private UsageType driverUsageType = UsageType.Moderate;
        public UsageType DriverUsageType { get { return driverUsageType; } set { driverUsageType = value; } }

        [SerializeField] private string chargerPreference = "";
        public string ChargerPreference { get { return chargerPreference; } set { chargerPreference = value; } }

        [SerializeField] private string preferredZip = "";
        public string PreferredZip { get { return preferredZip; } set { preferredZip = value; } }

        [SerializeField] private List<string> recentLocations = new List<string>();
        public List<string> RecentLocations
        {
            get { return recentLocations; }
            set { recentLocations = value; }
        }

        [SerializeField] private List<string> recentChargers = new List<string>();
        public List<string> RecentChargers
        {
            get { return recentChargers; }
            set { recentChargers = value; }
        }

        //[SerializeField] private ServerSchema.ChargingLocationInfo chargingLocationInfo;
        //public ServerSchema.ChargingLocationInfo ChargingLocationInfo
        //{
        //    get { return chargingLocationInfo; }
        //    set { chargingLocationInfo = value; }
        //}

        // ==============================================================================================

        // TODO: May need init
        public static UserSettings CreateInstance(string username,
                                                  float latitude,
                                                  float longitude,
                                                  MetricType metric,
                                                  int searchRadius,
                                                  List<string> recentLocations,
                                                 List<string> recentChargers)
        {
            var data = ScriptableObject.CreateInstance<UserSettings>();

            data.username = username;
            data.longitude = longitude;
            data.latitude = latitude;
            data.metric = metric;
            data.maxSearchRadius = searchRadius;
            data.recentLocations = recentLocations;
            data.recentChargers = recentChargers;

            //data.chargingLocationInfo = chargingLocationInfo;

            return data;
        }

        public void Print()
        {
            Debug.Log(this.ToString());
        }
    }

}
