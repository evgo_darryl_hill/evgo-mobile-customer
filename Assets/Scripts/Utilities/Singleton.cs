﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EVgo
{
    // Singleton Pattern
    public abstract class Singleton<T>
        where T : class, new()
    {
        private static T _instance = null;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new T();
                    return _instance;
                }
                return _instance;
            }
        }

        public virtual void Awake()
        {
            _instance = this as T;
        }

        protected virtual void OnEnable()
        {


        }

        protected virtual void OnDisable()
        {


        }
    }
}