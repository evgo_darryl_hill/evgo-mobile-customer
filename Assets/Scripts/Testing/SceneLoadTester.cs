﻿using UnityEngine;
using System.Collections;
using DoozyUI;
using UnityEngine.SceneManagement;

namespace EVgo
{
    public class SceneLoadTester : MonoBehaviour  // for loading and unloading scenes as necessary
    {
        public void Start()
        {
            //SceneManager.LoadSceneAsync("Splash");
            //UIManagerSendGameEvent.LoadSceneAsync("Intro"); // bootstrap
            //UIManager.SendGameEvent(SceneLoader.DEFAULT_LOAD_SCENE_ADDITIVE_ASYNC_SCENE_NAME )
            //SceneManager.("Intro");
            UIManager.SendGameEvent(SceneLoader.DEFAULT_LOAD_SCENE_ADDITIVE_ASYNC_SCENE_NAME + "AppIntro"); //to play intro otherwise just the splash screen
        }

        public void OnIntroEnd()  //TODO: need some generic method name here
        {
            Debug.Log("OnIntroEnd");
            UIManager.SendGameEvent(SceneLoader.DEFAULT_UNLOAD_SCENE_SCENE_NAME + "AppIntro");
            //UIManager.SendGameEvent(SceneLoader.DEFAULT_LOAD_SCENE_ADDITIVE_ASYNC_SCENE_NAME + "App-ex-function");
        }

        public void LoadScene(string sceneName)
        {
            UIManager.SendGameEvent(SceneLoader.DEFAULT_LEVEL_SCENE_NAME + sceneName);
        }

        public void OnButtonClick(string buttonName)
        {

            if (!buttonName.Contains("ButtonLevel")) { return; }

            UIManager.SendGameEvent(SceneLoader.DEFAULT_LEVEL_SCENE_NAME + buttonName.Replace("ButtonLevel", ""));
        }
    }
}