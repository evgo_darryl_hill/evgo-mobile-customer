﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EVgo
{
    public static class AppLocalCache
    {
        public static List<ServerSchema.ChargingLocationInfo> serverChargingLocations;

        public static void SetLocalCacheToData(List<ServerSchema.ChargingLocationInfo> locs)
        {
            // var filteredRows = filterViewController.FilterRecords(rows);
            serverChargingLocations = locs;
            Debug.Log("Server locations = " + serverChargingLocations.Count + " - vs - " + locs.Count);

            //TODO:  need to save this possibly 
        }

        public static ServerSchema.ChargingLocationInfo GetLocationByName(string locationName)
        {
            ServerSchema.ChargingLocationInfo hit;

            foreach (ServerSchema.ChargingLocationInfo chargingLocation in serverChargingLocations)
            {
                if (chargingLocation.name == locationName)
                {
                    hit = chargingLocation;
                    return hit;
                }
            }
            Debug.Log("No location found");
            return null;
        }

        public static List<ServerSchema.Station> GetChargersAtLocationNamed(string locationName)
        {
            ServerSchema.ChargingLocationInfo hit;

            foreach (ServerSchema.ChargingLocationInfo chargingLocation in serverChargingLocations)
            {
                if (chargingLocation.name == locationName)
                {
                    hit = chargingLocation;
                    //Debug.Log("Key - " + chargingLocation.externalId);
                    //Debug.Log("Name =" + chargingLocation.name);
                    //Debug.Log("stations =" + chargingLocation.stations);

                    return hit.stations;
                }
            }

            return null;
        }

        public static ServerSchema.Station GetChargerNamed(string chargerName)
        {
            ServerSchema.Station hit;  // station = charger

            List<ServerSchema.Station> chargers =
                                 GetChargersAtLocationNamed(AppStateController.Instance.appStateData.SelectedLocation.name);

            foreach (ServerSchema.Station charger in chargers)
            {
                if (charger.name == chargerName)
                {
                    hit = charger;
                    return hit;
                }
            }

            return null;
        }

        /*
         *
        //private static List<ServerSchema.ChargingLocationInfo> serverChargingLocations;
                    //public static List<ServerSchema.ChargingLocationInfo> ServerChargingLocations { get; private set; }
                    //= new List<ServerSchema.ChargingLocationInfo>(); 
         */
    }
}