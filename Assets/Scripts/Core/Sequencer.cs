﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EVgo
{
    public static class Sequencer  //, IUIAnimatableSingle
    {
        public static int numberOfRounds;

        #region Next/Previous support

        public static int GetNextIndex(List<string> list, int currentIndex)  // maxIndex s/b count if using enumerator
        {

            int newIdx = (currentIndex == list.Count - 1) ? currentIndex : currentIndex + 1;
            return newIdx;
        }

        public static int GetPreviousIndex(List<string> list, int currentIndex)  // count
        {
            int newIdx = (currentIndex == 0) ? 0 : currentIndex - 1;

            return newIdx;

        }
        #endregion

        //public static int GetNextIndex(int currentIndex, int startIndex, int maxIndex = 0)  // maxIndex s/b count if using enumerator
        //{
        //    //int newIdx = DetermineNextIndex(state);
        //    int newIdx = currentIndex + 1;

        //    if (newIdx >= maxIndex)
        //    {
        //        newIdx = startIndex;
        //    }

        //    Debug.Log("***" + currentIndex + " < == > " + newIdx);

        //    return newIdx;
        //}

        //public static int GetPreviousIndex(int currentIndex, int startIndex, int maxIndex = 0)  // count
        //{
        //    int newIdx = currentIndex - 1;   // vs idx++

        //    if (newIdx < 0)
        //    {
        //        newIdx = maxIndex - 1;
        //    }

        //    return newIdx;
        //}

    }
}