﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Diagnostics.Contracts;

namespace EVgo
{
    public class UserSettingsManager
    {
        // == persistance layer
        // const string filePath = @"c:\dump\json.txt";
        // const string filePath = "/app-save/UserSettings.txt";
        const string settingsFilename = @"UserSettings.txt";  // @ sign makes it a literal string
        private static string filePath = Application.persistentDataPath + settingsFilename;

        public static void Serialize(UserSettings settings)
        {
            var serializer = new JsonSerializer();

            try
            {
                using (var sw = new StreamWriter(filePath))
                using (JsonWriter writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, settings);
                }
            }
            catch
            {
                Debug.Log("Error Serializing user settings");
            }
        }

        public static UserSettings Deserialize(string path)
        {
            //Contract.Ensures(Contract.Result<UserSettings>() != null);

            var serializer = new JsonSerializer();
            try
            {
                using (var sw = new StreamReader(path))
                using (var reader = new JsonTextReader(sw))
                {
                    return serializer.Deserialize<UserSettings>(reader);
                }
            }
            catch
            {
                Debug.Log("Error deserializing user settings");
                return null;
            }
        }

        public static void SaveAppData(UserSettings settings)
        {

            Serialize(settings);
        }

        public static UserSettings LoadAppData()
        {
            UserSettings retrievedSettings = Deserialize(filePath);
            return retrievedSettings;
        }


        // ==== 
        bool IsSavedFile()
        {
            return Directory.Exists(Application.persistentDataPath + filePath);
        }

        public void CreateFile()
        {
            if (!IsSavedFile())
            {
                Directory.CreateDirectory(Application.persistentDataPath + filePath);
            }

            if (!Directory.Exists(Application.persistentDataPath + filePath))
            {

            }

        }

    }
}
