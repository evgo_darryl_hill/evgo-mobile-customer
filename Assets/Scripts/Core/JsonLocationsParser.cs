﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;
using UnityEngine.UI;
using UnityEngine.Events;

namespace EVgo
{
    public class JsonLocationsParser : MonoBehaviour  //
    {
        public static event Action<List<ServerSchema.ChargingLocationInfo>> OnLocationsParsed = delegate { };  // initializes so we dont have to check for null 

        void Awake()
        {
            APIController.OnLocationDataLoad += APIRequester_OnLocationDataLoad;  // how I start processing

        }

        void APIRequester_OnLocationDataLoad(string aJsonString)
        {
            //TODO:  try catch
            string newJsonString = "{\"rows\":" + aJsonString + "}";
            ServerSchema jsonSchema = JsonConvert.DeserializeObject<ServerSchema>(newJsonString);     // convert into object 

            if (jsonSchema.rows == null || jsonSchema.rows.Count == 0)
            {
                //OnSystemNotifications(no data found);
                Debug.Log("Warning - no data found == " + jsonSchema);
                return;
            }

            Debug.Log(jsonSchema.rows[0].name + jsonSchema.rows[0].fullAddress);
            Debug.Log(jsonSchema.rows[0].stations[0].name); // 1st station name
            Debug.Log(jsonSchema.rows[0].stations[0].stationPorts[0].stationPortStatus);  // 1st port
            Debug.Log(jsonSchema.rows[0].stations[0].stationPorts[0].powerLevel);
            Debug.Log(jsonSchema.rows[0].stations[0].stationPorts[0].handicapAccessible);

            // save to persistant store and send notification then controller picks it up 
            OnLocationsParsed(jsonSchema.rows);  // nested here

            AppManager.Instance.UnlockApp();
            Debug.Log("Done man - show the screen");
        }

    }

    #region DebugOutput

    //private void HeyThere(string strDebugText)
    //{
    //    try
    //    {
    //        System.Diagnostics.Debug.Write(strDebugText + Environment.NewLine);
    //        //.Text = txtDebugOutput.text + strDebugText + Environment.NewLine;
    //        //txtDebugOutput.SelectionStart = txtDebugOutput.TextLength;
    //        //txtDebugOutput.ScrollToCaret();

    //    }
    //    catch (System.Exception ex)
    //    {

    //        System.Diagnostics.Debug.Write(ex.Message.ToString() + Environment.NewLine);


    //    }

    //}
    #endregion
}