﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

// this is a straight map from the actual schema
namespace EVgo
{
    public class ServerSchema
    {
        public List<ChargingLocationInfo> rows;

        public class ChargingLocationInfo
        {
            public string name { get; set; }
            public string fullAddress { get; set; }
            public GeoPoint point { get; set; } // x, y
            public string externalId { get; set; }
            public List<Station> stations { get; set; }// needs to be parsed 
            public bool hideOnMap { get; set; }
        }

        public class GeoPoint
        {
            float x;
            float y;
        }

        public class Station
        {
            public string externalId { get; set; }
            public string name { get; set; }
            public List<StationPort> stationPorts { get; set; }
        }

        public class StationPort
        {
            public string stationPortStatus { get; set; }
            public string powerLevel { get; set; }
            public string qrCode { get; set; }
            public string handicapAccessible { get; set; }
            public string pricingPolicyExternalId { get; set; }
            public string free { get; set; }
        }
    }

}