﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;

namespace EVgo
{
    [RequireComponent(typeof(Animator))]

    public class AppStateController : MonoBehaviour  //, IUINavigationHandler
    {
        private static AppStateController _instance;          //Singleton pattern - 1 of 2
        public static AppStateController Instance { get { return _instance; } }

        public static Animator animator;
        public AnimatorStateInfoHelper animatorHelper { get; private set; }
        public AppUIPresenter presenter;
        public string requestedStateName; //, currentStateName;

        public eUIDirection currentDirection;
        public int historyIndex;
        public AppStateMetadata appStateData;
        [SerializeField] private List<string> stateHistory = new List<string>();


        void Awake()
        {
            if (_instance != null && _instance != this)   // Singleton pattern - 2 of 2
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
                SetupBindings();
            }

            DontDestroyOnLoad(this);
        }

        void SetupBindings()
        {
            animator = this.GetComponent<Animator>();
            presenter = this.GetComponent<AppUIPresenter>();
        }

        public void Start()
        {
            ChangeStateTo(requestedStateName);
        }

        #region Callback Handling
        void OnEnable()
        {
            SmbEventDispatcher.OnStateEntered += StateEnterRequest;
            SmbEventDispatcher.OnStateExited += StateExitRequest;
        }

        private void OnDisable()
        {
            SmbEventDispatcher.OnStateEntered -= StateEnterRequest;
            SmbEventDispatcher.OnStateExited -= StateExitRequest;
        }
        #endregion

        #region Exposed Behavior
        public void ChangeStateTo(string stateName)
        {
            if (string.IsNullOrEmpty(stateName)) return;

            ChangeStateTo(stateName, eUIDirection.jump);
        }

        public void ChangeStateTo(string stateName, eUIDirection direction)
        {
            if (appStateData.StateIsLocked) return;

            if (StateToUIEBinder.GetElementNames().Contains<string>(stateName))
            {
                currentDirection = direction;
                AppStateController.animator.SetTrigger(stateName);
            }
        }

        public void OnNextAction()
        {
            if (historyIndex < 0) return;

            int nextIndex = (historyIndex == stateHistory.Count - 1) ? historyIndex : historyIndex + 1;
            ChangeStateTo(stateHistory[nextIndex], direction: eUIDirection.forwards);
        }

        public void OnPreviousAction()
        {
            Debug.Log("Previous hit");
            int previousIndex = (historyIndex == 0) ? 0 : historyIndex - 1;
            ChangeStateTo(stateHistory[previousIndex], direction: eUIDirection.backwards);
        }
        #endregion

        public void StateEnterRequest(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            if (!presenter.IsReady) return; //TODO: logic for IS ready

            requestedStateName = TryToGetActiveStateName(animatorStateInfo, layerIndex);
            if (requestedStateName == null) return;
            if (requestedStateName == appStateData.CurrentState && HistoryIsNotEmpty()) return; // same state requested

            DoStateChangeWork(requestedStateName);
        }

        private bool HistoryIsNotEmpty()
        {

            return (stateHistory.Count > 0);
        }

        public void StateExitRequest(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) { }

        public void DoStateChangeWork(string requestedStateName)
        {

            Debug.Log("Here in do state change ");
            if (!string.IsNullOrEmpty(appStateData.CurrentState))
            {
                presenter.HideConnectedElements(appStateData.CurrentState);
            }

            if (string.IsNullOrEmpty(requestedStateName)) return;
            presenter.ShowConnectedElements(requestedStateName);

            appStateData.CurrentState = requestedStateName;  // triggers event
            StateHistoryManager.UpdateStateHistory(stateHistory, appStateData.CurrentState, currentDirection);
            historyIndex = stateHistory.Count - 1;
        }

        private string TryToGetActiveStateName(AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            var scenes = StateToUIEBinder.GetElementNames().ToArray();  // Debug.Log(sceneNames);
            animatorHelper = new AnimatorStateInfoHelper(animatorStateInfo, 0, scenes);

            return animatorHelper.stateName;
        }

        private void MyDebug()
        {
            Debug.Log("Hiding: " + appStateData.CurrentState + " before new request: " + requestedStateName);

            //int i = 0;
            //foreach (string stateName in stateHistory)
            //{
            //    //Debug.Log(i + " ==>  State - " + stateName);
            //    i++;
            //}
        }
    }

}