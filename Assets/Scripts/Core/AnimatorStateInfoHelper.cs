﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace EVgo
{
    public class AnimatorStateInfoHelper
    {
        private Animator animator;
        private AnimatorClipInfo[] clipInfos; // int for layer in FSM
        public string stateName { get; private set; } // clip name
        public float stateDuration;
        public int stateIndex;
        public int layerIndex;

        public AnimatorStateInfoHelper(AnimatorStateInfo animatorStateInfo, int layerIndex, string[] stateNames = null)  // constru
        {
            this.layerIndex = layerIndex;  // only 1 at present
            ParseStateInfo(animatorStateInfo, stateNames);
        }

        public void ParseStateInfo(AnimatorStateInfo animatorStateInfo, string[] stateNames = null)  // enum based
        {
            if (stateNames == null) return;  // return from uncharted waters         

            int i = -1;
            foreach (string val in stateNames)
            {
                i++;
                //Debug.Log("search val = " + val);
                if (animatorStateInfo.IsName(val))  // only way att
                {
                    //Debug.Log("Match at = " + i);
                    this.stateName = val;
                    this.stateIndex = i;
                    break;
                }
            }

            this.stateDuration = animatorStateInfo.length;
        }


        void OnGUI()
        {
            //Output the current Animation name and length to the screen
            if (this != null)
            {
                GUI.Label(new Rect(0, 0, 200, 20), "Clip Name:" + this.stateName);
                GUI.Label(new Rect(0, 30, 200, 20), "Clip Length: " + this.stateDuration);
            }
        }
    }
}