﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace EVgo
{
    public class AppManager : MonoBehaviour
    {
        private static AppManager _instance;  //Singleton pattern - 1 of 2
        public static AppManager Instance { get { return _instance; } }

        public bool IsBusy;

        #region - APP EVENTS
        //public static event System.Action<ChargerLocationCellData> OnChargeLocationSelected = delegate { };  // initializes so we dont have to check for null 
        //public static event System.Action<ChargerCellData> OnChargerSelected = delegate { };  // initializes so we dont have to check for null 
        //public static event System.Action<ChargerCellData> OnChargerPortSelected = delegate { };  // initializes so we dont have to check for null 
        #endregion

        // public static string sectionTitle = "Darryl Hill - EVgo";
        public APIController apiController;  // EVConnect api requester
        [SerializeField] public UserSettings userSettings;
        //[SerializeField] public AppStateMetadata appStateMetadata;
        [SerializeField] private DoozyUI.UINotification notify;

        void Awake()
        {
            if (_instance != null && _instance != this)   // Singleton pattern - 2 of 2
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
                Setup();
            }

            DontDestroyOnLoad(this);
        }
        void Setup()
        {
            JsonLocationsParser.OnLocationsParsed += AppLocalCache.SetLocalCacheToData;
        }

        public void UnlockApp()
        {
            if (IsBusy == false) return;

            notify.HideNotification(true);
            IsBusy = false;
        }


        public void LockApp(string aReason)
        {
            if (IsBusy) return;

            UIManagerAdditive uim = UIManagerAdditive.Instance;
            notify = DoozyUI.UIManager.ShowNotification("Busy", -1, false, "Busy", "Working hard for you. Standby.", uim.NotificationCallbackCancel);  //simple cancel callback
            IsBusy = true;
        }

        // REFERENCE: 
        //void Start()
        //{
        //    ChargingLocationCellView.OnChargeLocationSelected += DoChargeLocationSelected;
        //}

        //public void DoChargeLocationSelected(ServerSchema.ChargingLocationInfo chargingLoc)
        //{
        //    AppStateController.Instance.ChangeStateTo("ChargerListAtLocation");  // format of things
        //    // NOTE:  I also think I can just advance
        //    // NOTE:  any data is pulled once inside the next state
        //}

    }
}







