namespace EVgo
{
    public static class LogTypes
    {
        public const string System = "System";
        public const string App = "App";
        public const string Data = "Data";
        public const string Score = "FindACharger";
        public const string UI = "UI";
        public const string SFX = "SFX";
        public const string VFX = "VFX";
        public const string Graphics = "Graphics";
    }
}
