﻿using System.Collections.Generic;
using UnityEngine;
using DoozyUI;
using System;

namespace EVgo
{
    [System.Serializable]
    public class KeyToUIE : SerializableDictionary<string, List<UIElement>>
    {
        internal void TryGetValue(string currentStateName)
        {
            throw new NotImplementedException();
        }
    }

    public static class StateToUIEBinder
    {
        public static readonly string Prefix = "UIE - ";
        [SerializeField] private static List<string> _appSceneNames = new List<string>();

        public static List<String> GetElementNames()
        {
            if (_appSceneNames.Count == 0) Reset();

            return _appSceneNames;
        }

        private static void Reset()
        {
            _appSceneNames = new List<string>();
            var mcTransform = UIManager.GetMasterCanvas(false).transform;  //TODO: this generates errors
            var elements = GetTopLevelChildren(mcTransform);

            foreach (Transform t in elements)
            {
                //int key = element.Key; //string value = element.Value;           
                string cleaned = t.gameObject.name.Substring(Prefix.Length);  // remove or replace
                _appSceneNames.Add(cleaned);
            }
        }

        // Pattern - get top level children
        public static Transform[] GetTopLevelChildren(Transform Parent)
        {
            Transform[] Children = new Transform[Parent.childCount];
            for (int ID = 0; ID < Parent.childCount; ID++)
            {
                Children[ID] = Parent.GetChild(ID);
            }
            return Children;
        }

    }
}
