﻿namespace EVgo
{
    public enum AppUserType //DH
    {
        None, Dormant, New, Active, Registered, LoggedOut, Invalid, Deactivated, Count
    }

    public enum eMembershipType
    {
        None, PayAsYouGo, Monthly1, Monthly2, Monthly3
    }

    public enum eVehicleType
    {
        Unknown, Tesla, Leaf, Volt
    }

    // scoring run types
    public enum eChargerType
    {
        Unknown, BTC, ABB, Slim
    }

    public enum eChargerStatus
    {
        available, busy, failed, booked
    }

    // ball landing score zones
    public enum eUserType
    {
        New, Registered, Full, Unpaid, Delinquent, Help, Observer
    }

    // score zone kill ball types
    public enum eLocationType
    {
        None, Delayed, Immediate, Available, Construction, LimitedHours
    }

    // game mode states
    public enum eAppState  // dynamic states
    {
        Idle, Login, Find, Charge, Settle,
    }

    public enum eConnectorType
    {
        Combo = 0, Chademo = 1, Level2
    }

}