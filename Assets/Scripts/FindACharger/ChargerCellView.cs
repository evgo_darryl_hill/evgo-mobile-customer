﻿using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

namespace EVgo
{
    public class ChargerCellView : EnhancedScrollerCellView
    {
        /// <summary>
        /// A reference to the rect transform which will be
        /// updated by the content size fitter
        /// </summary>
        public RectTransform textRectTransform;

        /// <summary>
        /// The space around the text label so that we
        /// aren't up against the edges of the cell
        /// </summary>
        public RectOffset textBuffer;

        private Charger myCellData;
        public GameEvent OnChargerSelected;

        #region view elements
        public Text chargerName;
        public Text typeOfCharger;
        public Text rate;
        public Text status;
        public Text numberOfConnectors;
        #endregion

        public void SetData(Charger data)
        {
            //base.SetData(data);
            //someTextText.text = "HI Darryl!" + data.name +  data.address;       
            chargerName.text = data.name;
            typeOfCharger.text = data.type.ToString();
            rate.text = data.rate.ToString();
            status.text = data.status.ToString();
            numberOfConnectors.text = data.numberOfConnectors.ToString();

            // force update the canvas so that it can calculate the size needed for the text immediately
            Canvas.ForceUpdateCanvases();

            // set the data's cell size and add in some padding so the the text isn't up against the border of the cell
            data.cellSize = textRectTransform.rect.height + textBuffer.top + textBuffer.bottom;

            myCellData = data;
        }

        public void OnCellClicked()
        {
            Debug.Log("*** Charger (station) Cell clicked - " + chargerName.text);
            //fire some event with Cell info and data

            AppManager.Instance.userSettings.RecentChargers.Add(myCellData.name); //MyDebug();
            AppStateController.Instance.appStateData.SelectedCharger = AppLocalCache.GetChargerNamed(myCellData.name);  //uses metadata ti determine location
            OnChargerSelected.Raise();
        }
    }
}


