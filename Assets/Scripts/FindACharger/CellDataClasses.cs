﻿using UnityEngine;

namespace EVgo
{
    /// <summary>
    /// Super simple data class to hold information for each cell.
    /// </summary>
    public class CellData
    {
        public string someText;

        /// <summary>
        /// We will store the cell size in the model so that the cell view can update it
        /// </summary>
        public float cellSize;
    }

    public class ChargingLocation : CellData
    {
        public string externalId;
        public string name; //LocationInfo name      
        public string fullAddress;
        public string status;
        public int numberOfChargers;
        public float rate;
        public bool HideOnMap;
        public string distanceFromCurrent;
    }

    public class Charger : CellData
    {
        public string externalId;
        public string name; //LocationInfo name
        public eChargerType type;
        public eChargerStatus status;  // for now, unavailable if one is in use
        public float rate;
        public int numberOfConnectors;
    }

    public class Connector : CellData
    {
        //public string name;
        public string type;
        public string status;  //TODO:  type
        public int maxOutput;
        public int minutesTillFree;
        public bool canDisconnect;
        public float rate;
        public string qrCode;
        public string handicapAccessible;
        public string free;
    }
}