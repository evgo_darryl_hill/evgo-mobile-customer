﻿using UnityEngine;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using UnityEngine.Events;

namespace EVgo
{
    public class ChargingLocationTableView : MonoBehaviour, IEnhancedScrollerDelegate
    {
        public delegate void RecordsTally(int recordCount);
        public static event RecordsTally OnRecordTallyAction;

        /// <summary>
        /// This demo shows how you can use the calculated size of the cell view to drive the scroller's cell sizes.
        /// This can be good for cases where you do not know how large each cell will need to be until the contents are
        /// populated. An example of this would be text cells containing unknown information.
        /// </summary>

        //private List<ChargerLocationInfo> chargerLocations;
        public bool shouldPopulate;
        public ServerSchema _schema;
        private List<ChargingLocation> chargingLocations;
        public EnhancedScroller scroller;
        public EnhancedScrollerCellView cellViewPrefab;
        private UserSettings userSettings;
        //public List<ServerSchema.ChargingLocationInfo> filteredRows;

        void Awake()
        {
            JsonLocationsParser.OnLocationsParsed += SetChargingLocationRowsFrom;
        }

        void Start()
        {
            scroller.Delegate = this;

            userSettings = AppManager.Instance.userSettings;

            Clear();
        }

        public void Clear()
        {
            chargingLocations = new List<ChargingLocation>();

            if (shouldPopulate)
            {
                PopulateWithFooBar();
                shouldPopulate = false;
            }

            ResizeScroller();
        }

        public void SetChargingLocationRowsFrom(List<ServerSchema.ChargingLocationInfo> dataRows)
        {
            // var filteredRows = filterViewController.FilterRecords(rows);

            List<ServerSchema.ChargingLocationInfo> filteredRows = dataRows;
            OnRecordTallyAction?.Invoke(filteredRows.Count);  // optional it seems 

            chargingLocations = new List<ChargingLocation>();

            for (var i = 0; i < dataRows.Count; i++)
            {
                ServerSchema.ChargingLocationInfo location = dataRows[i];  // have to drop going forward
                //_data.Add(new ChargerLocationCellData() { cellSize = 0, someText = i.ToString() + location.name });  // populate the scroller with some text
                chargingLocations.Add(new ChargingLocation()
                {
                    cellSize = 0,
                    name = location.name,
                    fullAddress = location.fullAddress,
                    numberOfChargers = 99,
                    rate = .13f,
                    status = "Available" // "UnderConstruction"
                });
                //_data.Add(new Data() { cellSize = 0, someText = i.ToString() + location.name });
                // Debug.Log("Add to Data (" + i.ToString() + ")==>" + location.name);
            }

            ResizeScroller();
        }

        /// <summary>
        /// This function will exand the scroller to accommodate the cells, reload the data to calculate the cell sizes,
        /// reset the scroller's size back, then reload the data once more to display the cells.
        /// </summary>
        private void ResizeScroller()
        {
            // capture the scroller dimensions so that we can reset them when we are done
            var rectTransform = scroller.GetComponent<RectTransform>();
            var size = rectTransform.sizeDelta;

            // set the dimensions to the largest size possible to acommodate all the cells
            rectTransform.sizeDelta = new Vector2(size.x, float.MaxValue);

            // First Pass: reload the scroller so that it can populate the text UI elements in the cell view.
            // The content size fitter will determine how big the cells need to be on subsequent passes
            scroller.ReloadData();

            // reset the scroller size back to what it was originally
            rectTransform.sizeDelta = size;

            // Second Pass: reload the data once more with the newly set cell view sizes and scroller content size
            // scroller.ReloadData();
            // Debug.Log("Resize scroller");
        }

        #region EnhancedScroller Handlers

        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            return chargingLocations.Count;
        }

        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            // we pull the size of the cell from the model.
            // First pass (frame countdown 2): this size will be zero as set in the LoadData function
            // Second pass (frame countdown 1): this size will be set to the content size fitter in the cell view
            // Third pass (frmae countdown 0): this set value will be pulled here from the scroller

            //return _chargerLocationData[dataIndex].cellSize;
            return 200;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            ChargingLocationCellView cellView = scroller.GetCellView(cellViewPrefab) as ChargingLocationCellView;
            cellView.SetData(chargingLocations[dataIndex]);

            return cellView;
        }

        #endregion
        void PopulateWithFooBar()
        {
            // populate the scroller with some text
            for (var i = 0; i < 7; i++)
            {
                //_data.Add(new ChargerLocationCellData() { cellSize = 0, someText = i.ToString() + location.name });  // populate the scroller with some text
                chargingLocations.Add(new ChargingLocation()
                {
                    cellSize = 0,
                    name = (i * 11 + 0).ToString(),
                    fullAddress = GetRandomString(5, 500),
                    numberOfChargers = 99,
                    rate = 1.99f,
                    status = "MyStatus"
                });

            }
        }

        /// <summary>
        /// This function adds a new record, resizing the scroller and calculating the sizes of all cells
        /// </summary>
        public void AddNewRow()
        {
            // first, clear out the cells in the scroller so the new text transforms will be reset
            scroller.ClearAll();

            // reset the scroller's position so that it is not outside of the new bounds
            scroller.ScrollPosition = 0;

            // second, reset the data's cell view sizes
            foreach (var item in chargingLocations)
            {
                item.cellSize = 0;
            }

            // now we can add the data row
            // _chargerLocationData.Add(new ChargerLocationCellData() { cellSize = 0, someText = _chargerLocationData.Count.ToString() + " New Row Added!" });
            chargingLocations.Add(new ChargingLocation()
            {
                cellSize = 0,
                name = "* NEW ROW*",
                fullAddress = GetRandomString(5, 500),
                numberOfChargers = 00,
                rate = 0.0f,
                status = "Newbie"
            });

            ResizeScroller();

            // optional: jump to the end of the scroller to see the new content
            scroller.JumpToDataIndex(chargingLocations.Count - 1, 1f, 1f);
        }

        private string GetRandomString(int minCharAmount, int maxCharAmount)
        {
            const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789"; //add the characters you want
            string myString = "";
            int charAmount = Random.Range(minCharAmount, maxCharAmount); //set those to the minimum and maximum length of your string
            for (int i = 0; i < charAmount; i++)
            {
                myString += glyphs[Random.Range(0, glyphs.Length)];
            }

            return myString;
        }

    }
}

