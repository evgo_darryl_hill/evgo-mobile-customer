﻿using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;
using System.Collections;
using System.Collections.Generic;

namespace EVgo
{
    public class ChargingLocationCellView : EnhancedScrollerCellView
    {
        //public static event System.Action<ServerSchema.ChargingLocationInfo> OnChargeLocationSelected = delegate { };  // initializes so we dont have to check for null 

        /// <summary>
        /// A reference to the rect transform which will be
        /// updated by the content size fitter
        /// </summary>
        public RectTransform textRectTransform;

        /// <summary>
        /// The space around the text label so that we
        /// aren't up against the edges of the cell
        /// </summary>
        public RectOffset textBuffer;

        //TODO:  include external ID for speed/accuracy
        public Text chargerLocationName;
        public Text address;
        public Text numberOfChargers;
        public Text rate;
        public Text status;

        private ChargingLocation myCellData;
        public GameEvent OnChargingLocationSelected;

        public void SetData(ChargingLocation celllData)
        {
            //base.SetData(data);
            //someTextText.text = "HI Darryl!" + data.name +  data.address;       
            chargerLocationName.text = celllData.name;
            address.text = celllData.fullAddress;
            numberOfChargers.text = celllData.numberOfChargers.ToString();
            rate.text = celllData.rate.ToString();
            status.text = celllData.status;

            // force update the canvas so that it can calculate the size needed for the text immediately
            Canvas.ForceUpdateCanvases();

            // set the data's cell size and add in some padding so the the text isn't up against the border of the cell
            celllData.cellSize = textRectTransform.rect.height + textBuffer.top + textBuffer.bottom;

            myCellData = celllData;  // hard reset
        }

        public void OnCellClicked()
        {
            Debug.Log("*** Location Cell clicked - " + chargerLocationName.text);

            //TODO:  ServerSchema.ChargingLocationInfo serverData = AppManager.Instance.localCache.GetLocationByExtID();
            AppManager.Instance.userSettings.RecentLocations.Add(myCellData.name); //MyDebug();
            AppStateController.Instance.appStateData.SelectedLocation = AppLocalCache.GetLocationByName(myCellData.name);

            OnChargingLocationSelected.Raise();
        }

        private static void MyDebug()
        {
            List<string> recentLocs = AppManager.Instance.userSettings.RecentLocations;
            Debug.Log("Recent Locations @ usersettings = " + recentLocs[1] + " & count = " + recentLocs.Count);
        }


        //private bool isCoroutineExecuting;

        //IEnumerator ExecuteAfterTime(float time, System.Action task)
        //{
        //    if (isCoroutineExecuting)
        //        yield break;
        //    isCoroutineExecuting = true;
        //    yield return new WaitForSeconds(time);
        //    task();
        //    isCoroutineExecuting = false;
        //}
    }
}