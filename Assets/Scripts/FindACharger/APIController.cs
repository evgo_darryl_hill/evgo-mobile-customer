﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace EVgo
{
    public class APIController : MonoBehaviour
    {
        //public delegate void OnThrustChanged(float amount);
        public static event UnityAction<string> OnLocationDataLoad = delegate { };

        private const string baseUrl = "https://api-demo.evconnect.com/rest/v5/";
        private const string accept = "application/json";
        private const string appToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkMTZlODlmMy1mNGU5LTQ2NTMtYmMzNy1jYzI1NjY2NjUzMGIiLCJ1c2VyUm9sZSI6IkFETUlOIn0.OKsshezl_EkmsWrsdni5sK0GCKyuP2vCKlQWK5aBnZ9AKATHXo7NVJLLjSjEP1fV4L4W9-pnUrpJwGaZCLTknw";

        public string section = "locations/";

        public void GetLocationsFor(UserSettings userSettings = null)
        {
            //TODO:  test for null

            UserSettings settingsHolder = (userSettings == null) ? AppManager.Instance.userSettings : userSettings;

            GetLocationsFor(GenerateSpecificsFromSettings(settingsHolder));
        }

        private void GetLocationsFor(string specifics)
        {
            //TODO:  test for null
            WWWForm form = new WWWForm();
            Dictionary<string, string> headers = form.headers;
            headers["accept"] = accept;
            headers["EVC-API-TOKEN"] = appToken;

            //specifics = (specifics == null)
            WWW request = new WWW(baseUrl + section + specifics, null, headers);

            AppManager.Instance.LockApp("Pulling JSON data");

            StartCoroutine(OnResponse(request));
        }

        public string GenerateSpecificsFromSettings(UserSettings settings)
        {
            var tempSpecifics = "mapping?latitude=" + settings.Latitude.ToString() +
                         "&longitude=" + settings.Longitude.ToString() +
                         "&distance=" + settings.MaxSearchRadius.ToString() +
                         "&metric=" + settings.Metric.ToString().ToUpper();

            return tempSpecifics;
        }

        private IEnumerator OnResponse(WWW wwwResponse)
        {
            yield return wwwResponse;

            if (OnLocationDataLoad != null) OnLocationDataLoad(wwwResponse.text);

            print("Server response - " + wwwResponse.text);
        }

    }
}
