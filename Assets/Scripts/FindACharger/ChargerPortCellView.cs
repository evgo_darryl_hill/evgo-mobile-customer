﻿using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

namespace EVgo
{
    [RequireComponent(typeof(ShowIconForType))]

    public class ChargerPortCellView : EnhancedScrollerCellView
    {
        /// <summary>
        /// A reference to the rect transform which will be
        /// updated by the content size fitter
        /// </summary>
        public RectTransform textRectTransform;

        /// <summary>
        /// The space around the text label so that we
        /// aren't up against the edges of the cell
        /// </summary>
        public RectOffset textBuffer;

        #region view elements
        public Text qrCode;
        public Text powerLevel;
        public Text status;
        public Text type;
        public Text handicapAccessible;
        public Text pricingPolicyExternalId;
        public Text free;
        #endregion

        public void SetData(Connector data)
        {
            //base.SetData(data);
            //someTextText.text = "HI Darryl!" + data.name +  data.address;       
            qrCode.text = data.qrCode;
            powerLevel.text = data.type.ToString();
            //type.text = data.type.ToString();
            type.text = data.type;
            status.text = data.status.ToString();
            handicapAccessible.text = data.handicapAccessible;
            free.text = data.free;

            // force update the canvas so that it can calculate the size needed for the text immediately
            Canvas.ForceUpdateCanvases();

            // set the data's cell size and add in some padding so the the text isn't up against the border of the cell
            data.cellSize = textRectTransform.rect.height + textBuffer.top + textBuffer.bottom;
        }

        public void OnCellClicked()
        {
            Debug.Log("*** Charger Port Cell clicked - " + qrCode.text);
            //fire some event with Cell info and data
        }
    }
}


