﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EVgo
{
    public class FilterViewController : MonoBehaviour
    {
        public InputField latitude;
        public InputField longitude;
        public InputField zipCode;
        public Slider searchRadius;

        private UserSettings userSettings;

        void Awake()
        {
            userSettings = AppManager.Instance.userSettings;
        }

        void Start()
        {
            //load values from settings 
            latitude.text = userSettings.Latitude.ToString();
            longitude.text = userSettings.Longitude.ToString();
            //zipCode.text = userSettings.PreferredZip;

            searchRadius.normalizedValue = userSettings.MaxSearchRadius;  // should trigger value change
            //searchRadius.value = userSettings.MaxSearchRadius;
        }

        void CreateNewUserSettings()
        {
            //userSettings.Latitude = float.Parse(latitude.text, CultureInfo.InvariantCulture.NumberFormat);
            //UserSettings.CreateInstance<UserSettings>();
        }

        public List<ServerSchema.ChargingLocationInfo> FilterRecords(List<ServerSchema.ChargingLocationInfo> rows)  // another object
        {
            //FindObjectOfType all records that match user settings criteria
            List<ServerSchema.ChargingLocationInfo> filteredRows = new List<ServerSchema.ChargingLocationInfo>();

            bool bOn = false;
            foreach (ServerSchema.ChargingLocationInfo row in rows)
            {
                Debug.Log("Point: " + row.point);
                //print(location.name);  
                //Debug.Log(FilterRecords will be filtered here)

                bOn = (bOn) ? false : true;
                if (bOn) filteredRows.Add(row);
                else Debug.Log("Temp filter removed - " + row.point);
            }

            return filteredRows;

        }

        public void OnDownloadButtonHit()
        {
            //save input values to settings 
            userSettings.Latitude = float.Parse(latitude.text);  // string str = "3.14159";
            userSettings.Longitude = float.Parse(longitude.text);
            userSettings.MaxSearchRadius = (int)searchRadius.value;
            // perform API request 

            // Clear display once input is validated
            AppManager.Instance.apiController.GetLocationsFor(userSettings);
        }

    }
}
