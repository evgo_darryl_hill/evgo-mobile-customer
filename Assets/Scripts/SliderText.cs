﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderText : MonoBehaviour
{
    TMP_Text textComponent;
    string suffix;

    void Start()
    {
        textComponent = GetComponent<TMP_Text>();
        suffix = textComponent.text;
    }

    public void SetSliderValue(float sliderValue)
    {
        if (textComponent == null) return;

        textComponent.text = Mathf.Round(sliderValue).ToString();
        textComponent.text += " " + suffix;
    }
}
