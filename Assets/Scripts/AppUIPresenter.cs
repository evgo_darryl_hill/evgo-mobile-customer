﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DoozyUI;
using UnityEngine.SceneManagement;

namespace EVgo
{
    public class AppUIPresenter : MonoBehaviour
    {
        public bool IsReady = false;
        private static string AppCategory = "AppElements";

        private List<string> stateNames;
        public List<string> StateNames
        {
            get
            {
                if (stateNames == null)
                { stateNames = StateToUIEBinder.GetElementNames(); }  // Pulls all UIELements
                return stateNames;
            }
            private set
            {
                stateNames = value;
            }
        }

        private List<string> _sceneList;
        public List<string> sceneList
        {
            get
            {
                if (_sceneList == null) { return InitSceneList(); }
                else { return _sceneList; }
            }
        }

        public List<string> InitSceneList()
        {
            int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings;
            var theList = new List<string>();

            Debug.Log("Scene Names: ");
            for (int i = 0; i < sceneCount; i++)
            {
                var str = System.IO.Path.GetFileNameWithoutExtension(UnityEngine.SceneManagement.SceneUtility.GetScenePathByBuildIndex(i));
                Debug.Log(str);
                theList.Add(str);
            }

            return theList;
        }

        void Awake()
        {
            IsReady = true;
        }

        public void HideConnectedElements(string stateName)
        {
            UIManager.HideUiElement(stateName, AppCategory, false);  // allow animation to play

            if (SceneManager.GetSceneByName(stateName).IsValid()) //    //if (sceneList.Contains(stateName))  
            {
                SceneManager.UnloadSceneAsync(stateName);
            }
        }

        public void ShowConnectedElements(string stateName)
        {
            if (sceneList.Contains(stateName))
            {

                SceneManager.LoadSceneAsync(stateName, LoadSceneMode.Additive);
            }

            UIManager.ShowUiElement(stateName, AppCategory, false);

            IsReady = true;  //HACK: should happen in on show complete (SAB)
        }

        public void OnShowComplete()
        {
            //UnloadPreviousScene();
            IsReady = true;
        }

    }
}
