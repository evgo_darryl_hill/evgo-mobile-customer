﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

namespace EVgo
{
    public class FieldUpdater : MonoBehaviour
    {
        public TMP_Text textField;  // field to be updated
        private string suffix;

        // Use this for initialization
        void Awake()
        {
            suffix = textField.text;

            ChargerListViewController.OnRecordTallyAction += ChangeTextTo;
            // FIXME:  will most likely be useing an interace instead of an explict call
        }

        // Update is called once per frame
        void ChangeTextTo(int anInt)
        {
            textField.text = anInt.ToString() + " " + suffix;
        }

        void ChangeTextTo(string aString)
        {
            textField.text = aString + " " + suffix;
        }
    }

}